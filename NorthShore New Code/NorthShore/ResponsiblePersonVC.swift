//
//  ContactInfoViewController.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ResponsiblePersonVC: PDViewController {

    @IBOutlet weak var textFieldState: PDTextField!
  
    @IBOutlet weak var textFieldWorkphone: PDTextField!
    @IBOutlet weak var textFieldSocialSecurity: PDTextField!
    @IBOutlet weak var textFieldRelation: PDTextField!
    @IBOutlet weak var textFieldExtension: PDTextField!
    @IBOutlet weak var textFieldEmployer: PDTextField!
    @IBOutlet weak var textFieldEmergencyContactNumber: PDTextField!
    @IBOutlet weak var textFiledEmergencyContact: PDTextField!
    @IBOutlet weak var textFiledzip: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldAddress: PDTextField!
    @IBOutlet weak var textFieldDriverLicense: PDTextField!
    
    @IBOutlet weak var radioResponsiblePerson: RadioButton!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        StateListView.addStateListForTextField(textFieldState)
        
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.responsiblePersonIsPatient = radioResponsiblePerson.isSelected
        
        patient.RespSocialSecurity = textFieldSocialSecurity.text
        patient.RespemployerName = textFieldEmployer.text
        patient.RespContactName = textFiledEmergencyContact.text
        patient.RespState = textFieldState.text
        patient.Respcity = textFieldCity.text
        patient.RespZip = textFiledzip.text
        patient.RespRelationName = textFieldRelation.text
        patient.RespworkphoneNumber = textFieldWorkphone.text
        patient.RespHomephoneNumber = textFieldEmergencyContactNumber.text
        patient.RespAddress = textFieldAddress.text
        patient.RespDrivierlicense = textFieldDriverLicense.text
    }
    func loadValues() {
        radioResponsiblePerson.isSelected = patient.responsiblePersonIsPatient == nil ? false : patient.responsiblePersonIsPatient
        textFieldSocialSecurity.text = patient.RespSocialSecurity
        textFieldEmployer.text = patient.RespemployerName
        textFiledEmergencyContact.text = patient.RespContactName
        textFieldState.text = patient.RespState == nil || patient.RespState.isEmpty ? "IL" : patient.RespState
        textFieldCity.text = patient.Respcity
        textFiledzip.text = patient.RespZip
        textFieldRelation.text = patient.RespRelationName
        textFieldWorkphone.text = patient.RespworkphoneNumber
        textFieldEmergencyContactNumber.text = patient.RespHomephoneNumber
        textFieldAddress.text = patient.RespAddress
        textFieldDriverLicense.text = patient.RespDrivierlicense
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
@IBAction func radioResponsePersonAction (_ sender : RadioButton){
        if sender.tag == 1{
            textFiledEmergencyContact.text = patient.fullName
            textFieldRelation.text = "SELF"
            textFieldEmployer.text = patient.ContactemployerName
            
            textFieldEmergencyContactNumber.text = patient.HomephoneNumber
            
            textFieldWorkphone.text = patient.workphoneNumber
            textFieldSocialSecurity.text = patient.socialSecurityNumber
           
            textFieldDriverLicense.text = patient.drivingLicense
            textFieldAddress.text = patient.ContactAddress
            textFieldCity.text = patient.ContactCity
            textFieldState.text = patient.ContactState == nil || patient.ContactState == "" ? "IL" : patient.ContactState
            textFiledzip.text = patient.ContactZip
        } else {
            textFiledEmergencyContact.text = ""
     
            textFieldRelation.text = ""
            textFieldEmployer.text = ""
            
            textFieldEmergencyContactNumber.text = ""
            
            textFieldWorkphone.text = ""
            textFieldSocialSecurity.text = ""
            
            textFieldDriverLicense.text = ""
            
            textFieldAddress.text = ""
            textFieldCity.text = ""
            textFieldState.text = "IL"
            textFiledzip.text = ""
        }
    }
    
    
    func findEmptyTextField() -> UITextField? {
        let textFields = [textFiledEmergencyContact, textFieldRelation]
        for textField in textFields {
            if (textField?.isEmpty)! {
                return textField
            }
        }
        return nil
    }

    

    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        
        if let _ = findEmptyTextField() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldEmergencyContactNumber.isEmpty && !textFieldEmergencyContactNumber.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID HOME PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldSocialSecurity.isEmpty && !textFieldSocialSecurity.text!.isSocialSecurityNumber {
            let alert = Extention.alert("PLEASE ENTER VALID SOCIAL SECURITY NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldWorkphone.isEmpty && !textFieldWorkphone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID WORK PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else if !textFiledzip.isEmpty && !textFiledzip.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID ZIP CODE")
            self.present(alert, animated: true, completion: nil)
        } else {
            
            saveValues()
            
            if patient.maritialStatus == 2 {
                let diseaseInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kSpouseInformationVC") as! SpouseInformationVC
                diseaseInfoVC.patient = patient
                self.navigationController?.pushViewController(diseaseInfoVC, animated: true)
                
            } else {
                let diseaseInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kAgreementVC") as! AgreementViewController
                diseaseInfoVC.patient = patient
                self.navigationController?.pushViewController(diseaseInfoVC, animated: true)
            }
        }
    }
}

extension ResponsiblePersonVC : UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textFieldEmergencyContactNumber ||  textField == textFieldWorkphone || textField == textFieldEmergencyContactNumber {
            return textField.formatPhoneNumber(range, string: string)
        } else  if textField == textFiledzip {
            return textField.formatZipCode(range, string: string)
        } else  if textField == textFieldSocialSecurity  {
            return textField.formatNumbers(range, string: string, count: 9)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


