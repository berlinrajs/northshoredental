//
//  ReleaseRecordViewController.swift
//  DiamondDental
//
//  Created by Leojin Bose on 5/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ReleaseRecordViewController: PDViewController {

    
    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var radioButton: RadioButton!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var dateLabel: DateLabel!
    @IBOutlet weak var labelText: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        dateLabel.todayDate = patient.dateToday
        let patientName = "\(patient.fullName)"
        labelPatientName.text = patientName
        labelText.text = labelText.text!.replacingOccurrences(of: "kPatientName", with: patientName)
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.releaseRecordsignature1 = signatureView.isSigned() ? signatureView.signatureImage() : nil
    }
    func loadValues() {
        signatureView.previousImage = patient.releaseRecordsignature1
        radioButton.setSelectedWithTag(patient.releaseType == nil ? 10 : patient.releaseType)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func radioButtonAction(_ sender: RadioButton) {
        patient.releaseType = sender.tag
        if sender.tag == 3 {
            PopupTextField.sharedInstance.showWithTitle("North Shore",placeHolder : "NAME", keyboardType: .default, textFormat: .default, completion: { (textField, isEdited) in
                if isEdited {
                    self.patient.authorizedPerson = textField.text
                } else {
                    sender.deselectAllButtons()
                }
            })
        }
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !dateLabel.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else if radioButton.selected == nil {
            let alert = Extention.alert("PLEASE SELECT ALL THAT REQUIRED")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            let releaseRecordFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kReleaseRecordFormVC") as! ReleaseRecordFormViewController
            releaseRecordFormVC.patient = patient
            self.navigationController?.pushViewController(releaseRecordFormVC, animated: true)
        }
    }
}
