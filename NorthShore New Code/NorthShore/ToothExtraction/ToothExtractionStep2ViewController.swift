//
//  ToothExtractionStep2ViewController.swift
//  North Shore
//
//  Created by Office on 2/24/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ToothExtractionStep2ViewController: PDViewController {

    var toothExtractionStep2 : [PDOption]! = [PDOption]()
    
    @IBOutlet weak var textField1: PDTextField!
    @IBOutlet weak var textFieldOthers: PDTextField!
    @IBOutlet weak var collectionViewoptions: UICollectionView!
    @IBOutlet weak var buttonOther: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchData()
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        if !textFieldOthers.isEmpty {
            patient.othersText2 = textFieldOthers.text
        } else {
            patient.othersText2 = nil
        }
        if !textField1.isEmpty {
            patient.prognosisProcedure = textField1.text
        } else {
            patient.prognosisProcedure = nil
        }
        
        let selectedOptions = self.toothExtractionStep2.filter { (option) -> Bool in
            return option.isSelected != nil && option.isSelected == true
        }
        let step2Text : String! = ((selectedOptions as NSArray).value(forKey: "question") as! [String]).joined(separator: ", ")
        patient.toothExtractionQuestions2 = step2Text.isEmpty ? nil : step2Text
    }
    func loadValues() {
        textFieldOthers.text = patient.othersText2
        
        if patient.othersText2 != nil && patient.othersText2!.characters.count > 0 {
            textFieldOthers.isEnabled = true
            buttonOther.isSelected = true
        }
        
        textField1.text = patient.prognosisProcedure
        
        let selectedOptions = patient.toothExtractionQuestions2?.components(separatedBy: ", ")
        for option in self.toothExtractionStep2 {
            option.isSelected = selectedOptions?.contains(option.question)
            self.collectionViewoptions.reloadData()
        }
    }
    
    func fetchData() {
        PDOption.fetchQuestionsToothExtractionForm2 { (result, success) -> Void in
            if success {
                self.toothExtractionStep2.append(contentsOf: result!)
                self.collectionViewoptions.reloadData()
            }
        }
    }
    
    @IBAction func buttonNextAction(_ sender: AnyObject) {
        self.view.endEditing(true)
        let selectedOptions = self.toothExtractionStep2.filter { (option) -> Bool in
            return option.isSelected != nil && option.isSelected == true
        }
        
        if selectedOptions.count == 0 && !textFieldOthers.isEnabled {
            let alert = Extention.alert("PLEASE SELECT ANY OPTION")
            self.present(alert, animated: true, completion: nil)
        } else if textFieldOthers.isEnabled && textFieldOthers.isEmpty {
            let alert = Extention.alert("PLEASE ENTER OTHER REASON")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            let toothExtractionStep3VC = self.storyboard?.instantiateViewController(withIdentifier: "kToothExtractionStep3VC") as! ToothExtractionStep3ViewController
            toothExtractionStep3VC.patient = patient
            self.navigationController?.pushViewController(toothExtractionStep3VC, animated: true)
        }
    }
    
    @IBAction func buttonActionOthers(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        textFieldOthers.isEnabled = sender.isSelected
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ToothExtractionStep2ViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = self.toothExtractionStep2[indexPath.item]
        if let selected = obj.isSelected {
            obj.isSelected = !selected
        } else {
            obj.isSelected = true
        }
        collectionView.reloadData()
    }
}

extension ToothExtractionStep2ViewController : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.toothExtractionStep2.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellMedicalHistory", for: indexPath) as! MedicalHistoryCollectionViewCell
        let obj = self.toothExtractionStep2[indexPath.item]
        cell.configureCellOption(obj)
        return cell
    }
}
