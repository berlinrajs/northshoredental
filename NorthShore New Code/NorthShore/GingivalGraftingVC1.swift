//
//  GingivalGraftingVC1.swift
//  NorthShore
//
//  Created by Leojin Bose on 7/12/16.
//  Copyright © 2016 SRS. All rights reserved.
//


import UIKit

class GingivalGraftingVC1: PDViewController {


    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var signatureView2: SignatureView!
    @IBOutlet weak var labelDate1: DateLabel!
    @IBOutlet weak var labelDate2: DateLabel!
    @IBOutlet weak var textFieldRelationship: PDTextField!
    @IBOutlet weak var textFieldRelationName: PDTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
       
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.Gingivalsignature1 = signatureView1.isSigned() ? signatureView1.signatureImage() : nil
        patient.Gingivalsignature2 = signatureView2.isSigned() ? signatureView2.signatureImage() : nil
        patient.Gingivalrelationship = textFieldRelationship.text
        patient.GingivalrelationShipName = textFieldRelationName.text
    }
    func loadValues() {
        signatureView1.previousImage = patient.Gingivalsignature1
        signatureView2.previousImage = patient.Gingivalsignature2
        textFieldRelationship.text = patient.Gingivalrelationship
        textFieldRelationName.text = patient.GingivalrelationShipName
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if !signatureView1.isSigned() || !signatureView2.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if labelDate1.text == "Tap to date" || labelDate2.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let GingivalForm = self.storyboard?.instantiateViewController(withIdentifier: "kGingivalGraftingForm") as! GingivalGraftingForm
            GingivalForm.patient = self.patient
            self.navigationController?.pushViewController(GingivalForm, animated: true)
        }
    }
}
