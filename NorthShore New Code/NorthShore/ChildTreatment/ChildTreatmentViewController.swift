//
//  ChildTreatmentViewController.swift
//  North Shore
//
//  Created by Leojin Bose on 2/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ChildTreatmentViewController: PDViewController {

    var textRanges : [NSRange]! = [NSRange]()
    var selectedOptions : [String]! = [String]()
    
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet weak var textFieldDate: PDTextField!
    @IBOutlet weak var textFieldOthers: PDTextField!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: PDLabel!
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var tableViewOptions: UITableView!
    @IBOutlet weak var radioButtonParent: RadioButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldOthers.isEnabled = false
        DateInputView.addDatePickerForTextField(textFieldDate)
//        textFieldDate.inputAccessoryView = toolBar
//        textFieldDate.inputView = datePicker
        signatureView.layer.cornerRadius = 3.0
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChildTreatmentViewController.setDateOnLabel))
        tapGesture.numberOfTapsRequired = 1
        labelDate.addGestureRecognizer(tapGesture)
        
        var patientInfo = "I am unable to accompany my child"
        
        let patientName = getText("\(patient.fullName)")
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " DOB \(getText(patient.dateOfBirth))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateOfBirth)))
        
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        }
        
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float)], range: NSMakeRange(0, attributedString.length))
        labelDetails.attributedText = attributedString
        
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        if !textFieldDate.isEmpty {
            patient.treatmentDate = textFieldDate.text
        } else {
            patient.treatmentDate = nil
        }
        if !textFieldOthers.isEmpty {
            patient.otherTreatment1 = textFieldOthers.text
        } else {
            patient.otherTreatment1 = nil
        }
        patient.selectedOptions1 = self.selectedOptions
        patient.signatureChildTreatment = signatureView.isSigned() ? signatureView.signatureImage() : nil
        patient.relationship = radioButtonParent.selected == nil ? nil : radioButtonParent.selected.tag == 1 ? "Parent" : "Guardian"
    }
    func loadValues() {
        textFieldDate.text = patient.treatmentDate
        textFieldOthers.text = patient.otherTreatment1
        if patient.selectedOptions1 != nil {
            self.selectedOptions = patient.selectedOptions1
        }
        signatureView.previousImage = patient.signatureChildTreatment
        
        radioButtonParent.setSelectedWithTag(patient.relationship == nil ? 0 : patient.relationship == "Parent" ? 1 : 2)
    }
    
    func setDateOnLabel() {
        labelDate.text = patient.dateToday
        labelDate.textColor = UIColor.black
    }
    
   
    
    @IBAction func buttonNextPressed(_ sender: AnyObject) {
        self.view.endEditing(true)
        if self.selectedOptions.count == 0 {
            let alert = Extention.alert("PLEASE SELECT ALL THAT APPLY")
            self.present(alert, animated: true, completion: nil)
        } else if self.selectedOptions.contains("OTHER") && textFieldOthers.isEmpty {
            let alert = Extention.alert("PLEASE TYPE OTHER TREATMENT")
            self.present(alert, animated: true, completion: nil)
        } else if radioButtonParent.selected == nil {
            let alert = Extention.alert("PLEASE SELECT RELATIONSHIP")
            self.present(alert, animated: true, completion: nil)
        } else if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if labelDate.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let childTreatmentFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kChildTreatmentFormVC") as! ChildTreatmentFormViewController
            childTreatmentFormVC.patient = patient
            self.navigationController?.pushViewController(childTreatmentFormVC, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func toolbarDoneButtonAction(_ sender: AnyObject) {
        textFieldDate.resignFirstResponder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldDate.text = dateFormatter.string(from: datePicker.date).uppercased()
    }
    
    @IBAction func datePickerDateChanged(_ sender: AnyObject) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldDate.text = dateFormatter.string(from: datePicker.date).uppercased()
    }
}

extension ChildTreatmentViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let titles = ["TOOTH CLEANING", "SEALANTS", "FILLINGS", "EXTRACTIONS OF PRIMARY TEETH (BABY TEETH)", "EXTRACTION OF PERMANENT TEETH", "ROOT CANAL TREATMENT", "OTHER"]
        let title = titles[indexPath.row]
        if selectedOptions.contains(title) {
            selectedOptions.remove(at: selectedOptions.index(of: title)!)
        } else {
            selectedOptions.append(title)
        }
        if !selectedOptions.contains(titles[6]) {
            textFieldOthers.text = ""
        }
        tableView.reloadData()
    }
}

extension ChildTreatmentViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellChildTreatment", for: indexPath) as! ChildTreatmentTableViewCell
        let titles = ["TOOTH CLEANING", "SEALANTS", "FILLINGS", "EXTRACTIONS OF PRIMARY TEETH (BABY TEETH)", "EXTRACTION OF PERMANENT TEETH", "ROOT CANAL TREATMENT", "OTHER"]
        cell.labelTitle.text = titles[indexPath.row]
        cell.buttonRound.isSelected = selectedOptions.contains(titles[indexPath.row])
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        textFieldOthers.isEnabled = selectedOptions.contains(titles[6])
        
        return cell
        
    }
}
