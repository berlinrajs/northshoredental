//
//  HippaStep1VC.swift
//  North Shore
//
//  Created by SRS Web Solutions on 05/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HippaStep1VC: PDViewController {

    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.Hippa1signature1 = signatureView.isSigned() ? signatureView.signatureImage() : nil
    }
    func loadValues() {
        signatureView.previousImage = patient.Hippa1signature1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func nextAction(_ sender: AnyObject) {
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let step2VC = self.storyboard?.instantiateViewController(withIdentifier: "kHippaStep2VC") as! HippaStep2VC
            step2VC.patient = patient
            navigationController?.pushViewController(step2VC, animated: true)
        }
    }
}
