//
//  ToothWhiteningFormVC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 13/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class SedationConsentFormVC: PDViewController {

    
    @IBOutlet weak var labelDate1: UILabel!
  
    @IBOutlet weak var patientSignature: UIImageView!
  
    
    @IBOutlet weak var witessSignature: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        
        labelDate1.text = patient.dateToday
        patientSignature.image = patient.sedationsignature1
        witessSignature.image = patient.sedationsignature2
    
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
