//
//  ReleaseConfidentialViewController.swift
//  North Shore
//
//  Created by Office on 2/21/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ReleaseConfidentialViewController: PDViewController {
  
    @IBOutlet weak var labelDate: PDLabel!
    @IBOutlet weak var signatureView: SignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signatureView.layer.cornerRadius = 3.0
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ReleaseConfidentialViewController.setDateOnLabel))
        tapGesture.numberOfTapsRequired = 1
        labelDate.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    
    func saveValues() {
        patient.signature1 = signatureView.isSigned() ? signatureView.signatureImage() : nil
    }
    
    func loadValues() {
        signatureView.previousImage = patient.signature1
    }
    
    func setDateOnLabel() {
        labelDate.text = patient.dateToday
        labelDate.textColor = UIColor.black
    }
    
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if labelDate.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let authorizationFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kAuthorizationFormVC") as! AuthorizationFormViewController
            authorizationFormVC.patient = patient
            self.navigationController?.pushViewController(authorizationFormVC, animated: true)
        }
    }
   
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}



