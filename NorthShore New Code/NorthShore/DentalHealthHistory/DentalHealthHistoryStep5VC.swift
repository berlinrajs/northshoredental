//
//  DentalHealthHistoryStep5VC.swift
//  NorthShore
//
//  Created by SRS Websolutions.com on 7/4/18.
//  Copyright © 2018 SRS. All rights reserved.
//

import UIKit

class DentalHealthHistoryStep5VC: PDViewController {
    
    @IBOutlet weak var textFieldPharmacyName: PDTextField!
    @IBOutlet weak var textFieldPhone: PDTextField!
    @IBOutlet weak var textViewMedication: PDTextView!
    
    @IBOutlet var checkBox1: [UIButton]!
    @IBOutlet var checkBox2: [UIButton]!
    @IBOutlet var checkBox3: [UIButton]!
    
    @IBOutlet weak var sign1: SignatureView!
    @IBOutlet weak var labelDate: PDLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        sign1.layer.cornerRadius = 3.0.layer.cornerRadius = 3.0
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(DentalHealthHistoryStep5VC.setDateOnLabel1))
        tapGesture1.numberOfTapsRequired = 1
        labelDate.addGestureRecognizer(tapGesture1)
        
        loadValues()
    }
    
    func setDateOnLabel1() {
        labelDate.text = patient.dateToday
        labelDate.textColor = UIColor.black
    }
    
    func loadValues() {
        textFieldPharmacyName.text = patient.dhhPharmacyName
        textFieldPhone.text = patient.dhhPharmacyPhone
//        textViewMedication.text = patient.dhhMedicationList
        
        if patient.dhhMedicationsTag == nil {
            patient.dhhMedicationsTag = [Int]()
        }
        for button in checkBox1 {
            button.isSelected = patient.dhhMedicationsTag.contains(button.tag)
        }
        
        if patient.dhhBloodThinnersTag == nil {
            patient.dhhBloodThinnersTag = [Int]()
        }
        for button in checkBox2 {
            button.isSelected = patient.dhhBloodThinnersTag.contains(button.tag)
        }
        
        if patient.dhhOtherTag == nil {
            patient.dhhOtherTag = [Int]()
        }
        for button in checkBox3 {
            button.isSelected = patient.dhhOtherTag.contains(button.tag)
        }
        
        self.textViewDidBeginEditing(textViewMedication)
        textViewMedication.text = patient.dhhMedicationList
        self.textViewDidEndEditing(textViewMedication)
    }
    
    func saveValues() {
        patient.dhhPharmacyName = textFieldPharmacyName.text
        patient.dhhPharmacyPhone = textFieldPhone.text
//        patient.dhhMedicationList = textViewMedication.text
        
        patient.dhhSignature = sign1.isSigned() ? sign1.signatureImage() : nil
        
        patient.dhhMedicationList = textViewMedication.text == "TYPE HERE" ? "" : textViewMedication.text
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    
    @IBAction func buttonCheckBoxAction1 (_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            if !self.patient.dhhMedicationsTag.contains(sender.tag) {
                self.patient.dhhMedicationsTag.append(sender.tag)
            }
        } else {
            if self.patient.dhhMedicationsTag.contains(sender.tag) {
                self.patient.dhhMedicationsTag.remove(at: self.patient.dhhMedicationsTag.index(of: sender.tag)!)
            }
        }
    }
    
    @IBAction func buttonCheckBoxAction2 (_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            if !self.patient.dhhBloodThinnersTag.contains(sender.tag) {
                self.patient.dhhBloodThinnersTag.append(sender.tag)
            }
        } else {
            if self.patient.dhhBloodThinnersTag.contains(sender.tag) {
                self.patient.dhhBloodThinnersTag.remove(at: self.patient.dhhBloodThinnersTag.index(of: sender.tag)!)
            }
        }
    }
    
    @IBAction func buttonCheckBoxAction3 (_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            if !self.patient.dhhOtherTag.contains(sender.tag) {
                self.patient.dhhOtherTag.append(sender.tag)
            }
        } else {
            if self.patient.dhhOtherTag.contains(sender.tag) {
                self.patient.dhhOtherTag.remove(at: self.patient.dhhOtherTag.index(of: sender.tag)!)
            }
        }
    }
    
    @IBAction func buttonNextAction(_ sender: Any) {
        self.view.endEditing(true)
        if !textFieldPhone.isEmpty && !textFieldPhone.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER THE VALID PHONE")
        } else if !sign1.isSigned() {
            self.showAlert("PLEASE ADD THE VALID SIGNATURE")
        } else if labelDate.text == "Tap to date" {
            self.showAlert("PLEASE ADD THE DATE")
        } else {
            saveValues()
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "DentalHealthHistoryFormVC") as! DentalHealthHistoryFormVC
            nextVC.patient = patient
            self.navigationController?.pushViewController(nextVC, animated: true)
        }
        
    }
    
}

extension DentalHealthHistoryStep5VC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
         if textField == textFieldPhone  {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


extension DentalHealthHistoryStep5VC : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
