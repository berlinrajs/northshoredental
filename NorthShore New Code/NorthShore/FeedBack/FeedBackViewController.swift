//
//  FeedBackViewController.swift
//  DistinctiveDentalCare
//
//  Created by Berlin Raj on 25/08/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class FeedBackViewController: PDViewController {
    
    @IBOutlet weak var textViewComment: PDTextView!
    @IBOutlet weak var buttonAllowMessage: UIButton!
    @IBOutlet weak var buttonAnonymous: UIButton!
    @IBOutlet weak var viewRating: UIView!
    
    var ratingView : HCSStarRatingView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ratingView = HCSStarRatingView(frame: viewRating.bounds)
        ratingView.allowsHalfStars = true
        ratingView.emptyStarImage = UIImage(named: "ReviewNoStar")
        ratingView.halfStarImage = UIImage(named: "ReviewHalfStar")
        ratingView.filledStarImage = UIImage(named: "ReviewFullStar")
        ratingView.spacing = 5.0
        ratingView.maximumValue = 5
        ratingView.backgroundColor = UIColor.clear
        viewRating.addSubview(ratingView)
        
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    
    var feedBack: FeedBack! {
        get {
            return patient.feedBack
        }
    }
    
    
    func saveValues() {
        feedBack.comment = textViewComment.text == "Please share your thoughts. Let us know if there is anything we can improve on." ? "" : textViewComment.text!
        feedBack.rating = ratingView.value
        feedBack.isAllowMessage = self.buttonAllowMessage.isSelected
        feedBack.isAnonymous = self.buttonAnonymous.isSelected
    }
    func loadValues() {
        self.ratingView.value = feedBack.rating == nil ? 0.0 : feedBack.rating
        self.buttonAnonymous.isSelected = feedBack.isAnonymous == nil ? false : feedBack.isAnonymous
        self.buttonAllowMessage.isSelected = feedBack.isAllowMessage == nil ? false : feedBack.isAllowMessage
        self.textViewDidBeginEditing(textViewComment)
        self.textViewComment.text = feedBack.comment == nil ? "" : feedBack.comment
        self.textViewDidEndEditing(textViewComment)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonSubmitAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "Distinctive Dental Care", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.shared.openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.present(alertController, animated: true, completion: nil)
        } else {
            saveValues()
            BRProgressHUD.show()
            ServiceManager.postReview(feedBack.fullName, comment: feedBack.comment, rating: feedBack.rating, phoneNumber: feedBack.phoneNumber, allowMessage: feedBack.isAllowMessage, email: "", anonymous: feedBack.isAnonymous, completion: { (success, error) in
                BRProgressHUD.hide()
                if success {
                    self.showAlert(self.ratingView.value >= 4.0 && self.buttonAllowMessage.isSelected == true ? "Thanks for your feedback, Please check your phone" : "Thanks for your feedback", completion: { (completed) in
                        if completed {
                            self.patient.selectedForms.removeFirst()
                            self.gotoNextForm()
                        }
                    })
                } else {
                    self.showAlert(error!.localizedDescription)
                }
            })
        }
    }
    @IBAction func buttonAllowAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
}
extension FeedBackViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        if textViewComment.text == "" {
            textViewComment.text = "Please share your thoughts. Let us know if there is anything we can improve on."
            textViewComment.textColor = UIColor.lightGray
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textViewComment.text == "" || textViewComment.text == "Please share your thoughts. Let us know if there is anything we can improve on." {
            textViewComment.text = ""
            textViewComment.textColor = UIColor.black
        }
    }
}
