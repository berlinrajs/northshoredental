//
//  PopupTextField.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/12/16.
//  Copyright © 2016 SRS. All rights reserved.
//

enum TextFormat : Int {
    case `default` = 0
    case socialSecurity
    case toothNumber
    case phone
    case zipcode
    case number
    case date
}

import UIKit

class PopupTextField: UIView {

    static let sharedInstance = Bundle.main.loadNibNamed("PopupTextField", owner: nil, options: nil)!.first as! PopupTextField
    
    class func popUpView() -> PopupTextField {
        return Bundle.main.loadNibNamed("PopupTextField", owner: nil, options: nil)!.first as! PopupTextField
    }
    
    @IBOutlet weak var textField: PDTextField!
    @IBOutlet weak var labelTitle: UILabel!
    
    var completion:((UITextField, Bool)->Void)?
    var textFormat : TextFormat!
    var count : Int!

    
    func show(_ completion : @escaping (_ textField : UITextField, _ isEdited : Bool) -> Void) {
        self.showWithTitle(labelTitle.text, placeHolder: "TYPE HERE", keyboardType: .default, textFormat: .default, completion: completion)
    }
    
    func showWithTitle(_ title: String?, placeHolder : String?, keyboardType : UIKeyboardType, textFormat: TextFormat, completion : @escaping (_ textField : UITextField, _ isEdited : Bool) -> Void) {
        textField.text = ""
        labelTitle.text = title
        self.textFormat = textFormat
        if let _ = placeHolder {
            textField.placeholder = placeHolder
        }
        if textFormat == .date {
            //DateInputView.addDatePickerForTextField(textField, minimumDate: NSDate(), maximumDate: nil)
            
            DateInputView.addDatePickerForTextField(textField)
            
        } else {
            textField.keyboardType = keyboardType
        }
        
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        
        self.completion = completion
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.addSubview(self)
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    func showDatePopupWithTitle(_ title: String?, placeHolder : String?, minDate : Date?, maxDate : Date?, completion : @escaping (_ textField : UITextField, _ isEdited : Bool) -> Void) {
        textField.text = ""
        labelTitle.text = title
        self.textFormat = .date
        if let _ = placeHolder {
            textField.placeholder = placeHolder
        }
        DateInputView.addDatePickerForTextField(textField)
        self.completion = completion
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.addSubview(self)
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionOK(_ sender: AnyObject) {
        self.removeFromSuperview()
        completion?(self.textField, isEdited)
    }
    
    var isEdited : Bool {
        get {
            return !textField.isEmpty
        }
    }

}


extension PopupTextField : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if self.textFormat == .phone {
            return textField.formatPhoneNumber(range, string: string)
        } else if self.textFormat == .zipcode {
            return textField.formatZipCode(range, string: string)
        } else if self.textFormat == .number {
            return textField.formatNumbers(range, string: string, count: 3)
        } /* else if self.textFormat == .ToothNumber {
            return textField.formatToothNumbers(range, string: string)
        }*/ else if self.textFormat == .socialSecurity {
            return textField.formatSocialSecurityNumber(range, string: string)
        }
        return true
    }
    
    
}
