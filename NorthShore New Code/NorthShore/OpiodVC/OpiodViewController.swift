//
//  OpiodViewController.swift
//  North Shore
//
//  Created by SRS Web Solutions on 05/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class OpiodViewController: PDViewController {

    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let form = patient.selectedForms.last!.formTitle == kOpiodForm ? patient.selectedForms.last! : patient.selectedForms[patient.selectedForms.count - 2]
        if form.toothNumbers != nil {
            let string = "Please consider this information carefully before agreeing to take your \(form.toothNumbers!) prescription."
            let attString = NSMutableAttributedString(string: string)
            attString.addAttributes([NSUnderlineStyleAttributeName: 1], range: (string as NSString).range(of: form.toothNumbers!))
            labelDetails.attributedText = attString
        }

        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(OpiodViewController.labelDateTapped(_:)))
        tapgesture.numberOfTapsRequired = 1
        labelDate.addGestureRecognizer(tapgesture)
        
        labelName.text = "\(patient.fullName)"
        
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.signatureOpiod = signatureView.isSigned() ? signatureView.signatureImage() : nil
    }
    func loadValues() {
        signatureView.previousImage = patient.signatureOpiod
    }
    
    func labelDateTapped(_ sender: AnyObject) {
        self.labelDate.text = patient.dateToday
        self.labelDate.textColor = UIColor.black
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func nextBtnPressed(_ sender: AnyObject) {
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if labelDate.text == "Tap to Date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kOpiodFormViewController") as! OpiodFormViewController
            formVC.patient = self.patient
            navigationController?.pushViewController(formVC, animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
