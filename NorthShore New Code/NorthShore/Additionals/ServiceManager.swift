//
//  ServiceManager.swift
//  SecureDental
//
//  Created by Bose on 02/02/16.
//  Copyright © 2016 SecureDental. All rights reserved.
//

import UIKit


class ServiceManager: NSObject {
    
    class func fetchDataFromService(_ baseUrlString: String, serviceName: String, parameters : [String : String]?, success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : NSError) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: baseUrlString))
        
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.post(serviceName, parameters: parameters, progress: { (progress) in
            print(progress.fractionCompleted)
            }, success: { (task, result) in
                //                print(task)
                //                print(result)
                success(result as AnyObject)
        }) { (task, error) in
            //            print(task)
            //            print(error)
            failure(error as NSError)
        }
    }
    class func fetchDataFromService(_ baseUrlString: String, serviceName: String, parameters : [String : String]?, imageData: Data?, success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : NSError) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: baseUrlString))
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        
        manager.post(serviceName, parameters: parameters, constructingBodyWith: { (data) in
            if imageData != nil {
                data.appendPart(withFileData: imageData!, name: "task_data", fileName: "\(parameters!["patient_name"])_\(ServiceManager.date())_signature.jpeg", mimeType: "image/jpeg")
            }
            }, progress: { (progress) in
                
            }, success: { (task, result) in
                success(result as AnyObject)
        }) { (task, error) in
            failure(error as NSError)
        }
    }
    class func date() -> String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMM_dd_yyyy_hh_mm"
        return dateFormat.string(from: Date())
    }
    
    class func ChekInFormStatus(_ patientName: String, patientPurpose: String, signatureImage: UIImage?, completion: @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        //"mcdistinctivelaser"
        ServiceManager.fetchDataFromService("http://alpha.mncell.com/mconsent/", serviceName:"savepatients_info.php?", parameters: ["patientkey": "mcNorthShore", "patientname": patientName, "patientpurpose": patientPurpose], imageData: signatureImage == nil ? nil : UIImageJPEGRepresentation(signatureImage!, 0.2), success: { (result) in
            if (result["posts"] as! String == "success") {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"] as AnyObject)["message"] as! String]))
            }
        }) { (error) in
            completion(false, nil)
        }
    }
    class func postReview(_ name: String, comment: String, rating: CGFloat, phoneNumber: String, allowMessage: Bool, email: String, anonymous: Bool, completion: @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        //https://alpha.mncell.com/review/app_review.php
        ServiceManager.fetchDataFromService("https://alpha.mncell.com/review/", serviceName: "app_review.php", parameters: ["patient_appkey": "mcNorthShore", "patient_name": name, "patient_review_comment": comment, "patient_rating": "\(rating)", "patient_phone_number": phoneNumber, "sms_allowed": allowMessage == true ? "1" : "0", "patient_email": email, "user_anonymous": anonymous == true ? "1" : "0"], success: { (result) in
            if result["posts"] != nil && (result["posts"] as AnyObject)["status"] as! String == "success" {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"] as AnyObject)["message"] as! String]))
            }
        }) { (error) in
            completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong, Please Check your internet connection and try again"]))
        }
    }
    class func loginWithUsername(_ userName: String, password: String, completion: @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        ServiceManager.fetchDataFromService("http://mncell.com/mclogin/", serviceName: "apploginapi.php?", parameters: ["appkey": "mcnorth", "username": userName, "password": password], success: { (result) in
            if (result["posts"] as AnyObject)["status"] as! String == "success" {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"] as AnyObject)["message"] as! String]))
            }
        }) { (error) in
            completion(false, nil)
        }
    }
}
