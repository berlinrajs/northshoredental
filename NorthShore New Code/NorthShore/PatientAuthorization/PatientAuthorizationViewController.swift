//
//  PatientAuthorizationViewController.swift
//  North Shore
//
//  Created by Office on 2/21/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientAuthorizationViewController: PDViewController {

    @IBOutlet weak var textFieldFaxNumber: PDTextField!
    @IBOutlet weak var textFieldNewClinic: PDTextField!
    @IBOutlet weak var textFieldPatientNumber: PDTextField!
    @IBOutlet weak var textFieldPhoneNumber: PDTextField!
    @IBOutlet weak var textViewReasonForTransfer: UITextView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.previousClinicName = "North Shore"
        patient.newClinicName = textFieldNewClinic.text
        if !textFieldPatientNumber.isEmpty {
            patient.patientNumber = textFieldPatientNumber.text
        } else {
            patient.patientNumber = nil
        }
        if !textFieldFaxNumber.isEmpty {
            patient.faxNumber = textFieldFaxNumber.text
        } else {
            patient.faxNumber = nil
        }
        if textViewReasonForTransfer.text != "PLEASE TYPE REASON FOR TRANSFER" {
            patient.reasonForTransfer = textViewReasonForTransfer.text
        } else {
            patient.reasonForTransfer = nil
        }
        patient.phoneNumber = textFieldPhoneNumber.text
        patient.faxNumber = textFieldFaxNumber.text
    }
    func loadValues() {
        textFieldNewClinic.text = patient.newClinicName
        textFieldPatientNumber.text = patient.patientNumber
        textFieldFaxNumber.text = patient.faxNumber
        
        self.textViewDidBeginEditing(textViewReasonForTransfer)
        textViewReasonForTransfer.text = patient.reasonForTransfer
        self.textViewDidEndEditing(textViewReasonForTransfer)
        
        textFieldPhoneNumber.text = patient.phoneNumber
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if textFieldNewClinic.isEmpty {
            let alert = Extention.alert("PLEASE ENTER NEW CLINIC NAME")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldPhoneNumber.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldFaxNumber.isEmpty && !textFieldFaxNumber.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID FAX NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let releaseConfidentialVC = self.storyboard?.instantiateViewController(withIdentifier: "kReleaseConfidentialVC") as! ReleaseConfidentialViewController
                releaseConfidentialVC.patient = patient
            self.navigationController?.pushViewController(releaseConfidentialVC, animated: true)
        }
    }
}


extension PatientAuthorizationViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "PLEASE TYPE REASON FOR TRANSFER" {
            textView.text = ""
            textView.textColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "PLEASE TYPE REASON FOR TRANSFER"
            textView.textColor = UIColor.white.withAlphaComponent(0.5)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

extension PatientAuthorizationViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldPhoneNumber || textField == textFieldFaxNumber {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
