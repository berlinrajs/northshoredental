//
//  EndodonticTreatmentFormVC.swift
//  TotalHealthDental
//
//  Created by SRS Web Solutions on 06/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class EndodonticTreatmentFormVC: PDViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: FormLabel!
    @IBOutlet weak var labelToothNumber: UILabel!
    
    @IBOutlet weak var patientSignature: UIImageView!
    @IBOutlet weak var dentistSignature: UIImageView!
    @IBOutlet weak var witnessSignature: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelName.text = patient.fullName
        labelDate.text = patient.dateToday
        
        for form in patient.selectedForms {
            if form.formTitle == kEndodonticTreatment {
                labelToothNumber.text = form.toothNumbers
                break
            }
        }
        
        patientSignature.image = patient.endodonticPatientSignature
        dentistSignature.image = patient.endodonticdentistSignature
        witnessSignature.image = patient.endodonticwitnessSignature
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
