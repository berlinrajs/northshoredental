//
//  DentalHealthHistoryStep4VC.swift
//  NorthShore
//
//  Created by SRS Websolutions.com on 7/4/18.
//  Copyright © 2018 SRS. All rights reserved.
//

import UIKit

class DentalHealthHistoryStep4VC: PDViewController {

    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!
    @IBOutlet weak var labelTitle : UILabel!
    
    @IBOutlet var buttonNext: UIButton!
    @IBOutlet var buttonBack1: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelHeading : UILabel!
    
    var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
    }
    
    func loadValues() {
        self.labelTitle.text = "Do you have or ever had any of the following diseases or medical conditions?"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBackButton1Pressed (withSender sender: AnyObject) {
        if selectedIndex == 0 {
            let _ = self.navigationController?.popViewController(animated: true)
        } else {
            buttonBack1.isUserInteractionEnabled = false
            buttonNext.isUserInteractionEnabled = false
            
            self.activityIndicator.startAnimating()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                self.activityIndicator.stopAnimating()
                self.selectedIndex = self.selectedIndex - 1
                //                self.labelHeading.text = self.selectedIndex < 4 ? "MEDICAL HISTORY" : self.selectedIndex == 4 ? "ALLERGIES" : "DENTAL HISTORY"
                //                self.labelTitle.text = self.selectedIndex == 0 ? "" : self.selectedIndex < 4 ? "Have you ever had any of the following diseases or medical problems?" : self.selectedIndex == 4 ? "Are you allergic to any of the following" : ""
                self.tableViewQuestions.reloadData()
                self.buttonBack1.isUserInteractionEnabled = true
                self.buttonNext.isUserInteractionEnabled = true
            }
        }
    }
    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        
        if buttonVerified.isSelected == false {
            self.showAlert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            if selectedIndex == 2 {
                let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "DentalHealthHistoryStep5VC") as! DentalHealthHistoryStep5VC
                patientSignIn.patient = self.patient
                self.navigationController?.pushViewController(patientSignIn, animated: true)
                
            } else {
                buttonBack1.isUserInteractionEnabled = false
                buttonNext.isUserInteractionEnabled = false
                self.buttonVerified.isSelected = false
                self.activityIndicator.startAnimating()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                    self.activityIndicator.stopAnimating()
                    self.selectedIndex = self.selectedIndex + 1
                    //                    self.labelHeading.text = self.selectedIndex < 4 ? "MEDICAL HISTORY" : self.selectedIndex == 4 ? "ALLERGIES" : "DENTAL HISTORY"
                    //                    self.labelTitle.text = self.selectedIndex == 0 ? "" : self.selectedIndex < 4 ? "Have you ever had any of the following diseases or medical problems?" : self.selectedIndex == 4 ? "Are you allergic to any of the following" : ""
                    self.tableViewQuestions.reloadData()
                    self.buttonBack1.isUserInteractionEnabled = true
                    self.buttonNext.isUserInteractionEnabled = true
                }
            }
        }
        
        
    }
    
}



extension DentalHealthHistoryStep4VC : UITableViewDataSource,UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.medicalHistory.medicalQuestions[selectedIndex].count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 37
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PatientInfoCell
                        cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(question: self.patient.medicalHistory.medicalQuestions[selectedIndex][indexPath.row])
        return cell
    }
}

extension DentalHealthHistoryStep4VC : PatientInfoCellDelegate {

    func radioButtonTappedForCell(cell: PatientInfoCell) {
        if selectedIndex == 2 && (cell.tag == 16){
            
            PopupTextField.sharedInstance.showWithTitle( "OTHER" , placeHolder: "PLEASE SPECIFY" ,keyboardType: UIKeyboardType.default, textFormat: .default, completion: { (textField, isEdited) in
                if textField.isEmpty{
                    cell.radioButtonYes.setSelected(false)
                    self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].selectedOption = false
                }else{
                    self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].answer = textField.text
                }
            })

        }else{
//            PopupTextView.popUpView().showWithPlaceHolder("PLEASE LIST HERE", completion: { (popUp, textView) in
//                if textView.isEmpty{
//                    cell.radioButtonYes.setSelected(false)
//                    self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].selectedOption = false
//                }else{
//                    self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].answer = textView.text
//                }
//                popUp.close()
//            })
        }
    }
}
