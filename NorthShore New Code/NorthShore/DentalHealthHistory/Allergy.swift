//
//  Allergy.swift
//  DiamondDentalAuto
//
//  Created by Bala Murugan on 10/7/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class Allergy: NSObject {
    
    var allergyId : String!
    var allergyName : String!
    var allergyDescription: String!
    var isSelected : Bool! {
        willSet {
            if newValue == false {
                self.allergyDescription = "no"
            } else {
                self.allergyDescription = "yes"
            }
        }
    }
    
    
    init(details : AnyObject) {
        super.init()
        allergyId = details["alertId"] as? String
        allergyName = details["alertName"] as? String
        allergyDescription = "no"
        isSelected = false
    }
    
    override init() {
        super.init()
    }
    
    class func getAllData(_ arrayObjects : [AnyObject]?) -> [Allergy]? {
        guard let arrayObjects = arrayObjects,  arrayObjects.count > 0 else {
            // Value requirements not met, do something
            return nil
        }
        var allgys = [Allergy]()
        for dict in arrayObjects {
            let allgy = Allergy(details: dict)
            allgys.append(allgy)
        }
        return allgys
    }
    
//    class func getAllergyList( _ success:@escaping (_ result : [Allergy]) -> Void, failure :@escaping (_ error : Error?) -> Void) {
//        var accessToken = ""
//        if let _ = UserDefaults.standard.value(forKey: kAccessToken) as? String {
//            accessToken = UserDefaults.standard.value(forKey: kAccessToken) as! String
//        }
//        ServiceManager.fetchDataFromPostService("https://integrations.srswebsolutions.com/dxmaster/", serviceName: "medical-alerts.php", parameters: ["access_ip": hostUrl!, "access_token" : accessToken] as [String: AnyObject], success: { (result) in
//            let response : NSDictionary = result as! NSDictionary
//            if (response["status"] as! String) == "success" {
//                let allgy = Allergy.getAllData(response["alertvalues"] as? [AnyObject])
//                if let _ = allgy {
//                    success(allgy!)
//                } else {
//                    failure(NSError(errorMessage: "No data found") as Error)
//                }
//            }else{
//                failure(NSError(errorMessage: "No data found") as Error)
//            }
//        }) { (error) in
//            failure(error)
//        }
//    }
//    
//    
//    class func updateAllergyList(_ params : [String: AnyObject], success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : Error?) -> Void) {
//        ServiceManager.fetchDataFromPostService("https://integrations.srswebsolutions.com/dxmaster/", serviceName: "update_medical_alert.php", parameters: params , success: { (result) in
//            if (result["status"] as! String) == "success" {
//                success(result)
//            } else {
//                failure(NSError(errorMessage: "Something went wrong please try again later") as Error)
//            }
//        }) { (error) in
//            failure(error)
//        }
//    }
    
    
}
