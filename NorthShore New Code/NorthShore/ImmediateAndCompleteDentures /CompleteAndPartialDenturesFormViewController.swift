//
//  CompleteAndPartialDenturesFormViewController.swift
//  AL-ShifaDentistry
//
//  Created by SRS Web Solutions on 21/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CompleteAndPartialDenturesFormViewController: PDViewController {
    
    @IBOutlet var labelName: UILabel!
    @IBOutlet var imageViewSignature: UIImageView!
    @IBOutlet var imageViewSignature2: UIImageView!
    @IBOutlet var labelDate1: UILabel!
    @IBOutlet var labelDate2: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewSignature.image = patient.ImmediateSignature1
        imageViewSignature2.image = patient.ImmediateSignature2
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelName.text = patient.fullName


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
