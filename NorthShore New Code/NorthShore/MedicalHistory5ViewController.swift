//
//  MedicalHistoryStep1ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistory5ViewController: PDViewController {

    var medicalHistoryStep5 : [PDQuestion]! = [PDQuestion]()
    var selectedButton : RadioButton!
    var fetchCompleted : Bool! = false
    
    @IBOutlet weak var OtheralergicIssues: PDTextView!
    @IBOutlet weak var Areyouwoman: RadioButton!
    @IBOutlet weak var Areyoupregnant: RadioButton!
    @IBOutlet weak var Areyoutakebirthcontrol: RadioButton!
    @IBOutlet weak var Areyounursing: RadioButton!
    
    @IBOutlet weak var tableViewQuestions: UITableView!
    @IBOutlet var viewPopup: PDView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textViewAnswer: PDTextView!
    
    @IBOutlet weak var buttonVerified: UIButton!
    @IBOutlet weak var textFieldWeeks: PDTextField!
    @IBOutlet weak var womanView: PDView!
    @IBOutlet weak var Label1: UILabel!
    
    @IBOutlet weak var Label2: UILabel!
    @IBOutlet weak var Label3: UILabel!
    @IBOutlet weak var Label4: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.OtheralergicIssuesValue  = OtheralergicIssues.text
        patient.womanWeeks = textFieldWeeks.text
        
        patient.AreyouwomanTag = Areyouwoman.selected.tag
        patient.AreyoupregnantTag = Areyoupregnant.selected.tag
        patient.AreyoutakebirthcontrolTag = Areyoutakebirthcontrol.selected.tag
        patient.AreyounursingTag = Areyounursing.selected.tag
        
        patient.medicalHistoryQuestions5 = self.medicalHistoryStep5
    }
    func loadValues() {
        if patient.medicalHistoryQuestions5 == nil {
            fetchData()
        } else {
            self.medicalHistoryStep5 = patient.medicalHistoryQuestions5
            self.fetchCompleted = true
            self.tableViewQuestions.reloadData()
        }
        
        self.textViewDidBeginEditing(OtheralergicIssues)
        OtheralergicIssues.text = patient.OtheralergicIssuesValue
        self.textViewDidEndEditing(OtheralergicIssues)
        
        textFieldWeeks.text = patient.womanWeeks
        
        Areyouwoman.setSelectedWithTag(patient.AreyouwomanTag == nil ? 0 : patient.AreyouwomanTag)
        Areyoupregnant.setSelectedWithTag(patient.AreyoupregnantTag == nil ? 0 : patient.AreyoupregnantTag)
        Areyoutakebirthcontrol.setSelectedWithTag(patient.AreyoutakebirthcontrolTag == nil ? 0 : patient.AreyoutakebirthcontrolTag)
        Areyounursing.setSelectedWithTag(patient.AreyounursingTag == nil ? 0 : patient.AreyounursingTag)
        
        if Areyouwoman.selected != nil && Areyouwoman.selected.tag == 1 {
            self.womanView.isUserInteractionEnabled = true
            self.womanView.backgroundColor = UIColor.clear
            
            self.Label1.textColor = UIColor.white.withAlphaComponent(1)
            self.Label2.textColor = UIColor.white.withAlphaComponent(1)
            self.Label3.textColor = UIColor.white.withAlphaComponent(1)
            self.Label4.textColor = UIColor.white.withAlphaComponent(1)
            self.textFieldWeeks.borderColor = UIColor.white.withAlphaComponent(0.8)
        } else {
            self.womanView.isUserInteractionEnabled = false
            self.womanView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            
            self.textFieldWeeks.borderColor = UIColor.white.withAlphaComponent(0.2)
            self.Label1.textColor = UIColor.white.withAlphaComponent(0.2)
            self.Label2.textColor = UIColor.white.withAlphaComponent(0.2)
            self.Label3.textColor = UIColor.white.withAlphaComponent(0.2)
            self.Label4.textColor = UIColor.white.withAlphaComponent(0.2)
            
            textFieldWeeks.text = ""
            patient.womanWeeks = ""
        }
    }
    func fetchData() {
        PDQuestion.fetchQuestionsMedicalForm5 { (result, success) -> Void in
            self.fetchCompleted = true
            if success {
                self.medicalHistoryStep5.append(contentsOf: result!)
                self.tableViewQuestions.reloadData()
            }
        }
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if self.fetchCompleted == true {
            if !buttonVerified.isSelected {
                let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
                self.present(alert, animated: true, completion: nil)
            } else {
                
                saveValues()
                
                let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
                let MedicalHistory6 = storyboard.instantiateViewController(withIdentifier: "kMedicalHistoryStep6VC") as! MedicalHistoryStep6ViewController
                MedicalHistory6.patient = patient
                self.navigationController?.pushViewController(MedicalHistory6, animated: true)
            }
        }
    }
    @IBAction func AreYouPregnantAction(_ sender: AnyObject) {
        if sender.tag == 2 {
            textFieldWeeks.isEnabled = true
        } else {
            textFieldWeeks.text = ""
            textFieldWeeks.isEnabled = false
        }
    }

    @IBAction func AreYouWoman(_ sender: AnyObject) {
        if sender.tag == 1 {
            self.womanView.isUserInteractionEnabled = true
            self.womanView.backgroundColor = UIColor.clear
          
            self.Label1.textColor = UIColor.white.withAlphaComponent(1)
            self.Label2.textColor = UIColor.white.withAlphaComponent(1)
            self.Label3.textColor = UIColor.white.withAlphaComponent(1)
            self.Label4.textColor = UIColor.white.withAlphaComponent(1)
            self.textFieldWeeks.borderColor = UIColor.white.withAlphaComponent(0.8)
        } else {
            self.womanView.isUserInteractionEnabled = false
            self.womanView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            
            self.textFieldWeeks.borderColor = UIColor.white.withAlphaComponent(0.2)
            self.Label1.textColor = UIColor.white.withAlphaComponent(0.2)
            self.Label2.textColor = UIColor.white.withAlphaComponent(0.2)
            self.Label3.textColor = UIColor.white.withAlphaComponent(0.2)
            self.Label4.textColor = UIColor.white.withAlphaComponent(0.2)
            
            textFieldWeeks.text = ""
            patient.womanWeeks = ""
        }
    }
    
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func buttonActionPopupOk(_ sender: AnyObject) {
        
        let obj = medicalHistoryStep5[selectedButton.tag]
        if !textViewAnswer.isEmpty && textViewAnswer.text != "IF YES TYPE HERE" {
            obj.answer = textViewAnswer.text
            obj.selectedOption = true
        } else {
            selectedButton.isSelected = false
            obj.selectedOption = false
        }
        textViewAnswer.resignFirstResponder()
        self.viewPopup.removeFromSuperview()
        self.viewShadow.isHidden = true
    }
    
    func findEmptyValue() -> PDQuestion? {
        for question in self.medicalHistoryStep5 {
            if question.selectedOption == nil {
                return question
            }
        }
        return nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MedicalHistory5ViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

extension MedicalHistory5ViewController : MedicalHistoryCellDelegate {
   
    func radioButtonAction(_ sender: RadioButton) {
        selectedButton = sender
         let obj = medicalHistoryStep5[selectedButton.tag]
        obj.selectedOption = true
    }
}


extension MedicalHistory5ViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return medicalHistoryStep5.count
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryStep1", for: indexPath) as! MedicalHistoryStep1TableViewCell
        let obj = medicalHistoryStep5[indexPath.row]
        cell.configureCell(obj)
        cell.buttonYes.tag = indexPath.row
        cell.buttonNo.tag = indexPath.row
        cell.delegate = self
        return cell
        
        
    }
}

extension MedicalHistory5ViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField ==  textFieldWeeks{
            return textField.formatWeeks(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}




