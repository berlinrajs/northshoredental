//
//  ContactInfoViewController.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ContactInfoViewController: PDViewController {

    @IBOutlet weak var textFieldEmployer: PDTextField!

    @IBOutlet weak var textFieldHowLongthere: PDTextField!
    @IBOutlet weak var textFieldState: PDTextField!
  
    @IBOutlet weak var textFiledzip: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldAddress: PDTextField!
    @IBOutlet weak var textFieldOccupation: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
           StateListView.addStateListForTextField(textFieldState)  
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.ContactZip = textFiledzip.text
        patient.ContactCity = textFieldCity.text
        patient.ContactState = textFieldState.text
        patient.ContactemployerName = textFieldEmployer.text
        patient.HowLongThere = textFieldHowLongthere.text
        patient.ContactOccupation = textFieldOccupation.text
        patient.ContactAddress = textFieldAddress.text
    }
    func loadValues() {
        textFiledzip.text = patient.ContactZip
        textFieldCity.text = patient.ContactCity
        textFieldState.text = patient.ContactState == nil || patient.ContactState.isEmpty ? "IL" : patient.ContactState
        textFieldEmployer.text = patient.ContactemployerName
        textFieldHowLongthere.text = patient.HowLongThere
        textFieldOccupation.text = patient.ContactOccupation
        textFieldAddress.text = patient.ContactAddress
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonActionNext(_ sender: AnyObject) {
        
        self.view.endEditing(true)
        if !textFiledzip.isEmpty && !textFiledzip.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID ZIP CODE")
            self.present(alert, animated: true, completion: nil)
        } else {
            
            saveValues()
            YesOrNoAlert.sharedInstance.showWithTitle("DO YOU HAVE A INSURANCE POLICY", button1Title: "YES", button2Title: "NO", completion: { (buttonIndex) in
                if buttonIndex == 1 {
                    let info7VC = self.storyboard?.instantiateViewController(withIdentifier: "kPrimaryInsuranceVC") as! PrimaryInsuranceVC
                    info7VC.patient = self.patient
                    self.navigationController?.pushViewController(info7VC, animated: true)
                } else {
                    self.patient.resetPrimaryInsurance()
                   // self.patient.resetSecondaryInsurance()
                    let diseaseInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kResponsiblePersonVC") as! ResponsiblePersonVC
                    diseaseInfoVC.patient = self.patient
                    self.navigationController?.pushViewController(diseaseInfoVC, animated: true)
                }
            })

            
//            let diseaseInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kEmergencyContactInfoVC") as! EmergencyContactInfoViewController
//            diseaseInfoVC.patient = patient
//            self.navigationController?.pushViewController(diseaseInfoVC, animated: true)
        }
    }
    


}
extension ContactInfoViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textFiledzip {
            return textField.formatZipCode(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
