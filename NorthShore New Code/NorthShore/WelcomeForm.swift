//
//  WelcomeForm.swift
//  NorthShore
//
//  Created by SRS on 25/07/16.
//  Copyright © 2016 SRS. All rights reserved.
//
import UIKit

class WelcomeForm: PDViewController {
    
    
    @IBOutlet weak var patientInfo: AdultPatientInformation!
    @IBOutlet weak var patientInfo1: AdultPatientInformation1!
    @IBOutlet weak var patientInfo2: AdultPatientInformation2!
    @IBOutlet weak var patientInfo3: AdultPatientInformation3!
    @IBOutlet weak var patientInfo4: AdultPatientInformation4!
    
    @IBOutlet weak var patientName: FormLabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var viewPage2: UIView!
    @IBOutlet weak var viewPage4: UIView!
    @IBOutlet var viewPage3: UIView!
    @IBOutlet weak var Date: FormLabel!
    @IBOutlet weak var patientSocialSecurity: FormLabel!
    @IBOutlet weak var patientBdate: FormLabel!
    @IBOutlet weak var patientPreferedName: FormLabel!
    @IBOutlet weak var patientSex: RadioButton!
    @IBOutlet weak var patientAge: FormLabel!
    @IBOutlet weak var patientAddress: FormLabel!
    @IBOutlet weak var patientCity: FormLabel!
    @IBOutlet weak var patientState: FormLabel!
    @IBOutlet weak var patientZip: FormLabel!
    @IBOutlet weak var patientEmail: FormLabel!
    @IBOutlet weak var patientHomePhone: FormLabel!
    @IBOutlet weak var patientWorkPhone: FormLabel!
    @IBOutlet weak var patientDriverLicense: FormLabel!
    @IBOutlet weak var patientCellPhone: FormLabel!
    @IBOutlet weak var patientStatus: RadioButton!
    @IBOutlet weak var patientReachTime: FormLabel!
    @IBOutlet weak var patientThankforReferring: FormLabel!
    @IBOutlet weak var otherFamilyMembers: FormLabel!
    @IBOutlet weak var patientEmployer: FormLabel!
    @IBOutlet weak var patientHowLongThere: FormLabel!
    @IBOutlet weak var patientOccupation: FormLabel!
    @IBOutlet weak var patientEmpAddress: FormLabel!
    @IBOutlet weak var patientEmpCity: FormLabel!
    @IBOutlet weak var patientEmpState: FormLabel!
    @IBOutlet weak var patientEmpZip: FormLabel!
    
    @IBOutlet weak var RespPersonName: FormLabel!
    @IBOutlet weak var RespPersonSocialSecurity: FormLabel!
    @IBOutlet weak var RespPersonRelation: FormLabel!
    @IBOutlet weak var RespPersonHomePhone: FormLabel!
    @IBOutlet weak var RespPersonWorkPhone: FormLabel!
    @IBOutlet weak var RespPersonDriverLicense: FormLabel!
    @IBOutlet weak var RespPersonAddress: FormLabel!
    @IBOutlet weak var RespPersonCity: FormLabel!
    @IBOutlet weak var RespPersonState: FormLabel!
    @IBOutlet weak var RespPersonZip: FormLabel!
    
    //SPOUSE INFORMATION
    @IBOutlet weak var SpouseName: FormLabel!
    @IBOutlet weak var SpouseSocialSecurity: FormLabel!
    @IBOutlet weak var SpouseBdate: FormLabel!
    
    //INSURANCE - PRIMARY
    @IBOutlet weak var PrimaryInsuranceDentalCoverage: RadioButton!
    @IBOutlet weak var PrimaryInsuranceMedCoverage: RadioButton!
    @IBOutlet weak var PrimaryInsuranceOrthodonticCoverage: RadioButton!
    @IBOutlet weak var PrimaryInsuranceCoName: FormLabel!
    @IBOutlet weak var PrimaryInsuranceCoPhone: FormLabel!
    @IBOutlet weak var PrimaryInsuranceCoAddress: FormLabel!
    @IBOutlet weak var PrimaryInsuranceCoCity: FormLabel!
    @IBOutlet weak var PrimaryInsuranceCoState: FormLabel!
    @IBOutlet weak var PrimaryInsuranceCoZip: FormLabel!
    @IBOutlet weak var PrimaryInsuraceInsuredsName: FormLabel!
    @IBOutlet weak var PrimaryInsuranceInsuredsRelation: FormLabel!
    @IBOutlet weak var PrimaryInsuranceInsuredsBdate: FormLabel!
    @IBOutlet weak var PrimaryInsuranceInsuredsSS: FormLabel!
    
    // DENTAL HISTORY
    @IBOutlet weak var WhyHaveCometoDentistry: FormLabel!
    @IBOutlet weak var YourDentalHealth: RadioButton!
    @IBOutlet weak var TypesOfBristles: RadioButton!
    @IBOutlet var doYouHaveWisdomteeth: [RadioButton]!
    @IBOutlet weak var WisdomTeethYes: FormLabel!
    @IBOutlet weak var PreviousDentist: FormLabel!
    @IBOutlet weak var LastVisitDate: FormLabel!
    @IBOutlet weak var YLeavePreviousDentist: FormLabel!
    @IBOutlet weak var WhatdidyoulikeMost: FormLabel!
    @IBOutlet weak var HappyWithSmile: RadioButton!
    @IBOutlet weak var IfnotHappywithSmile: FormLabel!
    @IBOutlet weak var AreyouTeethSensitve: FormLabel!
    
    //MEDICAL HISTORY
    @IBOutlet weak var DoyouhavePhysician: RadioButton!
    @IBOutlet weak var YourCurrentPhysicalHealth: RadioButton!
    @IBOutlet var buttonList: [RadioButton]!
    @IBOutlet weak var AreyouCurrentlyUnderPhysicianCare: RadioButton!
    @IBOutlet weak var IfYesUnderPhysicianCare: FormLabel!
    @IBOutlet weak var LastPhysicianVisiit: FormLabel!
    @IBOutlet weak var PhysiciansName: FormLabel!
    @IBOutlet weak var PhysicianAddress: FormLabel!
    @IBOutlet weak var PhysicianPhone: FormLabel!
    @IBOutlet weak var PleaseListadditionalDrugs: FormLabel!
    
    // For Women
    @IBOutlet weak var AreyouTakingPills: RadioButton!
    @IBOutlet weak var AreyouPregnant: RadioButton!
    @IBOutlet weak var WomenWeek: FormLabel!
    @IBOutlet weak var AreYouNursing: RadioButton!
    //financial polyicy
    @IBOutlet weak var financialDate2: FormLabel!
    @IBOutlet weak var financialSign2: UIImageView!
    @IBOutlet weak var financialDate1: FormLabel!
    @IBOutlet weak var financialSign1: UIImageView!
    //ALL PATIENT
    
    @IBOutlet weak var AllPatientSign: UIImageView!
    @IBOutlet weak var AllpatientDate: FormLabel!
    //APPOINMENT POLICY
    @IBOutlet weak var AppoinmentSign: UIImageView!
    @IBOutlet weak var AppoinmentDate: FormLabel!
    // NOTICE OF PRIVACY
    @IBOutlet weak var noticeDate: FormLabel!
    @IBOutlet weak var NoticeSign: UIImageView!
    @IBOutlet weak var NoticePatientName: FormLabel!
    //AUTHORIZATIONS..
    @IBOutlet weak var paymentMethod: FormLabel!
    @IBOutlet weak var signatureView1: UIImageView!
    @IBOutlet weak var date1: FormLabel!
    @IBOutlet weak var signature2: UIImageView!
    @IBOutlet weak var date2: FormLabel!
    @IBOutlet weak var insuranceCoName: FormLabel!
    @IBOutlet weak var Doctorname: FormLabel!
    //Are you taking following
    @IBOutlet weak var Takingotherdrugs: FormLabel!
    @IBOutlet weak var additionalDrugs: FormLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pdfView = self.scrollView
        loadValues()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        let frame = CGRect(x: 0, y: 1024, width: screenSize.width, height: screenSize.height)
        viewPage2.frame = frame
        scrollView.contentSize = CGSize(width: screenSize.width, height: screenSize.height * 2)
        scrollView.addSubview(viewPage2)
        
        let frame1 = CGRect(x: 0, y: 2048, width: screenSize.width, height: screenSize.height)
        viewPage3.frame = frame1
        scrollView.contentSize = CGSize(width: screenSize.width, height: screenSize.height * 3)
        scrollView.addSubview(viewPage3)
        
        let frame2 = CGRect(x: 0, y: 3072, width: screenSize.width, height: screenSize.height)
        viewPage4.frame = frame2
        scrollView.contentSize = CGSize(width: screenSize.width, height: screenSize.height * 4)
        scrollView.addSubview(viewPage4)
    }

    func loadValues() {
        (buttonList as NSArray).enumerateObjects({ (btn, idx, stop) in
            let tag = (btn as AnyObject).tag - 1
            let obj = self.patient.medicalHistoryQuestions3[tag]
            (btn as! UIButton).isSelected = obj.selectedOption
            
        })
        patientName.text = patient.fullName
        Date.text = patient.dateToday.isEmpty ? "N/A" : patient.dateToday
        patientSocialSecurity.text = !patient.socialSecurityNumber.isSocialSecurityNumber ? "N/A" : patient.socialSecurityNumber.socialSecurityNumber
        patientBdate.text = patient.dateOfBirth.isEmpty ? "N/A" : patient.dateOfBirth
        patientPreferedName.text = patient.preferredName.isEmpty ? "N/A" : patient.preferredName
        if patient.patientGender != nil {
            patientSex.setSelectedWithTag(patient.patientGender!)
        } else {
            patientSex.deselectAllButtons()
        }
        patientAge.text = String(patient.patientAge)
        patientAddress.text = patient.addressLine.isEmpty ? "N/A" : patient.addressLine
        patientCity.text = patient.city.isEmpty ? "N/A" : patient.city
        patientState.text = patient.state.isEmpty ? "N/A": patient.state
        patientZip.text = patient.zipCode.isEmpty ? "N/A": patient.zipCode
        patientEmail.text = patient.email.isEmpty ? "N/A" : patient.email
        patientHomePhone.text = patient.HomephoneNumber.isEmpty ? "N/A" : patient.HomephoneNumber
        patientWorkPhone.text = patient.workphoneNumber.isEmpty ? "N/A" : patient.workphoneNumber
        patientDriverLicense.text = patient.drivingLicense.isEmpty ? "N/A" : patient.drivingLicense
        patientCellPhone.text = patient.AddrcellphoneNumber.isEmpty ? "N/A" : patient.AddrcellphoneNumber
        if patient.maritialStatus != nil {
            patientStatus.setSelectedWithTag(patient.maritialStatus!)
        } else {
            patientStatus.deselectAllButtons()
        }
        
        patientReachTime.text = patient.Timetomeet.isEmpty ? "N/A" : patient.Timetomeet
        patientThankforReferring.text = patient.radioHearAboutUsTag == nil ? "N/A" : patient.radioHearAboutUsTag == 9 ? patient.othersDetails : patient.radioHearAboutUsTag == 8 ? patient.referredByDetails : patient.radioHearAboutUsTag == 0 ? "N/A" : (["YELLOW PAGES", "GOOGLE","YAHOO", "DEXKNOWS", "WALK IN/DRIVE BY", "INSURANCE", "MAILER"])[patient.radioHearAboutUsTag! - 1]
        otherFamilyMembers.text = patient.OtherFamilymember.isEmpty ? "N/A" : patient.OtherFamilymember
        patientEmployer.text = patient.ContactemployerName.isEmpty ? "N/A" : patient.ContactemployerName
        patientHowLongThere.text = patient.HowLongThere.isEmpty ? "N/A" : patient.HowLongThere
        patientOccupation.text = patient.ContactOccupation.isEmpty ? "N/A" : patient.ContactOccupation
        patientEmpAddress.text = patient.ContactAddress.isEmpty ? "N/A" : patient.ContactAddress
        patientEmpCity.text = patient.ContactCity.isEmpty ? "N/A" : patient.ContactCity
        patientEmpState.text = patient.ContactState.isEmpty ? "N/A" : patient.ContactState
        patientEmpZip.text = patient.ContactZip.isEmpty ? "N/A" : patient.ContactZip
        
        //person responsible for account
        RespPersonName.text = patient.RespContactName.isEmpty ? "N/A" : patient.RespContactName
        RespPersonSocialSecurity.text = !patient.RespSocialSecurity.isSocialSecurityNumber ? "N/A" : patient.RespSocialSecurity.socialSecurityNumber
        RespPersonRelation.text = patient.RespRelationName.isEmpty ? "N/A" : patient.RespRelationName
        RespPersonHomePhone.text = patient.RespHomephoneNumber.isEmpty ? "N/A" : patient.RespHomephoneNumber
        RespPersonWorkPhone.text = patient.RespworkphoneNumber.isEmpty ? "N/A" : patient.RespworkphoneNumber
        RespPersonDriverLicense.text = patient.RespDrivierlicense.isEmpty ? "N/A" : patient.RespDrivierlicense
        RespPersonAddress.text = patient.RespAddress.isEmpty ? "N/A" : patient.RespAddress
        RespPersonCity.text = patient.Respcity.isEmpty ? "N/A" : patient.Respcity
        RespPersonState.text = patient.RespState.isEmpty ? "N/A" : patient.RespState
        RespPersonZip.text = patient.RespZip.isEmpty ? "N/A" : patient.RespZip
        
        // PRIMARY INSURANCE..
        if patient.dentalCoverageBtn != nil {
            PrimaryInsuranceDentalCoverage.setSelectedWithTag(patient.dentalCoverageBtn!)
        } else {
            PrimaryInsuranceDentalCoverage.deselectAllButtons()
        }
        if patient.OrthodonticCoverageBtn != nil {
            PrimaryInsuranceMedCoverage.setSelectedWithTag(patient.OrthodonticCoverageBtn!)
        } else {
            PrimaryInsuranceMedCoverage.deselectAllButtons()
        }
        if patient.medicalCoverageBtn != nil {
            PrimaryInsuranceOrthodonticCoverage.setSelectedWithTag(patient.medicalCoverageBtn!)
        } else {
            PrimaryInsuranceOrthodonticCoverage.deselectAllButtons()
        }
        PrimaryInsuranceCoName.text = patient.primaryInsuranceCompanyName.isEmpty ? "N/A" : patient.primaryInsuranceCompanyName
        PrimaryInsuranceCoPhone.text = patient.primaryPhone!.isEmpty ? "N/A" : patient.primaryPhone
        PrimaryInsuranceCoAddress.text = patient.primaryInsuranceCoAddress.isEmpty ? "N/A" : patient.primaryInsuranceCoAddress
        PrimaryInsuranceCoCity.text = patient.primaryInsuranceCoCity.isEmpty ? "N/A" : patient.primaryInsuranceCoCity
        PrimaryInsuranceCoState.text = patient.primaryInsuraceCoState.isEmpty ? "N/A" : patient.primaryInsuraceCoState
        PrimaryInsuranceCoZip.text = patient.primaryInsuranceCozip.isEmpty ? "N/A" : patient.primaryInsuranceCozip
        PrimaryInsuraceInsuredsName.text = patient.primaryInsuredPersonname.isEmpty ? "N/A" : patient.primaryInsuredPersonname
        PrimaryInsuranceInsuredsRelation.text = patient.primaryRelationship.isEmpty ? "N/A" : patient.primaryRelationship
        PrimaryInsuranceInsuredsBdate.text = patient.primaryInsuredDOB.isEmpty ? "N/A" : patient.primaryInsuredDOB
        PrimaryInsuranceInsuredsSS.text = patient.primarySocialSecurity.isEmpty ? "N/A" : !patient.primarySocialSecurity!.isSocialSecurityNumber ? "N/A" : patient.primarySocialSecurity!.socialSecurityNumber
        
        //SPOUSE INFORMATION
        SpouseName.text = patient.SpouseContactName == nil  ? "N/A" : patient.SpouseContactName
        SpouseSocialSecurity.text = patient.SpouseSocialSecurity == nil  ? "N/A" : !patient.SpouseSocialSecurity.isSocialSecurityNumber ? "N/A" : patient.SpouseSocialSecurity.socialSecurityNumber
        SpouseBdate.text = patient.SpouseDateofbirth == nil  ? "N/A" : patient.SpouseDateofbirth
        
        // DENTAL HISTORY
        WhyHaveCometoDentistry.text = patient.YCometoDentistry
        AreyouTeethSensitve.text =  patient.ToothSensitive
        if patient.CurrentDentalTag != nil {
            YourDentalHealth.setSelectedWithTag(patient.CurrentDentalTag!)
        } else {
            YourDentalHealth.deselectAllButtons()
        }
        if patient.TypesOfBristleTag != nil {
            TypesOfBristles.setSelectedWithTag(patient.TypesOfBristleTag!)
        } else {
            TypesOfBristles.deselectAllButtons()
        }
        
        // WISDOM TEETH FROM TABLEVIEW MEDIACAL HISTORY 1
        (doYouHaveWisdomteeth as NSArray).enumerateObjects({ (btn, idx, stop) in
            let obj = self.patient.medicalHistoryQuestions1[13]
            (btn as! UIButton).isSelected = obj.selectedOption
            
        })
        WisdomTeethYes.text = patient.wisdomTeethReason
        PreviousDentist.text = patient.PreviousDentist == nil  ? "N/A" : patient.PreviousDentist
        LastVisitDate.text = patient.LastDentistVisit == nil  ? "N/A" : patient.LastDentistVisit
        YLeavePreviousDentist.text = patient.YLeavePreviousDentist == "TYPE HERE"  ? "N/A" : patient.YLeavePreviousDentist
        WhatdidyoulikeMost.text = patient.WhatLikeAnyDentist == "TYPE HERE"  ? "N/A" : patient.WhatLikeAnyDentist
        if patient.YouLikeyourSmileTag != nil {
            HappyWithSmile.setSelectedWithTag(patient.YouLikeyourSmileTag!)
        } else {
            HappyWithSmile.deselectAllButtons()
        }
        IfnotHappywithSmile.text =  patient.SmileReasonPopup == nil  ? "N/A" : patient.SmileReasonPopup
        
        //MEDICAL HISTORY
        if patient.PersonalPhysicianTag != nil {
            DoyouhavePhysician.setSelectedWithTag(patient.PersonalPhysicianTag!)
        } else {
            DoyouhavePhysician.deselectAllButtons()
        }
        
        if patient.yourMedicalHealthTag != nil {
            
            YourCurrentPhysicalHealth.setSelectedWithTag(patient.yourMedicalHealthTag!)
        } else {
            YourCurrentPhysicalHealth.deselectAllButtons()
        }
        
        IfYesUnderPhysicianCare.text = patient.ifUnderPhysicanCare
        LastPhysicianVisiit.text = patient.MedicalPhysicanVisit == nil  ? "N/A" : patient.MedicalPhysicanVisit
        PhysiciansName.text = patient.MedicalPhysicianName == nil  ? "N/A" : patient.MedicalPhysicianName
        PhysicianAddress.text = patient.MedicalPhysicianAddress == nil  ? "N/A" : patient.MedicalPhysicianAddress
        PhysicianPhone.text = patient.MedicalPhysicianPhone == nil  ? "N/A" : patient.MedicalPhysicianPhone
        PleaseListadditionalDrugs.text = patient.SmileReasonPopup == nil  ? "N/A" : patient.SmileReasonPopup
        
        if patient.AreyoutakebirthcontrolTag != nil {
            
            AreyouTakingPills.setSelectedWithTag(patient.AreyoutakebirthcontrolTag!)
        } else {
            AreyouTakingPills.deselectAllButtons()
        }
        
        if patient.AreyoupregnantTag != nil {
            
            AreyouPregnant.setSelectedWithTag(patient.AreyoupregnantTag!)
            
        } else {
            
            AreyouPregnant.deselectAllButtons()
            
        }
        
        if patient.AreyounursingTag != nil {
            
            AreYouNursing.setSelectedWithTag(patient.AreyounursingTag!)
            
        } else {
            AreYouNursing.deselectAllButtons()
        }
        
        additionalDrugs.text = patient.OtheralergicIssuesValue == "TYPE HERE"  ? "N/A" : patient.OtheralergicIssuesValue
        WomenWeek.text = patient.womanWeeks == nil  ? "N/A" : patient.womanWeeks
        
        // Authorization..
        financialDate2.text = patient.dateToday
        financialSign2.image = patient.Agreementsignature1
        financialDate1.text = patient.dateToday
        financialSign1.image = patient.Agreementsignature2
        
        //ALL PATIENT
        AllPatientSign.image = patient.Hippa1signature1
        AllpatientDate.text = patient.dateToday
        
        //APPOINMENT POLICY
        AppoinmentSign.image = patient.Hippa2signature1
        AppoinmentDate.text = patient.dateToday
        
        // NOTICE OF PRIVACY
        noticeDate.text = patient.dateToday
        NoticeSign.image = patient.Hippa3signature1
        NoticePatientName.text = patient.fullName
        
        //AUTHORIZATIONS..
        paymentMethod.text = patient.MethodOfPayment == nil  ? "N/A" : patient.MethodOfPayment
        signatureView1.image = patient.medicalHis7signature1
        date1.text = patient.dateToday
        signature2.image = patient.medicalHis7signature2
        date2.text = patient.dateToday
        insuranceCoName.text = patient.primaryInsuranceCompanyName == nil  ? "N/A" : patient.primaryInsuranceCompanyName
        Doctorname.text = patient.doctorName
        
        //Are you taking following
        Takingotherdrugs.text = patient.takingAnyPrescriptionPopup == nil  ? "N/A" : patient.takingAnyPrescriptionPopup
        patientInfo.arrayOfQuestions = patient.medicalHistoryQuestions1
        patientInfo1.arrayOfQuestions = patient.medicalHistoryQuestions5
        patientInfo2.arrayOfQuestions = patient.medicalHistoryQuestions3
        patientInfo3.arrayOfQuestions  = patient.medicalHistoryQuestions6
        patientInfo4.arrayOfQuestions = patient.medicalHistoryQuestions4
    }
}
















