//
//  NewPatient3ViewController.swift
//  YourCaringDentist
//
//  Created by Bala Murugan on 6/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PrimaryInsuranceVC: PDViewController {
    
    @IBOutlet weak var textfieldInsuranceCompanyName : PDTextField!
    @IBOutlet weak var textfieldPhone : PDTextField!
    @IBOutlet weak var textfieldGroup : PDTextField!
    @IBOutlet weak var textfieldInsuranceCoAddress : PDTextField!
    @IBOutlet weak var textFieldInsuranceCoCity: PDTextField!
    @IBOutlet weak var textFieldInsuraceCoState: PDTextField!
    @IBOutlet weak var textFieldInsuranceCozip: PDTextField!
    @IBOutlet weak var textFieldInsuredname: PDTextField!
    @IBOutlet weak var textFieldSocialSecurity: PDTextField!
    
    @IBOutlet weak var textFieldInsuredDOB: PDTextField!
    @IBOutlet weak var textfieldRelationship : PDTextField!
    
    @IBOutlet weak var dentalCoverage: RadioButton!
    @IBOutlet weak var orthodonticCoverage: RadioButton!
    @IBOutlet weak var medicalCoverage: RadioButton!
    @IBOutlet weak var insuredPersonBtn: RadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        StateListView.addStateListForTextField(textFieldInsuraceCoState)
        
        DateInputView.addDatePickerForDateOfBirthTextField(textFieldInsuredDOB)
        
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.primaryInsuranceCompanyName = textfieldInsuranceCompanyName.text
        patient.primaryPhone = textfieldPhone.text
        patient.primaryGroup = textfieldGroup.text
        patient.primaryInsuranceCoAddress = textfieldInsuranceCoAddress.text
        patient.primaryInsuranceCoCity = textFieldInsuranceCoCity.text
        patient.primaryInsuraceCoState = (!textfieldInsuranceCoAddress.isEmpty || !textFieldInsuranceCoCity.isEmpty || !textFieldInsuranceCozip.isEmpty) ? textFieldInsuraceCoState.text : ""
        patient.primaryInsuranceCozip = textFieldInsuranceCozip.text
        
        patient.primaryInsuredPersonname = textFieldInsuredname.text
        
        patient.primarySocialSecurity = textFieldSocialSecurity.text
        patient.primaryInsuredDOB = textFieldInsuredDOB.text
        patient.primaryRelationship = textfieldRelationship.text
        
        patient.dentalCoverageBtn = dentalCoverage.selected == nil ? 0 : dentalCoverage.selected.tag
        patient.medicalCoverageBtn = medicalCoverage.selected == nil ? 0 : medicalCoverage.selected.tag
        patient.OrthodonticCoverageBtn = orthodonticCoverage.selected == nil ? 0 : orthodonticCoverage.selected.tag
        patient.insuredPersonTag = insuredPersonBtn.selected == nil ? 0 : insuredPersonBtn.selected.tag
    }
    
    func loadValues() {
        textfieldInsuranceCompanyName.text = patient.primaryInsuranceCompanyName
        textfieldPhone.text = patient.primaryPhone
        textfieldGroup.text = patient.primaryGroup
        textfieldInsuranceCoAddress.text = patient.primaryInsuranceCoAddress
        textFieldInsuranceCoCity.text = patient.primaryInsuranceCoCity
        textFieldInsuraceCoState.text = patient.primaryInsuraceCoState == nil || patient.primaryInsuraceCoState.isEmpty ? "IL" : patient.primaryInsuraceCoState
        textFieldInsuranceCozip.text = patient.primaryInsuranceCozip
        
        textFieldInsuredname.text = patient.primaryInsuredPersonname
        
        textFieldSocialSecurity.text = patient.primarySocialSecurity
        textFieldInsuredDOB.text = patient.primaryInsuredDOB
        textfieldRelationship.text = patient.primaryRelationship
        
        dentalCoverage.setSelectedWithTag(patient.dentalCoverageBtn == nil ? 0 : patient.dentalCoverageBtn)
        medicalCoverage.setSelectedWithTag(patient.medicalCoverageBtn == nil ? 0 : patient.medicalCoverageBtn)
        orthodonticCoverage.setSelectedWithTag(patient.OrthodonticCoverageBtn == nil ? 0 : patient.OrthodonticCoverageBtn)
        insuredPersonBtn.setSelectedWithTag(patient.insuredPersonTag == nil ? 0 : patient.insuredPersonTag)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func findEmptyTextField() -> UITextField? {
        let textFields = [textfieldInsuranceCompanyName]
        for textField in textFields {
            if (textField?.isEmpty)! {
                return textField
            }
        }
        return nil
    }
    
    
    @IBAction func onNextButtonPressed (_ sender : UIButton){
        
        if let _ = findEmptyTextField() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        } else if !textfieldPhone.isEmpty && !textfieldPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldInsuranceCozip.isEmpty && !textFieldInsuranceCozip.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID INSURANCE COMPANY ZIP CODE")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldSocialSecurity.isEmpty && !textFieldSocialSecurity.text!.isSocialSecurityNumber {
            let alert = Extention.alert("PLEASE ENTER VALID INSURED'S SOCIAL SECURITY NUMBER")
            self.present(alert, animated: true, completion: nil)
        }  else {
            
            saveValues()
            let diseaseInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kResponsiblePersonVC") as! ResponsiblePersonVC
            diseaseInfoVC.patient = self.patient
            self.navigationController?.pushViewController(diseaseInfoVC, animated: true)
        }
    }
    
    @IBAction func radioInsuredPersonAction (_ sender : RadioButton){
        if sender.tag == 1{
            textFieldInsuredname.text = patient.fullName
            textFieldInsuredDOB.text = patient.dateOfBirth
            textfieldRelationship.text = "PATIENT"
            textFieldSocialSecurity.text = patient.socialSecurityNumber
        } else{
            textFieldInsuredname.text = ""
            textFieldInsuredDOB.text = ""
            textfieldRelationship.text = ""
            textFieldSocialSecurity.text = ""
        }
    }
}

extension PrimaryInsuranceVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldSocialSecurity{
            return textField.formatNumbers(range, string: string, count: 9)
        } else if  textField == textFieldInsuranceCozip {
            return textField.formatZipCode(range, string: string)
        } else if textField == textfieldPhone {
            return textField.formatPhoneNumber(range, string: string)
        }

        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

