//
//  ToothWhiteningFormVC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 13/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ToothWhiteningFormVC: PDViewController {

    
    @IBOutlet weak var labelDate1: UILabel!
    @IBOutlet weak var labelDate2: UILabel!
    @IBOutlet weak var labelDate3: UILabel!
    @IBOutlet weak var labelDate4: UILabel!
    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var labelDentistName: UILabel!
    @IBOutlet weak var patientSignature: UIImageView!
    @IBOutlet weak var dentistSignature: UIImageView!
    
    @IBOutlet weak var initialView1: UIImageView!
    @IBOutlet weak var initialView2: UIImageView!
    @IBOutlet weak var initialView3: UIImageView!
    @IBOutlet weak var initialView4: UIImageView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelDate3.text = patient.dateToday
        labelDate4.text = patient.dateToday
        
        if patient.Toothwhitningsignature3 != nil {
            initialView1.image = patient.Toothwhitningsignature3
            initialView2.image = patient.Toothwhitningsignature3
            initialView3.image = patient.Toothwhitningsignature3
            initialView4.image = patient.Toothwhitningsignature3
        }
        
        labelPatientName.text = patient.fullName
        labelDentistName.text = patient.doctorName
        patientSignature.image = patient.Toothwhitningsignature1
        dentistSignature.image = patient.Toothwhitningsignature2
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
