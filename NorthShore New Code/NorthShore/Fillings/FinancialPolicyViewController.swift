//
//  FinancialPolicyViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class FinancialPolicyViewController: PDViewController {
    
    
    
    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var signatureView2: SignatureView!
    @IBOutlet weak var signatureView3: SignatureView!

    @IBOutlet var labelDate: [DateLabel]!
    @IBOutlet weak var tableViewServices: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var constraintViewHeight: NSLayoutConstraint!
    var viewHeight : CGFloat!

    var selectedServices : [Service]! = [Service]()

    override func viewDidLoad() {
        super.viewDidLoad()
        for date in labelDate {
            date.todayDate = patient.dateToday
        }
        selectedServices = Service.getAllServices()
        tableViewServices.reloadData()
        scrollView.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.consentFillings = self.selectedServices
        patient.Fillingsignature1 = signatureView1.isSigned() ? signatureView1.signatureImage() : nil
        patient.Fillingsignature2 = signatureView2.isSigned() ? signatureView2.signatureImage() : nil
        patient.Fillingsignature3 = signatureView3.isSigned() ? signatureView3.signatureImage() : nil
    }
    func loadValues() {
        if patient.consentFillings != nil {
            self.selectedServices = patient.consentFillings
        }
        tableViewServices.reloadData()
        signatureView1.previousImage = patient.Fillingsignature1
        signatureView2.previousImage = patient.Fillingsignature2
        signatureView3.previousImage = patient.Fillingsignature3
    }
    

    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if let _ = findEmptySignature() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if let _ = findEmptyDate() {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            let financialPolicyFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kFinancialPolicyFormVC") as! FinancialPolicyFormViewController
            financialPolicyFormVC.patient = patient
            self.navigationController?.pushViewController(financialPolicyFormVC, animated: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if viewHeight == nil {
            let height = (screenSize.height - tableViewServices.frame.height) + tableViewServices.contentSize.height
            constraintViewHeight.constant = height
            scrollView.contentSize = CGSize(width: screenSize.width, height: height)
            scrollView.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
            viewHeight = height
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func findEmptyDate() -> DateLabel? {
        for date in labelDate {
            if !date.dateTapped  {
                return date
            }
        }
        return nil
    }
    
    func findEmptySignature() -> SignatureView? {
        for signatureView in [signatureView1, signatureView2, signatureView3] {
            if !(signatureView?.isSigned())!  {
                return signatureView
            }
        }
        return nil
    }
}


extension FinancialPolicyViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let serviceObj = selectedServices[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellInitials") as! InitialsTableViewCell
        let text = serviceObj.serviceDescription
        let height = (text?.heightWithConstrainedWidth(568.0 * screenSize.width/768.0, font: cell.labelDetails.font))! + 260
        return height
    }
}


extension FinancialPolicyViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellInitials", for:  indexPath) as! InitialsTableViewCell
        let serviceObj = selectedServices[indexPath.row]
        cell.configureCell(serviceObj)
        cell.signatureView.tag = indexPath.row
        cell.signatureView.previousImage = serviceObj.signatureView.imageObj
        cell.signatureView.delegate = self
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        return cell
    }
}

extension FinancialPolicyViewController : SignatureViewDelegate {
    func drawingCompleted(_ image: UIImage!, sign signatureView: SignatureView!) {
        let serviceObj = selectedServices[signatureView.tag]
        serviceObj.signatureView.imageObj = image
        selectedServices[signatureView.tag] = serviceObj
    }
}
