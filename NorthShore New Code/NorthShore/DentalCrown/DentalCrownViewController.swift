//
//  DentalCrownViewController.swift
//  North Shore
//
//  Created by Office on 2/23/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DentalCrownViewController: PDViewController {

    var textRanges : [NSRange]! = [NSRange]()

    
    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var signatureView2: SignatureView!
    @IBOutlet weak var labelDate1: PDLabel!
    @IBOutlet weak var labelDate2: PDLabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelName.text = patient.fullName

        let form = patient.selectedForms.filter { (formObj) -> Bool in
            return formObj.formTitle == kDentalCrown
        }
        
        let patientName = getText("\(patient.fullName)")
        labelDetails.text = labelDetails.text?.replacingOccurrences(of: "FIRSTNAME LASTNAME", with: patientName).replacingOccurrences(of: "kToothNumber", with:getText(form[0].toothNumbers))
        
        signatureView1.layer.cornerRadius = 3.0
        signatureView2.layer.cornerRadius = 3.0
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(DentalCrownViewController.setDateOnLabel1))
        tapGesture1.numberOfTapsRequired = 1
        labelDate1.addGestureRecognizer(tapGesture1)
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(DentalCrownViewController.setDateOnLabel2))
        tapGesture2.numberOfTapsRequired = 1
        labelDate2.addGestureRecognizer(tapGesture2)
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.DentalCrownsignature1 = signatureView1.isSigned() ? signatureView1.signatureImage() : nil
        patient.DentalCrownsignature2 = signatureView2.isSigned() ? signatureView2.signatureImage() : nil
    }
    func loadValues() {
        signatureView1.previousImage = patient.DentalCrownsignature1
        signatureView2.previousImage = patient.DentalCrownsignature2
    }
    
    func setDateOnLabel1() {
        labelDate1.text = patient.dateToday
        labelDate1.textColor = UIColor.black
    }
    
    func setDateOnLabel2() {
        labelDate2.text = patient.dateToday
        labelDate2.textColor = UIColor.black
    }

   
    
    @IBAction func buttonNextPressed(_ sender: AnyObject) {
        if !signatureView1.isSigned() || !signatureView2.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if labelDate1.text == "Tap to date" || labelDate2.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            let dentalCrownFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kDentureCrownFormVC") as! DentalCrownFormViewController
            dentalCrownFormVC.patient = patient
            self.navigationController?.pushViewController(dentalCrownFormVC, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
