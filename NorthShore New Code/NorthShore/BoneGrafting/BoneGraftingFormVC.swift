//
//  BoneGraftingFormVC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 14/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class BoneGraftingFormVC: PDViewController {
   
    @IBOutlet weak var signatureView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.text = patient.dateToday
        labelName.text = patient.fullName
        signatureView.image = patient.BoneGraftingSignature
    }
    

    
}

extension BoneGraftingFormVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.boneGraftingQuestions.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MedicalHistoryStep1TableViewCell
        let obj = patient.boneGraftingQuestions[indexPath.row]
        
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        cell.question = obj
        cell.labelTitle.text = obj.question
        cell.buttonYes.isSelected = obj.selectedOption
        
        return cell
    }
}
