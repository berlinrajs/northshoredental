//
//  ParentInfoViewController.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ParentInfoViewController: PDViewController {
  
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
    @IBOutlet weak var textFieldDateOfBirth: PDTextField!
    
    @IBOutlet weak var buttonYes: UIButton!
    @IBOutlet weak var buttonNo: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DateInputView.addDatePickerForDateOfBirthTextField(textFieldDateOfBirth)
        labelDate.text = patient.dateToday
        
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        if buttonYes.isSelected {
            patient.isParent = true
            patient.parentFirstName = textFieldFirstName.text
            patient.parentLastName = textFieldLastName.text
            patient.parentDateOfBirth = textFieldDateOfBirth.text
        } else {
            patient.isParent = false
            patient.parentFirstName = ""
            patient.parentLastName = ""
            patient.parentDateOfBirth = ""
        }
    }
    func loadValues() {
        buttonYes.isSelected = patient.isParent != nil && patient.isParent == true
        textFieldFirstName.text = patient.parentFirstName
        textFieldLastName.text = patient.parentLastName
        textFieldDateOfBirth.text = patient.parentDateOfBirth
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        func gotoAddressInfoVC() {
            let addressInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kAddressInfoVC") as! AddressInfoViewController
            addressInfoVC.patient = patient
            self.navigationController?.pushViewController(addressInfoVC, animated: true)
        }
        
        self.view.endEditing(true)
        if buttonYes.isSelected {
            if textFieldFirstName.isEmpty {
                let alert = Extention.alert("PLEASE ENTER FIRST NAME")
                self.present(alert, animated: true, completion: nil)
            } else if textFieldLastName.isEmpty {
                let alert = Extention.alert("PLEASE ENTER LAST NAME")
                self.present(alert, animated: true, completion: nil)
            } else if textFieldDateOfBirth.isEmpty {
                let alert = Extention.alert("PLEASE ENTER DATE OF BIRTH")
                self.present(alert, animated: true, completion: nil)
            } else {
                saveValues()
                gotoAddressInfoVC()
            }
        } else {
            gotoAddressInfoVC()
        }
    }
    

    
    
    @IBAction func buttonActionYesNo(_ sender: UIButton) {
        sender.isSelected = true
        if sender == buttonYes {
            buttonNo.isSelected = false
            textFieldFirstName.isEnabled = true
            textFieldLastName.isEnabled = true
            textFieldDateOfBirth.isEnabled = true
        } else {
            buttonYes.isSelected = false
            textFieldFirstName.isEnabled = false
            textFieldLastName.isEnabled = false
            textFieldDateOfBirth.isEnabled = false
            textFieldLastName.text = ""
            textFieldFirstName.text = ""
            textFieldDateOfBirth.text = ""
        }
    }
}
