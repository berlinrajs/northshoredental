//
//  HippaStep3VC.swift
//  North Shore
//
//  Created by SRS Web Solutions on 05/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HippaStep3VC: PDViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var signatureView: SignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelName.text = "I \(patient.fullName), have reviewed a copy of this office’s Notice of Privacy Practices and consent to photos for patient file and/or of treatment."

        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.Hippa3signature1 = signatureView.isSigned() ? signatureView.signatureImage() : nil
    }
    func loadValues() {
        signatureView.previousImage = patient.Hippa3signature1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender: AnyObject) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func actionNext(_ sender: AnyObject) {
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let MedicalHistory1 = self.storyboard?.instantiateViewController(withIdentifier: "kMedicalHistoryStep1VC") as! MedicalHistoryStep1ViewController
            MedicalHistory1.patient = patient
            self.navigationController?.pushViewController(MedicalHistory1, animated: true)
        }
    }
}
