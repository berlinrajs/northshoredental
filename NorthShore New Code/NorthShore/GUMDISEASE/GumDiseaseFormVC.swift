//
//  GumDiseaseFormVC.swift
//  TotalHealthDental
//
//  Created by SRS Web Solutions on 07/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class GumDiseaseFormVC: PDViewController {

    @IBOutlet weak var labelPeriodonticDate: UILabel!
    @IBOutlet weak var labelPeriodonticType: UILabel!
    @IBOutlet weak var labelTeethNumbers: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var radioAgreement: RadioButton!
    @IBOutlet weak var radioPockets: RadioButton!
    @IBOutlet weak var signature: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.pdfView = self.view
        
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        labelPeriodonticDate.text = patient.periodontalDate
        labelPeriodonticType.text = patient.periodontalType == 1 ? "Early Periodontitis." : patient.periodontalType == 2 ? "Moderate Periodontitis." : "Advanced Periodontitis."
        
        radioPockets.setSelectedWithTag(patient.pocketsSelected!)
        radioAgreement.setSelectedWithTag(patient.periodontalagreementSelected!)
        
        if patient.pocketsSelected == 3 {
            labelTeethNumbers.text = patient.teethNumbers
        }
        
        labelName.text = patient.fullName
        signature.image = patient.periodontalsignature1
        labelDate.text = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
