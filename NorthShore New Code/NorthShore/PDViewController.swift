
//  PDViewController.swift
//  SecureDental
//
//  Created by Leojin Bose on 2/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
import Darwin
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class PDViewController: UIViewController {
    
    var patient : PDPatient!
  
    @IBOutlet weak var pdfView: UIScrollView?
    
    @IBOutlet weak var buttonBack: UIButton?
    @IBOutlet weak var buttonSubmit: UIButton?
    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = UIRectEdge()
        self.buttonSubmit?.backgroundColor = UIColor.green
        self.buttonBack?.isHidden = self.buttonSubmit == nil && isFromPreviousForm
        
        // Do any additional setup after loading the view.
    }

    var isFromPreviousForm: Bool {
        get {
            return navigationController?.viewControllers.count > 3 ? true : false
        }
    }
    
    func showAlert(_ message: String) {
        let alert = Extention.alert(message)
        self.present(alert, animated: true, completion: nil)
    }
    func showAlert(_ message: String, completion: @escaping (_ completed: Bool) -> Void) {
        let alertController = UIAlertController(title: "North Shore", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.destructive) { (action) -> Void in
            completion(true)
        }
        alertController.addAction(alertOkAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
        
    @IBAction func buttonActionBack(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
//    func report_memory() {
//        var info = task_basic_info()
//        var count = mach_msg_type_number_t(MemoryLayout.size(ofValue: info))/4
//        
//        let kerr: kern_return_t = withUnsafeMutablePointer(to: &info) {
//            
//            task_info(mach_task_self_,
//                      task_flavor_t(TASK_BASIC_INFO),
//                      task_info_t($0),
//                      &count)
//            
//        }
//        
//        if kerr == KERN_SUCCESS {
//            print("Memory in use (in bytes): \(info.resident_size)")
//        }
//        else {
//            print("Error with task_info(): " +
//                (String(cString: mach_error_string(kerr)) ?? "unknown error"))
//        }
//    }
    
    @IBAction func buttonActionSubmit(_ sender: AnyObject) {
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "North Shore", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.shared.openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        let pdfManager = PDFManager()
        pdfManager.authorizeDrive(self) { (success) -> Void in
            if success {
                self.buttonSubmit?.isHidden = true
                self.buttonBack?.isHidden = true
                if let _ = self.pdfView {
                    pdfManager.createImageForScrollView(self.pdfView!, patient: self.patient, completionBlock: { (finished) -> Void in
                        if finished {
                            self.patient.selectedForms.removeFirst()
                            self.gotoNextForm()
                        } else {
                            self.buttonSubmit?.isHidden = false
                            self.buttonBack?.isHidden = false
                        }
                    })
                } else {
                    pdfManager.createImageForView(self.view, patient: self.patient, completionBlock: { (finished) -> Void in
                        if finished {
                            self.patient.selectedForms.removeFirst()
                            self.gotoNextForm()
                        } else {
                            self.buttonSubmit?.isHidden = false
                            self.buttonBack?.isHidden = false
                        }
                    })
                }
            } else {
                self.buttonSubmit?.isHidden = false
                self.buttonBack?.isHidden = false
            }
        }
    }
    
    func gotoNextForm() {
        if isFromPreviousForm {
            if self.navigationController?.viewControllers.count > 2 {
                self.navigationController?.viewControllers.removeSubrange(2...(self.navigationController?.viewControllers.count)! - 2)
            }
        }
        
        let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
        
        if formNames.contains(kNewPatientSignInForm) {
            let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
            let immediateVC = storyboard.instantiateViewController(withIdentifier: "kAddressInfoVC") as! AddressInfoViewController
            immediateVC.patient = self.patient
            self.navigationController?.pushViewController(immediateVC, animated: true)
        } else if formNames.contains(kPatientAuthorization) {
            
            let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
            let patientAuthorizationVC = storyboard.instantiateViewController(withIdentifier: "kPatientAuthorizationVC") as! PatientAuthorizationViewController
            patientAuthorizationVC.patient = self.patient
            self.navigationController?.pushViewController(patientAuthorizationVC, animated: true)
        } else if formNames.contains(kDentalHealthHistory) {
            let storyboard = UIStoryboard(name: "DentalHealthHistory", bundle: nil)
            let cardCapture = storyboard.instantiateViewController(withIdentifier: "DentalHealthHistoryStep1VC") as! DentalHealthHistoryStep1VC
            cardCapture.patient = self.patient
            self.navigationController?.pushViewController(cardCapture, animated: true)
        } else if formNames.contains(kInsuranceCard) {
            let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
            let cardCapture = storyboard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            self.navigationController?.pushViewController(cardCapture, animated: true)
        } else if formNames.contains(kDrivingLicense) {
            let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
            let cardCapture = storyboard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            cardCapture.isDrivingLicense = true
            self.navigationController?.pushViewController(cardCapture, animated: true)
        }else if formNames.contains(kSelfieForm) {
            let drivingLicense = mainStoryboard.instantiateViewController(withIdentifier: "kSelfieStep1VC") as! SelfieStep1VC
            drivingLicense.patient = self.patient
            self.navigationController?.pushViewController(drivingLicense, animated: true)
        } else if formNames.contains(kGingivalGraftingSurgery) {
            let GingivalVC = mainStoryboard.instantiateViewController(withIdentifier: "kGingivalGraftingVC") as! GingivalGraftingVC1
            GingivalVC.patient = self.patient
            self.navigationController?.pushViewController(GingivalVC, animated: true)
        } else if formNames.contains(kConsentImplants) {
            let dentalVC = mainStoryboard.instantiateViewController(withIdentifier: "kDentalImplantsVC") as! DentalImplantsVC
            dentalVC.patient = self.patient
            self.navigationController?.pushViewController(dentalVC, animated: true)
        } else if formNames.contains(kBoneGrafting) {
            let boneGrafting = mainStoryboard.instantiateViewController(withIdentifier: "kBoneGraftingStep1VC") as! BoneGraftingStep1VC
            boneGrafting.patient = self.patient
            self.navigationController?.pushViewController(boneGrafting, animated: true)
        } else if formNames.contains(kEndodonticTreatment) {
            let endoTreatment = mainStoryboard.instantiateViewController(withIdentifier: "kEndodonticTreatmentStep1VC") as! EndodonticTreatmentStep1VC
            endoTreatment.patient = self.patient
            self.navigationController?.pushViewController(endoTreatment, animated: true)
        } else if formNames.contains(kCrownAndBridges) {
            let crownAndBridgesStep1VC = mainStoryboard.instantiateViewController(withIdentifier: "CrownsAndBridgesViewControllerScene") as! CrownsAndBridgesViewControllerScene
            crownAndBridgesStep1VC.patient = self.patient
            self.navigationController?.pushViewController(crownAndBridgesStep1VC, animated: true)
        } else if formNames.contains(kCompleteAndPartialDentures) {
            let partialDenturesVC = mainStoryboard.instantiateViewController(withIdentifier: "CompleteAndPartialDenturesViewControllerScene") as! CompleteAndPartialDenturesViewControllerScene
            partialDenturesVC.patient = patient
            self.navigationController?.pushViewController(partialDenturesVC, animated: true)
        } else if formNames.contains(kTreatmentFillings) {
            let consentFillingsVC = mainStoryboard.instantiateViewController(withIdentifier: "kFinancialPolicyVC") as! FinancialPolicyViewController
            consentFillingsVC.patient = patient
            self.navigationController?.pushViewController(consentFillingsVC, animated: true)
        } else if formNames.contains(kMedicalClearance) {
            let medicalClearanceStep1VC = mainStoryboard.instantiateViewController(withIdentifier: "kMedicalClearanceStep1VC") as! MedicalClearanceStep1ViewController
            medicalClearanceStep1VC.patient = self.patient
            self.navigationController?.pushViewController(medicalClearanceStep1VC, animated: true)
        } else if formNames.contains(kGumDisease) {
            let step1VC = mainStoryboard.instantiateViewController(withIdentifier: "kGumDiseaseStep1VC") as! GumDiseaseStep1VC
            step1VC.patient = patient
            self.navigationController?.pushViewController(step1VC, animated: true)
        } else if formNames.contains(kRefusalDentalTreatment) {
            let refusalOfDentalTreatmentVC = mainStoryboard.instantiateViewController(withIdentifier: "kRefusalOfDentalVC") as! RefusalOfDentalTreatmentViewController
            refusalOfDentalTreatmentVC.patient = self.patient
            self.navigationController?.pushViewController(refusalOfDentalTreatmentVC, animated: true)
        } else if formNames.contains(kRefusalofTreatment) {
            let refusalofTreatmentVC = mainStoryboard.instantiateViewController(withIdentifier: "RefusalOfTreatmentViewControllerScene") as! RefusalOfTreatmentViewControllerScene
            refusalofTreatmentVC.patient = patient
            self.navigationController?.pushViewController(refusalofTreatmentVC, animated: true)
        } else if formNames.contains(kReleaseRecord) {
            let releaseRecordVC = mainStoryboard.instantiateViewController(withIdentifier: "kReleaseRecordVC") as! ReleaseRecordViewController
            releaseRecordVC.patient = patient
            self.navigationController?.pushViewController(releaseRecordVC, animated: true)
        } else if formNames.contains(kToothWhitening) {
            let toothwhiteVC = mainStoryboard.instantiateViewController(withIdentifier: "kToothWhiteningStep1VC") as! ToothWhiteningStep1VC
            toothwhiteVC.patient = self.patient
            self.navigationController?.pushViewController(toothwhiteVC, animated: true)
        } else if formNames.contains(klazerWhitening) {
            let lazerwhiteVC = mainStoryboard.instantiateViewController(withIdentifier: "kLazerWhiteningStep1VC") as! LazerWhiteningStep1VC
            lazerwhiteVC.patient = self.patient
            self.navigationController?.pushViewController(lazerwhiteVC, animated: true)
        } else if formNames.contains(kExtractionConsent) {
            let extractionVC = mainStoryboard.instantiateViewController(withIdentifier: "kExractionConsentStep1VC") as! ExtractionConsentStep1VC
            extractionVC.patient = self.patient
            self.navigationController?.pushViewController(extractionVC, animated: true)
        } else if formNames.contains(kSedationConsent) {
            let extractionVC = mainStoryboard.instantiateViewController(withIdentifier: "kSedationConsentStep1VC") as! SedationConsentStep1VC
            extractionVC.patient = self.patient
            self.navigationController?.pushViewController(extractionVC, animated: true)
        } else if formNames.contains(kDentalCrown) {
            let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
            let dentalCrownVC = storyboard.instantiateViewController(withIdentifier: "kDentureCrownVC") as! DentalCrownViewController
            dentalCrownVC.patient = patient
            self.navigationController?.pushViewController(dentalCrownVC, animated: true)
        } else if formNames.contains(kInofficeWhitening) {
            let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
            let inOfficeWhiteningVC = storyboard.instantiateViewController(withIdentifier: "kInOfficeWhiteningVC") as! InOfficeWhiteningViewController
            inOfficeWhiteningVC.patient = patient
            self.navigationController?.pushViewController(inOfficeWhiteningVC, animated: true)
        } else if formNames.contains(kTreatmentOfChild) {
            let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
            let childTreatmentVC = storyboard.instantiateViewController(withIdentifier: "kChildTreatmentVC") as! ChildTreatmentViewController
            childTreatmentVC.patient = patient
            self.navigationController?.pushViewController(childTreatmentVC, animated: true)
        } else if formNames.contains(kToothExtraction) {
            let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
            let toothExtractionVC = storyboard.instantiateViewController(withIdentifier: "kToothExtractionVC") as! ToothExtractionViewController
            toothExtractionVC.patient = patient
            self.navigationController?.pushViewController(toothExtractionVC, animated: true)
        }
        else if formNames.contains(kPhotographyOrTestimonial) {
            let storyboard = UIStoryboard(name: "DentalHealthHistory", bundle: nil)
            let PTVC = storyboard.instantiateViewController(withIdentifier: "PhotographyVC") as! PhotographyOrTestimonialViewControler
            PTVC.patient = patient
            self.navigationController?.pushViewController(PTVC, animated: true)
        }
        else if formNames.contains(kOpiodForm) {
            let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
            let opiodVC = storyboard.instantiateViewController(withIdentifier: "kOpiodViewController") as! OpiodViewController
            opiodVC.patient = self.patient
            self.navigationController?.pushViewController(opiodVC, animated: true)
        } else if formNames.contains(kFeedBack) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let feedBackVC = storyboard.instantiateViewController(withIdentifier: "kFeedBackInfoViewController") as! FeedBackInfoViewController
            feedBackVC.patient = patient
            self.navigationController?.pushViewController(feedBackVC, animated: true)
        } else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: kFormsCompletedNotification), object: nil)
            self.navigationController?.popToRootViewController(animated: true)
        }
////        let kConsentForms = ["CONSENT FORMS" : [kTeethExtraction, kToothWhitening, kPartialDenture, kInofficeWhitening, kTreatmentOfChild, kEndodonticTreatment, kDentalCrown, kOpiodForm, kCrownPickup, kCrownAndBridgeDelivery, kDentureConsent, kFullDentureConsent, kConsentForTreatment, kCrownAndBridge, kPeriodontalTreatment, kEndodonticTreatmentConsent, kExtractionConsent]]
//
    }
}


