//
//  DentalHealthHistoryStep3VC.swift
//  NorthShore
//
//  Created by SRS Websolutions.com on 7/4/18.
//  Copyright © 2018 SRS. All rights reserved.
//

import UIKit

class DentalHealthHistoryStep3VC: PDViewController {

    @IBOutlet weak var textFieldPhysiciansName: PDTextField!
    @IBOutlet weak var textFieldDateLastVisit: PDTextField!
    
    @IBOutlet weak var radioIllness: RadioButton!
    @IBOutlet weak var radioPregnant: RadioButton!
    @IBOutlet weak var radioNursing: RadioButton!
    @IBOutlet weak var radioBirthControl: RadioButton!
    
    @IBOutlet var buttonCheck: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DateInputView.addDatePickerForTextField(textFieldDateLastVisit)
        
        loadValues()
    }
    
    func loadValues() {
        textFieldPhysiciansName.text = patient.dhhPhysicianName
        textFieldDateLastVisit.text = patient.dhhPhysicianDateLastVisit
        
        if patient.dhhAllergies == nil {
            patient.dhhAllergies = [Int]()
        }
        for button in buttonCheck {
            button.isSelected = patient.dhhAllergies.contains(button.tag)
        }
        radioIllness.setSelectedWithTag(patient.dhhIllnessTag)
        radioPregnant.setSelectedWithTag(patient.dhhWomenTag1)
        radioNursing.setSelectedWithTag(patient.dhhWomenTag2)
        radioBirthControl.setSelectedWithTag(patient.dhhWomenTag3)
    }
    
    func saveValues() {
        patient.dhhPhysicianName = textFieldPhysiciansName.text
        patient.dhhPhysicianDateLastVisit = textFieldDateLastVisit.text
        
        patient.dhhWomenTag1 = radioPregnant.selected == nil ? 0 : radioPregnant.selected.tag
        patient.dhhWomenTag2 = radioNursing.selected == nil ? 0 : radioNursing.selected.tag
        patient.dhhWomenTag3 = radioBirthControl.selected == nil ? 0 : radioBirthControl.selected.tag
        
        patient.dhhIllnessTag = radioIllness.selected == nil ? 0 : radioIllness.selected.tag
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    
    @IBAction func buttonCheckBoxAction (_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            if !self.patient.dhhAllergies.contains(sender.tag) {
                self.patient.dhhAllergies.append(sender.tag)
            }
        } else {
            if self.patient.dhhAllergies.contains(sender.tag) {
                self.patient.dhhAllergies.remove(at: self.patient.dhhAllergies.index(of: sender.tag)!)
            }
        }
        
        if sender.tag == 8 {
            PopupTextField.sharedInstance.showWithTitle( "OTHER" , placeHolder: "PLEASE SPECIFY" ,keyboardType: UIKeyboardType.default, textFormat: .default, completion: { (textField, isEdited) in
                if !textField.isEmpty {
                    self.patient.dhhOtherAllergies = textField.text!
                } else {
                    self.patient.dhhOtherAllergies = ""
                }
            })
        }
        
    }
    
    
    @IBAction func radioButtonAction(_ sender: RadioButton) {
        if sender.tag == 1 {
            PopupTextField.sharedInstance.showWithTitle( "IF YES" , placeHolder: "PLEASE DESCRIBE" ,keyboardType: UIKeyboardType.default, textFormat: .default, completion: { (textField, isEdited) in
                if !textField.isEmpty {
                    self.patient.dhhIllnessYES = textField.text!
                } else {
                    self.patient.dhhIllnessYES = ""
                }
            })
        } else {
            self.patient.dhhIllnessYES = ""
        }
    }
    
    @IBAction func buttonNextAction(_ sender: Any) {
        self.view.endEditing(true)
        
        saveValues()
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "DentalHealthHistoryStep4VC") as! DentalHealthHistoryStep4VC
        nextVC.patient = patient
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
}
