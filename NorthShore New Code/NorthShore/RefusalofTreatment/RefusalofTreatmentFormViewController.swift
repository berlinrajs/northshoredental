//
//  RefusalofTreatmentFormViewController.swift
//  AL-ShifaDentistry
//
//  Created by SRS Web Solutions on 21/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class RefusalofTreatmentFormViewController: PDViewController {
    
    @IBOutlet var imageViewSignature: SignatureView!
    
    @IBOutlet var labelDob: UILabel!
    
    @IBOutlet var labelDate: UILabel!
    
    @IBOutlet var labelName: UILabel!
    
    @IBOutlet var buttonRefusal: [UIButton]!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        imageViewSignature.image = patient.Refusaltreatsignature1
        labelDob.text = patient.dateOfBirth
        labelDate.text = patient.dateToday
        labelName.text = ("\(patient.fullName)")
        
        for button in buttonRefusal {
            
            let obj = patient.refusalOptions[button.tag]
            button.isSelected = obj.isSelected!
            
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
