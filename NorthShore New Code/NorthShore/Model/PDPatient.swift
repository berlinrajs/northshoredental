//
//  PDPatient.swift
//  SecureDental
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class PDPatient: NSObject {
    var selectedForms : [Forms]!
    
    var PatientTile : Int?
    var firstName : String!
    var lastName : String!
    var middleName: String!
    var preferredName : String!
    var dateOfBirth : String!
    var patientGender : Int?
    
    var isParent: Bool!
    var parentFirstName : String!
    var parentLastName :String!
    var parentDateOfBirth :String!
    
    var patientAge: Int! {
        get {
            if dateOfBirth == nil || dateOfBirth.characters.count == 0 {
                return 0
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            
            let numberOfDays = (Date().timeIntervalSince(dateFormatter.date(from: self.dateOfBirth)!))/(3600 * 24)
            return Int(numberOfDays/365.2425)
        }
    }

    
    //address vc
    var addressLine : String!
    var city : String!
    var state : String!
    var zipCode : String!
    var socialSecurityNumber : String!
    var email : String!
    var maritialStatus : Int! = 7
    var radioHearAboutUsTag :Int?
    var referredByDetails :String!
    var othersDetails :String!
    var Timetomeet :String!
    var OtherFamilymember :String!
    var phoneExtension : String!
    var HomephoneNumber : String!
    var workphoneNumber : String!
    var AddrcellphoneNumber : String!
    var drivingLicense : String!
    
    //contact vc
    var ContactemployerName : String!
    var HowLongThere : String!
    var ContactState : String!
    var ContactZip : String!
    var ContactCity : String!
    var ContactAddress : String!
    var ContactOccupation : String!
    
    //photographyOrTestimonial
    var PTPatientSign:  UIImage?
    
    //Dental Health History
    var dhhFloss: String!
    var dhhTeeth: String!
    
    var dhhProblemCheckBoxTag: [Int]!
    
    var dhhPhysicianName: String!
    var dhhPhysicianDateLastVisit: String!
    
    var dhhWomenTag1: Int! = 0
    var dhhWomenTag2: Int! = 0
    var dhhWomenTag3: Int! = 0
    
    
    var dhhAllergies: [Int]!
    
    var dhhPharmacyName: String!
    var dhhPharmacyPhone: String!
    var dhhMedicationList: String!
    
    var dhhIllnessTag: Int! = 0
    var dhhMedicationsTag: [Int]!
    var dhhBloodThinnersTag: [Int]!
    var dhhOtherTag: [Int]!
    
    var dhhSignature: UIImage?
    var dhhOtherAllergies: String!
    var dhhIllnessYES: String!
    
    //emergency contact vc
//    var emergemployerName : String!
    var emergContactName : String!
    var emergState : String!
    var emergcity : String!
    var emergZip : String!
    var emergAddress : String!
    var emergHomephoneNumber : String!
    var emergworkphoneNumber : String!
     var emergRelationName : String!
     var emergphoneExtension : String!
//    var emergSocialSecurity : String!
    
    // responsible person inform vc
    
    var responsiblePersonIsPatient: Bool!
    var RespemployerName : String!
    var RespContactName : String!
    var RespState : String!
    var Respcity : String!
    var RespZip : String!
    var RespAddress : String!
    var RespHomephoneNumber : String!
    var RespworkphoneNumber : String!
    var RespRelationName : String!
    var RespphoneExtension : String!
    var RespSocialSecurity : String!
    var RespDrivierlicense : String!
    
    // Spouse VC
    var SpouseemployerName : String?
    var SpouseContactName : String?
    var SpouseworkphoneNumber : String!
    var SpouseDateofbirth : String?
    var SpousephoneExtension : String!
    var SpouseSocialSecurity : String!
     var  SpouseDriverLicense: String!
    
    
   
    
    // Primary insurance VC
    var primaryInsuranceCompanyName : String!
    var primaryPhone : String!
    var primaryGroup : String!
    var primaryInsuranceCoAddress : String!
    var primaryInsuranceCoCity : String!
    var primaryInsuraceCoState : String!
    var primaryInsuranceCozip : String!
    var  primaryInsuredPersonname: String!
    var primarySocialSecurity : String!
    var primaryInsuredDOB : String!
    var primaryRelationship : String!
    var primaryEmployer : String!
    var primaryEmployerAddress : String!
    var primaryEmpCity : String!
    var  primaryEmpzip: String!
    var  primaryEmployerState: String!
    
    var dentalCoverageBtn: Int!
    var medicalCoverageBtn: Int!
    var OrthodonticCoverageBtn: Int!
    var insuredPersonTag: Int!
    func resetPrimaryInsurance () {
        primaryInsuranceCompanyName = ""
        primaryPhone = ""
        primaryGroup = ""
        primaryInsuranceCoAddress = ""
        primaryInsuranceCoCity = ""
        primaryInsuraceCoState = ""
        primaryInsuranceCozip = ""
        
        primaryInsuredPersonname = ""
        
        primarySocialSecurity = ""
        primaryInsuredDOB = ""
        primaryRelationship = ""
        primaryEmployer = ""
        
        primaryEmployerAddress = ""
        primaryEmpCity = ""
        primaryEmpzip = ""
        primaryEmployerState = ""
        
        dentalCoverageBtn = 0
        medicalCoverageBtn = 0
        OrthodonticCoverageBtn = 0
    }
    
    func resetSecondaryInsurance() {
        secondaryInsuranceCompanyName = ""
        secondaryPhone = ""
        secondaryGroup = ""
        secondaryInsuranceCoAddress = ""
        secondaryInsuranceCoCity = ""
        secondaryInsuraceCoState = ""
        secondaryInsuranceCozip = ""
        secondaryInsuredPersonname = ""
        secondarySocialSecurity = ""
        secondaryInsuredDOB = ""
        secondaryRelationship = ""
        secondaryEmployer = ""
        secondaryEmployerAddress = ""
        secondaryEmpCity = ""
        secondaryEmpzip = ""
        secondaryEmployerState = ""
        secondarydentalCoverageBtn = 0
        secondarymedicalCoverageBtn = 0
        secondaryOrthodonticCoverageBtn = 0
    }
    
    
    
    // Primary insurance VC
    var secondaryInsuranceCompanyName : String?
    var secondaryPhone : String?
    var secondaryGroup : String!
    var secondaryInsuranceCoAddress : String?
    var secondaryInsuranceCoCity : String!
    var secondaryInsuraceCoState : String!
    var secondaryInsuranceCozip : String!
    var secondaryInsuredPersonname: String!
    var secondarySocialSecurity : String?
    var secondaryInsuredDOB : String?
    var secondaryRelationship : String!
    var secondaryEmployer : String?
    var secondaryEmployerAddress : String!
    var secondaryEmpCity : String!
    var secondaryEmpzip: String!
    var secondaryEmployerState: String!
    
    var secondarydentalCoverageBtn: Int!
    var secondarymedicalCoverageBtn: Int!
    var secondaryOrthodonticCoverageBtn: Int!
    
    // Agreement VC
     var Agreementsignature1: UIImage?
    var Agreementsignature2: UIImage?
    
    // HIPPA VC1
     var Hippa1signature1: UIImage?
    // HIPPA VC2
    var Hippa2signature1 : UIImage?
    //HIPPA VC3
     var Hippa3signature1 : UIImage?
    
    //MEDICAL HISTORY1
    var medicalHistoryQuestions1 : [PDQuestion]!
    var YCometoDentistry : String!
    var wisdomTeethReason : String!
    
    
    // MEDICALHISTORY 2
    var CurrentDentalTag : Int!
    var TypesOfBristleTag : Int!
    var YouLikeyourSmileTag : Int!
    var isUnderPhysician: Int!
    
    var HowlongUseToothBrush : String!
    var ToothSensitive : String!
    var PreviousDentist : String!
    var LastDentistVisit : String!
    var YLeavePreviousDentist : String!
    var WhatLikeAnyDentist : String!
    
    // MEDICAL HISTORY 2 POPUP
    
    var SmileReasonPopup : String!
    
    // MEDICAL HISTORY 4
      var medicalHistoryQuestions4 : [PDOption]!
    
    
    //MEDICAL HISTORY3
    var medicalHistoryQuestions3 : [PDQuestion]!
    var yourMedicalHealthTag : Int!
    var PersonalPhysicianTag : Int!
    var MedicalPhysicianName :String!
     var   MedicalPhysicianAddress :String!
   var   MedicalPhysicanVisit :String!
    var   MedicalPhysicianPhone :String!
     var   ifUnderPhysicanCare :String!
    
    
    //MEDICAL HISTORY5
    var medicalHistoryQuestions5 : [PDQuestion]!
     var OtheralergicIssuesValue :String!
    var AreyouwomanTag : Int!
    var AreyoupregnantTag : Int!
    var AreyoutakebirthcontrolTag : Int!
    var AreyounursingTag : Int!
    var womanWeeks : String!
    
    
    //MEDICAL HISTORY 6
    var medicalHistoryQuestions6 : [PDOption]!
    var takingAnyPrescriptionPopup : String!
    
    //MEDICAL HISTORY 7
    
    // Agreement VC
    var medicalHis7signature1: UIImage?
    var medicalHis7signature2: UIImage?
    var MethodOfPayment : String!

    
    
    
    var dateToday : String!
    var doctorName: String = "Dr.Luma Naim"
    var signature5: UIImage?
    var initials : UIImage!
    var relation : String!
    var paymentTag : Int!
    var privacyAcknowledgementTag : Int?
    var privacyAcknowledgementReason : String?
    var okPressed : Bool!
    var cellPhoneNumber: String?
  
    var preferredConfirmation : Int?
    var SpouseName : String!
    var spouseDob : String!
    var istext : Bool!
    var isemail : Bool!
    var isPhone : Bool!
    var preferredPhone : String!
    var insuranceIdNo : String!
    var GroupIdNo : String!
    var policyholderName : String!
    var policyHolderDob : String!
    var emergencyName : String!
    var emergencyRelation : String!
    var emergencyHomeNo : String!
    var emergencyCellNo : String!
    var insuranceCompanyName : String!
    
    
    //PATIENT AUTHORIZATION
    var previousClinicName : String!
    var newClinicName : String!
    var patientNumber : String!
    var faxNumber : String?
    var reasonForTransfer : String?
    var phoneNumber : String!

    //RELEASE CONFIDENTIAL VC
    
    var signature1 : UIImage!
    var signatureInOffice: UIImage!
    var signatureChildTreatment: UIImage!
    var signatureToothExtraction: UIImage!
    var signatureOpiod: UIImage!
    
    // TOOTH EXTRACTION VC
    var signature2 : UIImage!
    
    var signature3 : UIImage!
    
    //TOOTH EXTRACTION
    var othersText1 : String?
    var othersText2 : String?
    var toothExtractionQuestions1 : String?
    var toothExtractionQuestions2 : String?
    var prognosisProcedure : String?

    // DENTAL CROWN
    var DentalCrownsignature1 : UIImage!
     var DentalCrownsignature2 : UIImage!
    
    //CHILD TREATMENT
    var selectedOptions1 : [String]!
    var otherTreatment1 : String?
    var treatmentDate : String?
      var relationship: String?
    
    var fullName: String {
        get {
            if middleName.count > 0{
                return "\(firstName!) \(middleName!) \(lastName!)"
            }else{
                 return "\(firstName!) \(lastName!)"
            }
        }
    }
    
    var VisitorFirstName : String!
    var VisitorLastName : String!
    var VisitorFullName : String {
        return VisitorFirstName + " " + VisitorLastName
    }
    var VisitingPurpose : String!
    var visitorSignature: UIImage!
    
    // GINGIVAL GRAFTING VC1 
    
    var Gingivalsignature1 : UIImage!
    var Gingivalsignature2 : UIImage!
    var Gingivalrelationship: String?
    var GingivalrelationShipName: String?
    
    // DENTAL IMPLANTS VC
    
    var DentalImplantsignature1 : UIImage!
    var DentalImplantsignature2 : UIImage!
    var DentalImplantrelationship: String?
    var DentalImplantrelationShipName: String?
    
    // BONE GRAFTING VC1
    
    var boneGraftingQuestions: [PDQuestion]!
    var BoneGraftingSignature : UIImage!
    
     // ENDODONTIC TREATMENT VC
    var endodonticPatientSignature :UIImage!
    var endodonticdentistSignature :UIImage!
    var endodonticwitnessSignature : UIImage!
    
    
    // CROWN BRIDGE VC
    
    var CrownBridgesignature1 : UIImage!
    var CrownBridgesignature2 : UIImage!
    
    // IMMEDIATE AND COMPLETE DENTURES

    var ImmediateSignature1 : UIImage!
    var ImmediateSignature2 : UIImage!
    
    // FINANCIAL POLICY FORM VIEWCONTROLLER
      var consentFillings: [Service]!
    var Fillingsignature1 : UIImage!
    var Fillingsignature2 : UIImage!
    var Fillingsignature3 : UIImage!
    
    //MEDICALCLEARANCE VC1
        var selectedOptions : [String]!
    
    //MEDICALCLEARANCE VC2
       var medicalSignature1 :UIImage!
     var physicianName : String!
    var clearanceQuestions : [PDQuestion]!
    
    
    // GUMDISEASESTEP1VC
    var periodontalDate: String?
    var periodontalType: Int?
    var pocketsSelected: Int?
    var teethNumbers: String?
    var periodontalagreementSelected: Int?
    var periodontalsignature1 : UIImage!
    
    // REFUSAL DENTAL VC
    
    var Refusalsignature1 : UIImage!
    var Refusalsignature2 : UIImage!
    var otherTreatment :String?
    
    //REFUSAL TREATMENT VC
    
    var Refusaltreatsignature1 : UIImage!
   var refusalOptions : [PDOption]!
    
    // RELEASE RECORD
    
     var releaseType: Int!
    var authorizedPerson : String!
     var releaseRecordsignature1 : UIImage!
    
    // TOOTH WHITENING STEP1 VC
    
    var Toothwhitningsignature1 : UIImage!
    var Toothwhitningsignature2 : UIImage!
    var Toothwhitningsignature3 : UIImage!
    
    // LAZER WHITENING VC
    
     var Lazersignature1 : UIImage!
    
    //EXTRACTION CONSENT VC
    var AdditionalComments :String?
    var Extractionsignature1 : UIImage!
    var Extractionsignature2 : UIImage!
    var Extractionsignature3 : UIImage!
    
    //SEDETION CONSENT VC
    var sedationsignature1 : UIImage!
    var sedationsignature2 : UIImage!
    
    
    // test 
       var patientInformation: PatientInformation!

       
    required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
    
    var feedBack: FeedBack!
    
    var medicalHistory : MedicalHistory = MedicalHistory()
}

class MedicalHistory : NSObject{
    
    var medicalQuestions : [[PDQuestion]]!
    
    
    required override init() {
        super.init()
        
        let quest1 : [String] = ["AIDS",
                                 "Anemia",
                                 "Arthritis, Rheumatism",
                                 "Artificial Heart Valves",
                                 "Artificial Joints",
                                 "Asthma",
                                 "Back Problems",
                                 "Bleeding Abnormally",
                                 "Blood Disease",
                                 "Cancer",
                                 "Chemical Dependency",
                                 "Chemotherapy",
                                 "Circulatory Problems",
                                 "Congenital Heart Lesions",
                                 "Cortisone Treatments",
                                 "Cough, Persistent",
                                 "Cough up blood"]
        
        let quest2 : [String] = ["Diabetes",
                                 "Epilepsy",
                                 "Fainting",
                                 "Glaucoma",
                                 "Headaches",
                                 "Heart Murmur",
                                 "Heart Problem",
                                 "Hemophilia",
                                 "Hepatitis",
                                 "Hernia Repairs",
                                 "High Blood Pressure",
                                 "HIV Positive",
                                 "Jaw Pain",
                                 "Kidney Disease",
                                 "Liver Disease",
                                 "Mitral Valve Prolapse",
                                 "Nervous Problem"]
        
        let quest3 : [String] = ["Pacemaker",
                                 "Psychiatric Care",
                                 "Radiation Treatment",
                                 "Respiratory Disease",
                                 "Rheumatic Fever",
                                 "Scarlet Fever",
                                 "Shortness of Breath",
                                 "Skin Rash",
                                 "Stroke",
                                 "Swelling of Feet/Ankles",
                                 "Thyroid Problems",
                                 "Tobacco Habit",
                                 "Tonsillitis",
                                 "Tuberculosis",
                                 "Ulcer",
                                 "Venereal Disease",
                                 "Other"]
        
      
        
        
        
        self.medicalQuestions = [PDQuestion.getArrayOfQuestions(questions: quest1),
                                 PDQuestion.getArrayOfQuestions(questions: quest2),
                                 PDQuestion.getArrayOfQuestions(questions: quest3)
//                                 PDQuestion.getArrayOfQuestions(questions: quest4),
                                 //                                 MCQuestion.getArrayOfQuestions(questions: quest5),
            //                                 MCQuestion.getArrayOfQuestions(questions: quest6)
        ]
        
        self.medicalQuestions[2][16].isAnswerRequired = true
        
    }
    
    
}

class FeedBack: NSObject {
    var firstName: String!
    var lastName: String!
    var fullName: String! {
        get {
            if firstName == nil || lastName == nil {
                return ""
            }
            return ([firstName, lastName] as NSArray).componentsJoined(by: " ")
        }
    }
    var phoneNumber: String!
    
    var comment: String!
    var rating: CGFloat!
    var isAllowMessage: Bool!
    var isAnonymous: Bool!
}

class PatientInformation: NSObject {
    
    var patientOrGuardianSignature: UIImage!
    
    var physicianName: String?
    var officePhone: String?
    var dateOfLastExam: String?
    
    var contactLense: Int?
    var persistentCough: Int?
    var isPregnent: Int?
    var isNursing: Int?
    var oralContraceptives: Int?
    
    var questionGroup1: [PDQuestion]!
    var questionGroup2: [[PDQuestion]]!
    var questionGroup3: [PDQuestion]!
    
    override init() {
        super.init()
        let quest1: [String] = ["Are you under medical treatment now?",
                                "Have you ever been hospitalized for any surgical operation or serious illness within the last 5 years?",
                                "Are you taking any medication(s) including non-prescription medicine?",
                                "Have you ever taken Fen-Phen/Redux?",
                                "Have you ever taken Fosamax, Boniva, Actonel or any cancer medications containing biphosphonates?",
                                "Have you taken Viagra, Revatio, Cialis or Levitra in the last 24 hours?",
                                "Do you use tobacco?",
                                "Do you use controlled substances?"]
        
        let quest21: [String] = ["High Blood Pressure",
                                 "Heart Attack",
                                 "Rheumatic Fever",
                                 "Swollen Ankles",
                                 "Fainting / Seizures",
                                 "Asthma",
                                 "Low Blood Pressure",
                                 "Epilepsy / Convulsions",
                                 "Leukemia",
                                 "Diabetes",
                                 "Kidney Diseases",
                                 "AIDS or HIV Infection",
                                 "Thyroid Problem",
                                 "Heart Disease",
                                 "Cardiac pacemaker",
                                 "Heart Murmur",
                                 "Mitral Valve Prolapse",
                                 "Angina",
                                 "Frequently Tired",
                                 "Emphysema"]
        
        let quest22: [String] = ["Cancer",
                                 "Radiation Therapy",
                                 "Arthritis",
                                 "Joint Replacement or Implant",
                                 "Hepatitis / Jaundice",
                                 "Sexually Transmitted Disease",
                                 "Stomach Troubles / Ulcers",
                                 "Chest Pains",
                                 "Stroke",
                                 "Hay Fever / Allergies",
                                 "Tuberculosis",
                                 "Glaucoma",
                                 "Recent Weight Loss",
                                 "Liver Disease",
                                 "Respiratory Problems",
                                 "Other"]
        
        let quest3: [String] = ["Local Anesthetics (e.g. Novocain)",
                                "Penicillin or any other Antibiotics",
                                "Sulfa Drugs",
                                "Sedatives",
                                "Iodine",
                                "Any Metals (e.g. nickel, mercury, etc.)",
                                "Latex Rubber",
                                "Other"]
        
        self.questionGroup1 = PDQuestion.arrayOfQuestions(quest1)
        self.questionGroup1[1].isAnswerRequired = true
        self.questionGroup1[2].isAnswerRequired = true
        
        self.questionGroup2 = [PDQuestion.arrayOfQuestions(quest21), PDQuestion.arrayOfQuestions(quest22)]
        self.questionGroup2[1].first?.isAnswerRequired = true
        self.questionGroup2[1].last?.isAnswerRequired = true
        
        self.questionGroup3 = PDQuestion.arrayOfQuestions(quest3)
        self.questionGroup3.last?.isAnswerRequired = true
    }
}

class Service: NSObject {
    
    var signatureView : SignatureView!
    var serviceDescription : String!
    var index : Int!
    
    override init() {
        super.init()
    }
    
    init(serviceName : String) {
        super.init()
        self.serviceDescription = serviceName
        signatureView = SignatureView()
    }
    
    class func getAllServices() -> [Service] {
        let arrayServices = ["1. FILLINGS:\nI understand that a more extensive restoration than originally place, or possibly root canal therapy, may be required due to addition conditions discovered during tooth preparation. I understand that significant changes in response to temperature may occur after too restoration such  as temporary sensitivity or pain. I also understand that if my tooth does not respond to treatment with a filling, require periodic replacement with additional fillings and/or crowns. I understand I may need further treatment in this office or possibly by a specialist if complications arise during treatment, and any costs thus incurred are my responsibility.",
                             "2. DRUGS AND MEDICATIONS:\nI understand that antibiotics, analgesics, anesthetics and other medications can cause allergic reactions, resulting in redness and swelling of tissues, itching, pain, nausea vomiting or more severe allergic reactions which, although rare, can lead to death. I have informed the doctor of any known allergies. Certain medications may cause drowsiness and it is advisable not to drive or operate hazardous equipment when taking such drugs.",
                             "3. RISKS OF DENTAL ANESTHESIA\nI understand that pain, bruising and occasional temporary or sometimes-permanent numbness in lips, cheeks, tongue or associated facial structures can occur with local anesthetics. About 90% of these cases resolves themselves in less than 8 weeks. Although very rarely needed, a referral to a specialist for evaluation and possibly treatment may be needed if the symptoms do not resolve.",
                             "4. Due to unique differences in each patient’s oral cavity and oral hygiene abilities there is always a risk for relapse,recurrence and/or failure restorations. I understand that it is impossible to predict if and how fast my condition would worsen if untreated, but it is the doctor’s opinion that therapy would be helpful and worsening of the condition(s) would occur sooner without the recommended treatment.",
                             "5. CHANGES IN TREATMENT PLAN:\nI understand that during the course of treatment it may be necessary to change or add procedures because of conditions discovered during treatment that were not evident during examination. I authorize my doctor to use professional judgment to provide appropriate care and understand that the fee proposed is subject to change, depending upon those unforeseen or undiagnosed conditions that may only become apparent once treatment has begun."]
        var serviceList = [Service]()
        for (idx, dict) in arrayServices.enumerated() {
            let obj = Service(serviceName: dict)
            obj.index = idx
            serviceList.append(obj)
        }
        return serviceList
    }
    
}


