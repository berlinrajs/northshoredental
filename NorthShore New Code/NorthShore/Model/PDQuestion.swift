//
//  PDQuestion.swift
//  ProDental
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDQuestion: NSObject {
    var question : String!
    var isAnswerRequired : Bool!
    var answer : String?
    var explanation: String?
    var options: [PDOption]?
    var selectedOption : Bool! {
        didSet {
            if selectedOption == false
            {
                self.answer = nil
            }
        }
    }
    
    override init() {
        super.init()
    }
    
    init(dict : NSDictionary) {
        super.init()
        self.question = dict["question"] as! String
        self.isAnswerRequired = (dict["verification"] as! String) == "Yes"
        self.selectedOption = false
    }
    
    class func getObjects (_ arrayResult : NSArray) -> [PDQuestion] {
        var questions  = [PDQuestion]()
        for dict in arrayResult {
            let obj = PDQuestion(dict: dict as! NSDictionary)
//            obj.selectedOption = false
            questions.append(obj)
        }
        return questions
    }
    
    
    init(question: String) {
        super.init()
        self.question = question
        self.isAnswerRequired = false
        self.selectedOption = false
    }
    
    
    class func arrayOfQuestions(_ array: [String]) -> [PDQuestion]{
        var questions = [PDQuestion]()
        for string in array {
            let question = PDQuestion(question: string)
            question.selectedOption = false
            questions.append(question)
        }
        return questions
    }
    
    class func getArrayOfQuestions (questions : [String]) -> [PDQuestion] {
        var arrayQuestions  = [PDQuestion]()
        for quest in questions {
            let obj = PDQuestion(question: quest)
            arrayQuestions.append(obj)
        }
        return arrayQuestions
    }
    
    class func getJSONObject(_ responseString : String) -> AnyObject? {
        do {
            let object = try JSONSerialization.jsonObject(with: responseString.data(using: String.Encoding.utf8)!, options: JSONSerialization.ReadingOptions.allowFragments)
            return object as AnyObject
        } catch {
            return nil
        }
    }

 
    
    class func fetchQuestionsForm1(_ completion:(_ result : [PDQuestion]?, _ success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[{\"question\":\"Are you currently in pain?\",\"verification\":\"Yes\"},{\"question\":\"Do you require antibiotics before dental treatment?\",\"verification\":\"Yes\"},{\"question\":\"Have you experienced problems associated with any previous dental work?\",\"verification\":\"Yes\"},{\"question\":\"Do you know or have you ever experienced pain / discomfort in your jaw joint(TMJ/TMD)?\",\"verification\":\"Yes\"},{\"question\":\"Do you floss daily? \",\"verification\":\"Yes\"},{\"question\":\"Do you Brush daily?\",\"verification\":\"Yes\"},{\"question\":\"Do you use anything in addition to your brush and floss?\",\"verification\":\"Yes\"},{\"question\":\"Would you like fresher breath?\",\"verification\":\"Yes\"},{\"question\":\"Would you like Whiter teeth?\",\"verification\":\"Yes\"},{\"question\":\"Do your gums ever bleed?\",\"verification\":\"Yes\"},{\"question\":\"Do your gums ever Itch?\",\"verification\":\"Yes\"},{\"question\":\"Have you ever had periodontal disease?\",\"verification\":\"Yes\"},{\"question\":\"Do you have mobility in your teeth?\",\"verification\":\"Yes\"},{\"question\":\"Do you still have wisdom teeth?\",\"verification\":\"Yes\"}]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
        
        //        ServiceManager.fetchDataFromService("general_api.php", parameters: nil, success: { (result) -> Void in
        //            completion(result: self.getObjects(result["posts"] as! NSArray), success: true)
        //            print(result)
        //            }) { (error) -> Void in
        //                completion(result: nil, success: false)
        //        }
    }

    class func fetchQuestionsMedicalForm3(_ completion:(_ result : [PDQuestion]?, _ success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[{\"question\":\"Are you currently under the care of a physician?\",\"verification\":\"Yes\"},{\"question\":\"Do you smoke or use tobacco in any other form?\",\"verification\":\"Yes\"},{\"question\":\"Have you been told that you snore or hold your breath while sleeping or wake up gasping for breath?\",\"verification\":\"Yes\"},{\"question\":\"Have your taken Fosamax or any other Bisphosphonate? \",\"verification\":\"Yes\"}]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
        
        //        ServiceManager.fetchDataFromService("general_api.php", parameters: nil, success: { (result) -> Void in
        //            completion(result: self.getObjects(result["posts"] as! NSArray), success: true)
        //            print(result)
        //            }) { (error) -> Void in
        //                completion(result: nil, success: false)
        //        }
    }
    
    class func fetchQuestionsMedicalForm5(_ completion:(_ result : [PDQuestion]?, _ success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[{\"question\":\"Aspirin\",\"verification\":\"Yes\"},{\"question\":\"Barbiturates\",\"verification\":\"Yes\"},{\"question\":\"Codeine\",\"verification\":\"Yes\"},{\"question\":\"Dental Anesthetics\",\"verification\":\"Yes\"},{\"question\":\"Erythromycin \",\"verification\":\"Yes\"},{\"question\":\"Jewelry/Metals \",\"verification\":\"Yes\"},{\"question\":\"Latex \",\"verification\":\"Yes\"},{\"question\":\"Penicillin \",\"verification\":\"Yes\"},{\"question\":\"Sedatives \",\"verification\":\"Yes\"},{\"question\":\"Sulfa Drugs \",\"verification\":\"Yes\"},{\"question\":\"Tetracycline \",\"verification\":\"Yes\"},{\"question\":\"Other \",\"verification\":\"Yes\"}]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
        
        //        ServiceManager.fetchDataFromService("general_api.php", parameters: nil, success: { (result) -> Void in
        //            completion(result: self.getObjects(result["posts"] as! NSArray), success: true)
        //            print(result)
        //            }) { (error) -> Void in
        //                completion(result: nil, success: false)
        //        }
    }
    
    
    
 
    
}





class PDOption : NSObject {
    var question : String!
    var isSelected : Bool?
    var index : Int!
    
    init(value : String) {
        super.init()
        self.question = value
    }
    
    class func arrayOfOptions(_ array: [String]) -> [PDOption]{
        var questions  = [PDOption]()
        for value in array {
            let obj = PDOption(value: value)
            obj.isSelected = false
            questions.append(obj)
        }
        return questions
    }
    
    
    class func getObjects (_ arrayResult : NSArray) -> [PDOption] {
        var questions  = [PDOption]()
        for (idx, value) in arrayResult.enumerated() {
            let obj = PDOption(value: value as! String)
            obj.index = idx
            obj.isSelected = false
            questions.append(obj)
        }
        return questions
    }
    
    
    class func fetchQuestionsToothExtractionForm1(_ completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        let responseString = "{\"Status\":\"success\",\"posts\":[\"Pain\",\"Infection\",\"Decay\",\"Gum Disease\",\"Broken tooth\",\"Non Restorable\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
    }
    
    class func fetchQuestionsToothExtractionForm2(_ completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        let responseString = "{\"Status\":\"success\",\"posts\":[\"No Treatment\",\"Root Canal Therapy\",\"Filling\",\"Crowns\",\"Gum Treatment\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
    }
    
    class func fetchQuestionsForm4(_ completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[\"Abnormal Bleeding\",\"Alcohol Abuse\",\"Anemia\",\"Arthritis\",\"Artificial Bones/joints\",\"Artificial valves\",\"Asthma\",\"Blood Transfusion\",\"Cancer\",\"Chemotherapy\",\"Chicken Pox\",\"Colitis\",\"Congenital Heart Defect\",\"Diabetes\",\"Difficulty Breathing\",\"Drug Abuse\",\"Emphysema\",\"Epilepsy\",\"Fainting spells\",\"Fever Blisters\",\"Glaucoma\",\"Hay fever\",\"Headaches \",\"Heart Attack\",\"Heart Murmur\",\"Heart Surgery\",\"Hemophilia \",\" Hepatitis\",\"Herpes\",\"High Blood Pressure\",\"HIV+/AIDS \",\"Hospitalized for Any Reason\",\"Kidney Problem\",\"Liver Disease\",\"Low Blood Pressure\",\"Lupus\",\"Mitral Valve Prolapse\",\"Osteoporosis/Pagets Disease\",\"Pacemaker\",\"Persistent Cough\",\"Psychiatric Treatment\",\"Radiation Treatment\",\"Rheumatic fever\",\"Scarlet fever\",\"Seizures\",\"Shingles\",\"Sickle Cell Disease\",\"Sinus Problems\",\"Steroid Therapy\",\"Stroke\",\"Thyroid Problems\",\"Tonsillitis\",\"Tuberculosis(TB)\",\"Ulcers\",\"Venereal Disease\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
        
        //        ServiceManager.fetchDataFromService("disease_api.php", parameters: nil, success: { (result) -> Void in
        //            completion(result: self.getObjects(result["posts"] as! NSArray), success: true)
        //            print(result)
        //            }) { (error) -> Void in
        //                completion(result: nil, success: false)
        //        }
    }
    
    
    
    class func fetchQuestionsForm6(_ completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[\"Acetaminophen\",\"Antibiotics\",\"Antihistamines\",\"Aspirin\",\"Blood Thinners\",\"Blood Pressure Medication\",\"Cold Remedies\",\"Digitalis/Heart Medication\",\"Insulin/Diabetes Drugs\",\"Nitroglycerin\",\"Recreational Drugs\",\"Steroids/Cortisone\",\"Thyroid medicine\",\"Tranquilizers\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
        
        //        ServiceManager.fetchDataFromService("disease_api.php", parameters: nil, success: { (result) -> Void in
        //            completion(result: self.getObjects(result["posts"] as! NSArray), success: true)
        //            print(result)
        //            }) { (error) -> Void in
        //                completion(result: nil, success: false)
        //        }
    }


    
    class func hippaForm1(_ completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        let questions = ["I have read the material available at the front desk.",
                         "I would like to request a printed copy to take with me."]
        completion(self.getObjects(questions as NSArray), true)
    }
    
    
    class func hippaForm2(_ isChild : Bool, completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        let questions = ["You may use my \(isChild == true ? "child’s" : "") first name & photo on your Web site gallery and social media pages. This may include Facebook, Google+, Twitter, Instagram, and Pinterest.",
                         "You may use my \(isChild == true ? "child’s" : "") photo as referenced above, but no name.",
                         "I would prefer that my \(isChild == true ? "child’s" : "") smile is not featured online."]
        completion(self.getObjects(questions as NSArray), true)
    }
}
