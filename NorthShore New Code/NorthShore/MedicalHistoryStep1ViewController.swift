//
//  MedicalHistoryStep1ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryStep1ViewController: PDViewController {
    
    var medicalHistoryStep1 : [PDQuestion]! = [PDQuestion]()
    var selectedButton : RadioButton!
    var fetchCompleted : Bool! = false
    
    @IBOutlet weak var ToContactDentistry: PDTextView!
    @IBOutlet weak var tableViewQuestions: UITableView!
    
    @IBOutlet var viewPopup: PDView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textViewAnswer: PDTextView!
    @IBOutlet weak var buttonVerified: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    
    func saveValues() {
        patient.medicalHistoryQuestions1 = self.medicalHistoryStep1
        patient.YCometoDentistry = ToContactDentistry.text == "TYPE HERE" ? "" : ToContactDentistry.text
    }
    
    func loadValues() {
        if patient.medicalHistoryQuestions1 == nil {
            fetchData()
        } else {
            self.medicalHistoryStep1 = patient.medicalHistoryQuestions1
            self.fetchCompleted = true
            self.tableViewQuestions.reloadData()
        }
        
        self.textViewDidBeginEditing(ToContactDentistry)
        ToContactDentistry.text = patient.YCometoDentistry
        self.textViewDidEndEditing(ToContactDentistry)
    }
    
    func fetchData() {
        PDQuestion.fetchQuestionsForm1 { (result, success) -> Void in
            self.fetchCompleted = true
            if success {
                self.medicalHistoryStep1.append(contentsOf: result!)
                self.tableViewQuestions.reloadData()
            }
        }
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if self.fetchCompleted == true {
            if !buttonVerified.isSelected {
                let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
                self.present(alert, animated: true, completion: nil)
            } else {
                saveValues()
                
                let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
                let medicalHistoryStep2VC = storyboard.instantiateViewController(withIdentifier: "kMedicalHistory2VC") as! MedicalHistory2VC
                medicalHistoryStep2VC.patient = patient
                self.navigationController?.pushViewController(medicalHistoryStep2VC, animated: true)
            }
        }
    }
    
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func buttonActionPopupOk(_ sender: AnyObject) {
        let obj = medicalHistoryStep1[selectedButton.tag]
        if !textViewAnswer.isEmpty && textViewAnswer.text != "TYPE HERE" {
            obj.answer = textViewAnswer.text
            obj.selectedOption = true
            
            patient.wisdomTeethReason = textViewAnswer.text
        } else {
            selectedButton.isSelected = false
            obj.selectedOption = false
        }
        textViewAnswer.resignFirstResponder()
        self.viewPopup.removeFromSuperview()
        self.viewShadow.isHidden = true
    }
    
    
    func findEmptyValue() -> PDQuestion? {
        for question in self.medicalHistoryStep1 {
            if question.selectedOption == nil {
                return question
            }
        }
        return nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MedicalHistoryStep1ViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

extension MedicalHistoryStep1ViewController : MedicalHistoryCellDelegate {
    func radioButtonAction(_ sender: RadioButton) {
        selectedButton = sender
        
        if selectedButton.tag == 13 {
            let frameSize = CGRect(x: 0, y: 0, width: 348, height: 174)
            self.viewPopup.frame = frameSize
            self.viewPopup.center = self.view.center
            self.viewShadow.addSubview(self.viewPopup)
            self.viewPopup.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            textViewAnswer.text = "TYPE HERE"
            textViewAnswer.textColor = UIColor.lightGray
            self.viewShadow.isHidden = false
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.3)
            self.viewPopup.transform = CGAffineTransform.identity
            UIView.commitAnimations()
        } else {
            let obj = medicalHistoryStep1[selectedButton.tag]
            obj.selectedOption = true
        }
    }
}


extension MedicalHistoryStep1ViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return medicalHistoryStep1.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryStep1", for: indexPath) as! MedicalHistoryStep1TableViewCell
        let obj = medicalHistoryStep1[indexPath.row]
        cell.configureCell(obj)
        cell.buttonYes.tag = indexPath.row
        cell.buttonNo.tag = indexPath.row
        cell.delegate = self
        return cell
    }
}

