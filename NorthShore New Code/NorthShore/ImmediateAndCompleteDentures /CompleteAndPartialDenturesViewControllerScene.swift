//
//  ViewController.swift
//  AL-ShifaDentistry
//
//  Created by SRS Web Solutions on 20/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CompleteAndPartialDenturesViewControllerScene: PDViewController {
    
    
    @IBOutlet var imageViewSignature1: SignatureView!
    @IBOutlet var imageViewSignature2: SignatureView!
    
    @IBOutlet var labelDate1: DateLabel!
    @IBOutlet var labelDate2: DateLabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        // Do any additional setup after loading the view, typically from a nib.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.ImmediateSignature1 = imageViewSignature1.isSigned() ? imageViewSignature1.signatureImage() : nil
        patient.ImmediateSignature2 = imageViewSignature2.isSigned() ? imageViewSignature2.signatureImage() : nil
    }
    func loadValues() {
        imageViewSignature1.previousImage = patient.ImmediateSignature1
        imageViewSignature2.previousImage = patient.ImmediateSignature2
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if !imageViewSignature1.isSigned() || !imageViewSignature2.isSigned()  {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate1.dateTapped || !labelDate2.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            let nextViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "CompleteAndPartialDenturesFormViewController") as! CompleteAndPartialDenturesFormViewController
            nextViewControllerObj.patient = self.patient
            self.navigationController?.pushViewController(nextViewControllerObj, animated: true)
        }
    }
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

