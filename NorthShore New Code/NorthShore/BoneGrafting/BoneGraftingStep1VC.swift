//
//  BoneGraftingStep1VC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 14/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class BoneGraftingStep1VC: PDViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonVerified: UIButton!
  
    var boneGraftingQuestions: [PDQuestion] = [PDQuestion]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.boneGraftingQuestions = boneGraftingQuestions
    }
    func loadValues() {
        if patient.boneGraftingQuestions != nil {
            boneGraftingQuestions = patient.boneGraftingQuestions
        } else {
            let stringQuestion = ["History of taking bisphosphonates (i.e. Boniva, Fosamax, Actonel, Recast)", "Radiation treatment to the head or neck area", "Bleeding problems", "Taking blood thinner medications", "Taking daily aspirin", "Taking anticoagulants (i.e. Coumadin, Plavix, Love NOx, Fragmin, Angiomax)", "Predisposed to Asthma or Hives", "Pregnant, recent pregnancy or nursing"]
            
            for question in stringQuestion {
                let quest = PDQuestion(dict: ["question": question, "verification": "No"])
                quest.selectedOption = false
                boneGraftingQuestions.append(quest)
            }
        }
        tableView.reloadData()
    }

    @IBAction func buttonBackAction(_ sender: PDButton) {
        self.buttonActionBack(sender)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonNextAction(_ sender: PDButton) {
        self.view.endEditing(true)
        if !buttonVerified.isSelected {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let step2 = self.storyboard?.instantiateViewController(withIdentifier: "kBoneGraftingStep2VC") as! BoneGraftingStep2VC
            step2.patient = patient
            self.navigationController?.pushViewController(step2, animated: true)
        }
    }
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension BoneGraftingStep1VC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 || indexPath.row == 5 ? 45 : 34
    }
}
extension BoneGraftingStep1VC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return boneGraftingQuestions.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MedicalHistoryStep1TableViewCell
        let obj = boneGraftingQuestions[indexPath.row]
        
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        cell.question = obj
        cell.labelTitle.text = obj.question
        cell.buttonYes.isSelected = obj.selectedOption
        
        return cell
    }
}
