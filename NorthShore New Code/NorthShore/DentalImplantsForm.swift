//
//  GingivalGraftingForm.swift
//  NorthShore
//
//  Created by Leojin Bose on 7/12/16.
//  Copyright © 2016 SRS. All rights reserved.
//
import UIKit

class DentalImplantsForm: PDViewController {
    
    @IBOutlet weak var ImageviewSignature1: UIImageView!
    
    
    @IBOutlet weak var dateLabel1: UILabel!
    
    
    @IBOutlet weak var relationName: UILabel!
    
    @IBOutlet weak var ImageviewSignature2: UIImageView!
    
    @IBOutlet weak var patientName: UITextView!
    @IBOutlet weak var datelabel2: UILabel!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        ImageviewSignature1.image = patient.DentalImplantsignature1
        
            ImageviewSignature2.image = patient.DentalImplantsignature2
        
        dateLabel1.text = patient.dateToday
        
        datelabel2.text = patient.dateToday
        
        patientName.text = patient.fullName
        
        if (patient.DentalImplantrelationship == "") {
            
            relationName.text = "N/A"
        } else {
        
        
        relationName.text = patient.DentalImplantrelationship
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}