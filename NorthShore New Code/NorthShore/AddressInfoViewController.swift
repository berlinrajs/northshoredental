//
//  AddressInfoViewController.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AddressInfoViewController: PDViewController {
    
   
    @IBOutlet weak var textFieldAddressLine: PDTextField!
    
    @IBOutlet weak var textFieldState: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldZipCode: PDTextField!
    @IBOutlet weak var textFieldEmail: PDTextField!
    @IBOutlet weak var textFieldHomePhone: PDTextField!
    @IBOutlet weak var textFieldSecurityNumber: PDTextField!
    
    @IBOutlet weak var textFieldDriverLicense: PDTextField!
    @IBOutlet weak var textFieldWorkPhone: PDTextField!
    @IBOutlet weak var textFieldCell: PDTextField!
    @IBOutlet weak var textFieldExtension: PDTextField!
    @IBOutlet weak var radioButtonMaritialStatus: RadioButton!
    
    @IBOutlet weak var radioHearAboutUs: RadioButton!
    
    var arrayStates : [String] = [String]()
    var referenceDetails : String = ""
    var othersDetails : String = ""
    
    @IBOutlet weak var textViewBesttimeTomeet: PDTextView!
    
    @IBOutlet weak var otherFamilyMembers: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        textFieldSecurityNumber.placeholder = "SOCIAL SECURITY NUMBER"
        
        StateListView.addStateListForTextField(textFieldState)
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.addressLine = textFieldAddressLine.text!
        patient.state = textFieldState.text!
        patient.socialSecurityNumber = textFieldSecurityNumber.text!
        
        patient.zipCode = textFieldZipCode.text
        patient.city = textFieldCity.text
        patient.email = textFieldEmail.text?.lowercased()
        patient.HomephoneNumber = textFieldHomePhone.text
        patient.workphoneNumber = textFieldWorkPhone.text
        
        patient.AddrcellphoneNumber = textFieldCell.text
        patient.drivingLicense = textFieldDriverLicense.text
        patient.maritialStatus = radioButtonMaritialStatus.selected == nil ? 7 : radioButtonMaritialStatus.selected.tag
        
        patient.radioHearAboutUsTag = radioHearAboutUs.selected == nil ? 0 : radioHearAboutUs.selected.tag
        patient.referredByDetails = referenceDetails
        patient.othersDetails = othersDetails
        
        patient.Timetomeet = textViewBesttimeTomeet.text == "TYPE HERE" ? "" : textViewBesttimeTomeet.text
        patient.OtherFamilymember = otherFamilyMembers.text
    }
    func loadValues() {
        self.othersDetails = patient.othersDetails == nil ? "" : patient.othersDetails
        self.referenceDetails = patient.referredByDetails == nil ? "" : patient.referredByDetails
        textFieldAddressLine.text = patient.addressLine
        textFieldState.text = patient.state == nil || patient.state.isEmpty ? "IL" : patient.state
        textFieldSecurityNumber.text = patient.socialSecurityNumber
        
        textFieldZipCode.text = patient.zipCode
        textFieldCity.text = patient.city
        textFieldEmail.text = patient.email
        textFieldHomePhone.text = patient.HomephoneNumber
        textFieldWorkPhone.text = patient.workphoneNumber
        
        textFieldCell.text = patient.AddrcellphoneNumber
        textFieldDriverLicense.text = patient.drivingLicense
        
        radioButtonMaritialStatus.setSelectedWithTag(patient.maritialStatus)
        radioHearAboutUs.setSelectedWithTag(patient.radioHearAboutUsTag == nil ? 0 : patient.radioHearAboutUsTag!)
        
        otherFamilyMembers.text = patient.OtherFamilymember
        
        self.textViewDidBeginEditing(textViewBesttimeTomeet)
        textViewBesttimeTomeet.text = patient.Timetomeet
        self.textViewDidEndEditing(textViewBesttimeTomeet)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if let _ = findEmptyTextField() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldZipCode.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID ZIP CODE")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldSecurityNumber.isEmpty && !textFieldSecurityNumber.text!.isSocialSecurityNumber {
            let alert = Extention.alert("PLEASE ENTER VALID SOCIAL SECURITY NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldHomePhone.isEmpty && !textFieldHomePhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID HOME PHONE")
            self.present(alert, animated: true, completion: nil)
        }  else if !textFieldCell.isEmpty && !textFieldCell.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID CELL PHONE")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldWorkPhone.isEmpty && !textFieldWorkPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID WORK PHONE")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldEmail.isEmpty && !textFieldEmail.text!.isValidEmail {
            let alert = Extention.alert("PLEASE ENTER VALID EMAIL")
            self.present(alert, animated: true, completion: nil)
        } else if radioButtonMaritialStatus.selected == nil  {
            let alert = Extention.alert("PLEASE SELECT YOUR MARITIAL STATUS")
            self.present(alert, animated: true, completion: nil)                           
        } else {
            saveValues()
            
            let contactInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kContactInfoVC") as! ContactInfoViewController
            contactInfoVC.patient = patient
            self.navigationController?.pushViewController(contactInfoVC, animated: true)
        }
    }
    
    
    @IBAction func radioHearAboutUsTag(_ sender: AnyObject) {
        if sender.tag == 8 {
            PopupTextField.sharedInstance.showWithTitle("",placeHolder:"REFERRED BY" ,keyboardType: UIKeyboardType.default, textFormat: .default, completion: { (textField, isEdited) in
                if !textField.isEmpty {
                    self.referenceDetails = textField.text!
                } else {
                    self.referenceDetails = ""
                    self.radioHearAboutUs.deselectAllButtons()
                }
            })
        } else if sender.tag == 9 {
            PopupTextField.sharedInstance.showWithTitle( "" , placeHolder: "PLEASE SPECIFY" ,keyboardType: UIKeyboardType.default, textFormat: .default, completion: { (textField, isEdited) in
                if !textField.isEmpty {
                    self.othersDetails = textField.text!
                } else {
                    self.othersDetails = ""
                    self.radioHearAboutUs.deselectAllButtons()
                }
            })
        }
    }
    
    func findEmptyTextField() -> UITextField? {
        let textFields = is18YearsOld ? [textFieldCity, textFieldState, textFieldZipCode, textFieldAddressLine] : [textFieldCity, textFieldState, textFieldZipCode, textFieldAddressLine]
        for textField in textFields {
            if (textField?.isEmpty)! {
                return textField
            }
        }
        return nil
    }
    
    var is18YearsOld : Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        let birthDate = dateFormatter.date(from: patient.dateOfBirth.capitalized)
        let ageComponents = (Calendar.current as NSCalendar).components(.year, from: birthDate!, to: Date(), options: NSCalendar.Options(rawValue: 0))
        return ageComponents.year! >= 18
    }
}

extension AddressInfoViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textFieldZipCode {
            return textField.formatZipCode(range, string: string)
        } else if textField == textFieldSecurityNumber {
            return textField.formatNumbers(range, string: string, count: 9)
        } else if textField == textFieldHomePhone || textField == textFieldWorkPhone || textField == textFieldCell {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension AddressInfoViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.isEmpty {
            textView.text = "TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
