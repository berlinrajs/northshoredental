//
//  Forms.swift
//  CarePointDental
//
//  Created by samadsyed on 2/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
public let screenSize = UIScreen.main.bounds.size

let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"

let kNewPatientSignInForm = "WELCOME FORM"
let kMedicalHistoryForm = "MEDICAL HISTORY FORM"

let kPatientAuthorization = "PATIENT AUTHORIZATION"
let kDentalHealthHistory = "DENTAL HEALTH HISTORY"
let kInsuranceCard = "SCAN INSURANCE CARD"
let kDrivingLicense = "SCAN DRIVING LICENSE"
let kVisitorCheckForm = "VISITOR CHECK IN FORM"
let kConsentForms = "CONSENT FORMS"
let kSelfieForm = "PATIENT PHOTO"

//let kNewPatientSignInForm = "NEW PATIENT SIGN-IN FORM"

let kGingivalGraftingSurgery =  "GINGIVAL GRAFTING SURGERY"
let kConsentImplants = "DENTAL IMPLANT(S)"


let kBoneGrafting = "CONSENT FOR BONE GRAFTING"
let kEndodonticTreatment = "ENDODONTIC TREATMENT"
let kCrownAndBridges = "CROWNS AND BRIDGES"
let kCompleteAndPartialDentures = "IMMEDIATE COMPLETE AND PARTIAL DENTURES"
let kTreatmentFillings = "CONSENT TO TREATMENT FILLING"
let kMedicalClearance = "MEDICAL CLEARANCE REQUEST FORM"
let kGumDisease = "NOTIFICATION OF PERIODONTAL (GUM) DISEASE"
let kRefusalDentalTreatment = "REFUSAL OF DENTAL TREATMENT"
let kRefusalofTreatment = "REFUSAL OF TREATMENT"
let kReleaseRecord = "RELEASE RECORD DENTAL"
let kToothWhitening = "TOOTH WHITENING TREATMENT"
let klazerWhitening = "LED TEETH WHITENING"
let kExtractionConsent = "EXTRACTION CONSENT FORM"
let kSedationConsent = "ORAL SEDATION CONSENT FORM"

let kDentalCrown = "DENTAL CROWN"
let kInofficeWhitening = "INOFFICE WHITENING"
let kTreatmentOfChild = "TREATMENT OF A MINOR CHILD"


let kToothExtraction = "TOOTH EXTRACTION"
let kPhotographyOrTestimonial = "PHOTOGRAPHY/TESTIMONIALS"

let kOpiodForm = "OPIOID FORM"
let kFeedBack = "CUSTOMER REVIEW FORM"


let toothNumberRequired = [kToothExtraction,kDentalCrown,kEndodonticTreatment,kOpiodForm]


extension String {
    var fileName : String {
        return self.replacingOccurrences(of: " - ", with: "_").replacingOccurrences(of: " ", with: "_").replacingOccurrences(of: "/", with: "_OR_").replacingOccurrences(of: ",", with: "")
    }
}


class Forms: NSObject {
    
    var formTitle : String!
    var subForms : [Forms]!
     var additionalDetail : String!
    var isSelected : Bool!
    var toothNumbers : String!
    var isToothNumberRequired : Bool!
    var index : Int!
    
    init(formDetails : NSDictionary) {
        super.init()
        self.isSelected = false
    }
    
    override init() {
        super.init()
    }
    
    class func getAllForms (_ completion :(_ isConnectionfailed: Bool, _ forms : [Forms]?) -> Void) {
        let isConnected = Reachability.isConnectedToNetwork()
        let forms = [kVisitorCheckForm, kNewPatientSignInForm,kPatientAuthorization, kDentalHealthHistory, kInsuranceCard,kDrivingLicense,kSelfieForm,kConsentForms, kFeedBack]
        let formObj = getFormObjects(forms, isSubForm: false)
        completion(isConnected ? false : true, formObj)
    }
    
    fileprivate class func getFormObjects (_ forms : [String], isSubForm : Bool) -> [Forms] {
        var formList : [Forms]! = [Forms]()
        for (idx, form) in forms.enumerated() {
            let formObj = Forms()
            formObj.isSelected = false
            formObj.index = isSubForm ? idx + 8 : idx
            formObj.formTitle = form
            formObj.isToothNumberRequired = toothNumberRequired.contains(form)
            if formObj.formTitle == kConsentForms {
                formObj.subForms = getFormObjects([kGingivalGraftingSurgery,kConsentImplants,kBoneGrafting,kEndodonticTreatment,kCrownAndBridges,kCompleteAndPartialDentures,kTreatmentFillings,kMedicalClearance,kGumDisease,kRefusalDentalTreatment,kRefusalofTreatment,kReleaseRecord,kToothWhitening,klazerWhitening,kExtractionConsent,kSedationConsent,kDentalCrown,kInofficeWhitening,kTreatmentOfChild,kToothExtraction,kOpiodForm,kPhotographyOrTestimonial], isSubForm:  true)
            }
            if formObj.formTitle == kFeedBack {
                formObj.index = idx + 28
            }
            formList.append(formObj)
        }
        return formList
    }
    
}
