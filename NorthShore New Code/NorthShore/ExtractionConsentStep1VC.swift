//
//  ToothWhiteningStep1VC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 13/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ExtractionConsentStep1VC: PDViewController {

    @IBOutlet weak var patientSignatureView: SignatureView!
    @IBOutlet weak var DoctorSignature: SignatureView!
    @IBOutlet weak var witnessSignature: SignatureView!
    
    @IBOutlet weak var additionalComments: PDTextView!
    @IBOutlet weak var labelDate1: DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func saveValues() {
        patient.Extractionsignature1 = patientSignatureView.isSigned() ? patientSignatureView.signatureImage() : nil
        patient.Extractionsignature2 = DoctorSignature.isSigned() ? DoctorSignature.signatureImage() : nil
        patient.Extractionsignature3 = witnessSignature.isSigned() ? witnessSignature.signatureImage() : nil
        patient.AdditionalComments = additionalComments.text == "TYPE HERE" ? "" : additionalComments.text 
    }
    
    func loadValues() {
     
        labelDate1.todayDate = patient.dateToday
        
        patientSignatureView.previousImage = patient.Extractionsignature1
        DoctorSignature.previousImage = patient.Extractionsignature2
        witnessSignature.previousImage = patient.Extractionsignature3
        
        self.textViewDidBeginEditing(additionalComments)
        additionalComments.text = patient.AdditionalComments
        self.textViewDidEndEditing(additionalComments)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
   
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if !patientSignatureView.isSigned() || !DoctorSignature.isSigned() || !witnessSignature.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }  else if !labelDate1.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kExtractionConsentFormVC") as! ExtractionConsentFormVC
            formVC.patient = patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
extension ExtractionConsentStep1VC : UITextViewDelegate {
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
