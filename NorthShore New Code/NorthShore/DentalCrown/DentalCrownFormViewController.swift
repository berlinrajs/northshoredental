//
//  DentalCrownFormViewController.swift
//  North Shore
//
//  Created by Office on 2/23/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DentalCrownFormViewController: PDViewController {

    @IBOutlet weak var labelDate1: UILabel!
    @IBOutlet weak var labelDate2: UILabel!
    @IBOutlet weak var imageViewSignature1: UIImageView!
    @IBOutlet weak var imageViewSignature2: UIImageView!

    @IBOutlet weak var labelDetails: UILabel!
    
    var textRanges : [NSRange]! = [NSRange]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageViewSignature1.image = patient.DentalCrownsignature1
        imageViewSignature2.image = patient.DentalCrownsignature2
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        // Do any additional setup after loading the view.
        
        var patientInfo = "I"
        
        let patientName = getText("\(patient.fullName)")
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        let form = patient.selectedForms.filter { (formObj) -> Bool in
            return formObj.formTitle == kDentalCrown
        }
        
        patientInfo = patientInfo + " have been made aware that a crown is recommended on tooth \(getText(form[0].toothNumbers))"
        textRanges.append(patientInfo.rangeOfText(getText(form[0].toothNumbers)))
        patientInfo = patientInfo + " due to root canal  therapy. I understand that my tooth has become weakened due to the root canal and that it is at risk of breaking/fracturing. If a crown is not placed in the above area North Shore is not responsible for any fractures or breakage of that tooth. Furthermore I understand that if my tooth is to break due to not having the recommended crown that it may have to be extracted depending on the fracture. It is my responsibility to have the crown placed within 30 days of the root canal. I have been made fully aware of the consequences if I do not follow the recommended treatment by Dr. Naim."
        
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        }
        
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float)], range: NSMakeRange(0, attributedString.length))
        labelDetails.attributedText = attributedString
        // Do any additional setup after loading the view.
    }
    
   
    
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
