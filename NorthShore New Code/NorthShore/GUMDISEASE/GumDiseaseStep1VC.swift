//
//  GumDiseaseStep1VC.swift
//  TotalHealthDental
//
//  Created by SRS Web Solutions on 07/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class GumDiseaseStep1VC: PDViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var textFieldDate: PDTextField!
    @IBOutlet weak var radioPeriodontics: RadioButton!
    @IBOutlet weak var radioPockets: RadioButton!
    @IBOutlet weak var textFieldPockets: PDTextField!
    @IBOutlet weak var radioAgreement: RadioButton!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    
    @IBOutlet weak var viewPocketsPopup: PDView!
    @IBOutlet weak var viewShadow: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       // buttonBack?.hidden = isFromPreviousForm
        labelDate.todayDate = patient.dateToday
        DateInputView.addDatePickerForTextField(textFieldDate)
        labelName.text = patient.fullName
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.periodontalDate = textFieldDate.text!
        patient.periodontalType = radioPeriodontics.selected.tag
        patient.pocketsSelected = radioPockets.selected.tag
        patient.periodontalagreementSelected = radioAgreement.selected.tag
        patient.periodontalsignature1 = signatureView.isSigned() ? signatureView.signatureImage() : nil
    }
    func loadValues() {
        textFieldDate.text = patient.periodontalDate
        radioPeriodontics.setSelectedWithTag(patient.periodontalType == nil ? 0 : patient.periodontalType!)
        radioPockets.setSelectedWithTag(patient.pocketsSelected == nil ? 0 : patient.pocketsSelected!)
        radioAgreement.setSelectedWithTag(patient.periodontalagreementSelected == nil ? 0 : patient.periodontalagreementSelected!)
        signatureView.previousImage = patient.periodontalsignature1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonNextAction(_ sender: AnyObject) {
        if textFieldDate.isEmpty {
            let alert = Extention.alert("PLEASE SELECT A DATE")
            self.present(alert, animated: true, completion: nil)
        } else if radioPeriodontics.selected == nil {
            let alert = Extention.alert("PLEASE ENTER ALL THE DETAILS NEEDED")
            self.present(alert, animated: true, completion: nil)
        } else if radioPockets.selected == nil {
            let alert = Extention.alert("PLEASE ENTER ALL THE DETAILS NEEDED")
            self.present(alert, animated: true, completion: nil)
        } else if radioAgreement.selected == nil {
            let alert = Extention.alert("PLEASE CONFIRM THE AGREEMENT")
            self.present(alert, animated: true, completion: nil)
        } else if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT FOR DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            let formVC = storyboard?.instantiateViewController(withIdentifier: "kGumDiseaseFormVC") as! GumDiseaseFormVC
            formVC.patient = patient
            navigationController?.pushViewController(formVC, animated: true)
        }
    }
    
    @IBAction func pockets8mmSelected() {
        showPopup()
    }
    
    func showPopup() {
        textFieldPockets.text = ""
        self.viewPocketsPopup.frame = CGRect(x: 0, y: 0, width: 512.0, height: 250.0)
        self.viewPocketsPopup.center = view.center
        self.viewShadow.addSubview(self.viewPocketsPopup)
        self.viewPocketsPopup.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        self.viewShadow.isHidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewPocketsPopup.transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionDone() {
        textFieldPockets.resignFirstResponder()
        
        self.viewPocketsPopup.removeFromSuperview()
        self.viewShadow.isHidden = true
        
        if textFieldPockets.isEmpty {
            radioPockets.deselectAllButtons()
            patient.teethNumbers = ""
        } else {
            patient.teethNumbers = textFieldPockets.text!
        }
    }
}
extension GumDiseaseStep1VC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.lengthOfBytes(using: String.Encoding.utf8) == 0 {
            return true
        }
        if textField == textFieldPockets {
            return textField.formatToothNumber(range, string: string)
        }
        return true
    }
}
