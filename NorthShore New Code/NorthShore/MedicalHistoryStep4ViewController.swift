//
//  MedicalHistoryStep4ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryStep4ViewController: PDViewController {
    
//    var medicalHistoryStep4 : [PDOption]! = [PDOption]()
    var currentIndex : Int = 0
    var fetchCompleted : Bool! = false
    
    @IBOutlet weak var buttonNext: PDButton!
    
    @IBOutlet weak var buttonBackOutlet: PDButton!
    @IBOutlet weak var tableViewQuestions: UITableView!
//    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var buttonVerified: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        
    }
    func loadValues() {
        if patient.medicalHistoryQuestions4 == nil {
            fetchData()
        } else {
            self.fetchCompleted = true
            tableViewQuestions.reloadData()
        }
    }
    
    func fetchData() {
        patient.medicalHistoryQuestions4 = [PDOption]()
        PDOption.fetchQuestionsForm4 { (result, success) -> Void in
            self.fetchCompleted = true
            if success {
                self.patient.medicalHistoryQuestions4.append(contentsOf: result!)
                self.tableViewQuestions.reloadData()
            }
        }
    }
    
    @IBAction func buttonBackView(_ sender: AnyObject) {
        if currentIndex == 0 {
            self.buttonActionBack(sender)
        } else {
            self.buttonActionBack(sender)
//            buttonBackOutlet.userInteractionEnabled = false
//            buttonNext.userInteractionEnabled = false
//            self.buttonVerified.selected = true
//            self.activityIndicator.startAnimating()
//            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.4 * Double(NSEC_PER_SEC)))
//            dispatch_after(delayTime, dispatch_get_main_queue()) {
//                self.activityIndicator.stopAnimating()
//                self.currentIndex = self.currentIndex - 1
//                self.tableViewQuestions.reloadData()
//                self.buttonBackOutlet.userInteractionEnabled = true
//                self.buttonNext.userInteractionEnabled = true
//            }
        }
    }
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if self.fetchCompleted == true {
            if ((currentIndex + 1) * 20) < self.patient.medicalHistoryQuestions4.count {
                if !buttonVerified.isSelected {
                    let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
                    let MedicalHistory4 = storyboard.instantiateViewController(withIdentifier: "kMedicalHistoryStep4VC") as! MedicalHistoryStep4ViewController
                    MedicalHistory4.patient = patient
                    MedicalHistory4.currentIndex = self.currentIndex + 1
                    self.navigationController?.pushViewController(MedicalHistory4, animated: true)
//                    buttonBackOutlet.userInteractionEnabled = false
//                    buttonNext.userInteractionEnabled = false
//                    self.buttonVerified.selected = false
//                    self.activityIndicator.startAnimating()
//                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.4 * Double(NSEC_PER_SEC)))
//                    dispatch_after(delayTime, dispatch_get_main_queue()) {
//                        self.activityIndicator.stopAnimating()
//                        self.currentIndex = self.currentIndex + 1
//                        self.tableViewQuestions.reloadData()
//                        self.buttonBackOutlet.userInteractionEnabled = true
//                        self.buttonNext.userInteractionEnabled = true
//                    }
                }
            } else {
                if !buttonVerified.isSelected {
                    let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
                    let MedicalHistory5 = storyboard.instantiateViewController(withIdentifier: "kMedicalHistoryStep5VC") as! MedicalHistory5ViewController
                    MedicalHistory5.patient = patient
                    self.navigationController?.pushViewController(MedicalHistory5, animated: true)
                }
            }
        }
    }
    
    func findEmptyValue() -> PDOption? {
        let objects = self.patient.medicalHistoryQuestions4.filter { (obj) -> Bool in
            return obj.index >= (currentIndex * 20) && obj.index < ((currentIndex + 1) * 20)
        }
        for question in objects {
            if question.isSelected == nil {
                return question
            }
        }
        return nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension MedicalHistoryStep4ViewController : MedicalHistoryCellDelegate {
    func radioButtonAction(_ sender: RadioButton) {
        self.tableViewQuestions.reloadData()
    }
}


extension MedicalHistoryStep4ViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let objects = self.patient.medicalHistoryQuestions4.filter { (obj) -> Bool in
            return obj.index >= (currentIndex * 20) && obj.index < ((currentIndex + 1) * 20)
        }
        return objects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryStep4", for: indexPath) as! MedicalHistoryStep1TableViewCell
        let index = (currentIndex * 20) + indexPath.row
        let obj = self.patient.medicalHistoryQuestions4[index]
        cell.configureCellOption(obj)
//        if let selected = obj.isSelected {
//            cell.buttonYes.selected = selected
//        } else {
//            cell.buttonYes.deselectAllButtons()
//        }
        cell.buttonYes.tag = index
        cell.buttonNo.tag = index
        cell.delegate = self
        return cell
    }
}
