//
//  GingivalGraftingForm.swift
//  NorthShore
//
//  Created by Leojin Bose on 7/12/16.
//  Copyright © 2016 SRS. All rights reserved.
//
import UIKit

class GingivalGraftingForm: PDViewController {
    
    @IBOutlet weak var ImageviewSignature1: UIImageView!
    @IBOutlet weak var ImageviewSignature2: UIImageView!
    
    @IBOutlet weak var dateLabel1: UILabel!
    @IBOutlet weak var datelabel2: UILabel!
    @IBOutlet weak var relationName: UILabel!
    
    @IBOutlet weak var patientName: UITextView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        ImageviewSignature1.image = patient.Gingivalsignature1
        ImageviewSignature2.image = patient.Gingivalsignature2
        
        dateLabel1.text = patient.dateToday
        datelabel2.text = patient.dateToday
        patientName.text = patient.fullName
        
        if (patient.Gingivalrelationship == "") {
            relationName.text = "N/A"
        } else {
            relationName.text = patient.Gingivalrelationship
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
