//
//  PhotographyOrTestimonialViewControler.swift
//  NorthShore
//
//  Created by SRS Websolutions on 27/08/18.
//  Copyright © 2018 SRS. All rights reserved.
//

import UIKit

class PhotographyOrTestimonialViewControler: PDViewController {

    @IBOutlet weak var patientSign: SignatureView!
     @IBOutlet weak var dateLabel: PDLabel!
    override func viewDidLoad() {
       
        super.viewDidLoad()
       dateLabel.text = patient.dateToday
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(setDateOnLabel))
        tapGesture.numberOfTapsRequired = 1
        dateLabel.addGestureRecognizer(tapGesture)
    }
    func setDateOnLabel() {
        dateLabel.text = patient.dateToday
        dateLabel.textColor = UIColor.black
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func doneBtnAction(_ sender: Any) {
       // PTPatientSign
     patient.PTPatientSign = patientSign.isSigned() ? patientSign.signatureImage() : nil
        
        if !patientSign.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }
        else if dateLabel.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        }
        
        else {
            
            let storyboard = UIStoryboard(name: "DentalHealthHistory", bundle: nil)
            let PTFormsVC = storyboard.instantiateViewController(withIdentifier: "PhotographerFormsVC") as! PhotographerOrTestimonialFormVc
            PTFormsVC.patient = patient
            self.navigationController?.pushViewController(PTFormsVC, animated: true)
        }
    }
}
