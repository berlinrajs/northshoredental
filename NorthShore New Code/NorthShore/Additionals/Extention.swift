//
//  Extention.swift
//  SecureDental
//
//  Created by Leojin Bose on 03/02/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


public func getText(_ text : String) -> String {
    return "  \(text)  "
}

class Extention: NSObject {
    class func alert(_ message : String) -> UIAlertController {
        let alertController = UIAlertController(title: "North Shore", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.destructive) { (action) -> Void in
            
        }
        alertController.addAction(alertOkAction)
        return alertController
    }
}


extension NSError {
    
    convenience init(errorMessage : String) {
        self.init(domain: "Error", code: 101, userInfo: [NSLocalizedDescriptionKey : errorMessage])
    }
    
}
extension UIImage {
    func compressedImageWithRatio(_ ratio: CGFloat)   -> UIImage {
        if self.size.height == 0 || self.size.width == 0 {
            return self
        }
        UIGraphicsBeginImageContext(CGSize(width: self.size.width * ratio, height: self.size.height * ratio))
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width * ratio, height: self.size.height * ratio))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

extension UILabel {
    func setAttributedText() {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        let attributedString = NSAttributedString(string: self.text!,
                                                  attributes: [
                                                    NSParagraphStyleAttributeName: paragraphStyle,
                                                    NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float),
                                                    NSFontAttributeName : self.font
            ])
        self.attributedText = attributedString
    }
}


extension UITextField {
    var isEmpty : Bool {
        return self.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lengthOfBytes(using: String.Encoding.utf8) == 0
    }
    
    func formatToothNumbers(_ range: NSRange, string: String) -> Bool {
        if string.lengthOfBytes(using: String.Encoding.utf8) == 0 {
            return true
        }
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "01234567890,").inverted) != nil {
            return false
        }
        
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let textFieldString = self.text!.replacingCharacters(in: newRange, with: string)
        let textString = textFieldString.components(separatedBy: ",")
        
        if textFieldString.characters.count > 2 {
            let lastString = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 1))
            let lastTwoStrings = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 2))
            let lastThreeStrings = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 3))
            
            if lastTwoStrings == ",," {
                return false
            }
            if lastString == "," && lastThreeStrings.components(separatedBy: ",").count == 3 {
                let requiredString = textFieldString.substring(to: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 2)) + "0" + lastTwoStrings
                self.text = requiredString
                return false
            }
            
        } else {
            if textFieldString.characters.count == 2 {
                let lastString = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 1))
                if lastString == "," {
                    self.text = "0" + textFieldString
                    return false
                }
            }
            if textFieldString == "," {
                return false
            }
        }
        for text in textString {
            if text == "0" {
                return true
            }
            if text == "00" {
                return false
            }
            if Int(text) > 35 {
                return false
            }
        }
        return true
    }
    
    func formatSocialSecurityNumber(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        
        let components = newString.components(separatedBy: CharacterSet.decimalDigits.inverted)
        
        let decimalString : String = components.joined(separator: "")
        let length = decimalString.characters.count
        let decimalStr = decimalString as NSString
        
        
        let hasLeadingOne = length > 0 && decimalStr.character(at: 0) == (1 as unichar)
        
        if length == 0 || (length > 9 && !hasLeadingOne) || length > 10
        {
            let newLength = self.text!.characters.count + string.characters.count - range.length as Int
            
            return (newLength > 9) ? false : true
        }
        var index = 0 as Int
        let formattedString = NSMutableString()
        
        if hasLeadingOne
        {
            formattedString.append("1 ")
            index += 1
        }
        if (length - index) > 3
        {
            let areaCode = decimalStr.substring(with: NSMakeRange(index, 3))
            formattedString.appendFormat("%@-", areaCode)
            index += 3
        }
        if length > 3 && length - index > 2
        {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 2))
            formattedString.appendFormat("%@-", prefix)
            index += 2
        }
        
        let remainder = decimalStr.substring(from: index)
        formattedString.append(remainder)
        self.text = formattedString as String
        return false
    }
    
    func formatPhoneNumber(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        
        let components = newString.components(separatedBy: CharacterSet.decimalDigits.inverted)
        
        let decimalString : String = components.joined(separator: "")
        let length = decimalString.characters.count
        let decimalStr = decimalString as NSString
        
        
        let hasLeadingOne = length > 0 && decimalStr.character(at: 0) == (1 as unichar)
        if length == 0 || (length > 10 && !hasLeadingOne) || length > 11
        {
            let newLength = self.text!.characters.count + string.characters.count - range.length as Int
            return (newLength > 10) ? false : true
        }
        var index = 0 as Int
        let formattedString = NSMutableString()
        
        if hasLeadingOne
        {
            formattedString.append("1 ")
            index += 1
        }
        if (length - index) > 3
        {
            let areaCode = decimalStr.substring(with: NSMakeRange(index, 3))
            formattedString.appendFormat("(%@)", areaCode)
            index += 3
        }
        if length - index > 3
        {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 3))
            formattedString.appendFormat("%@-", prefix)
            index += 3
        }
        
        let remainder = decimalStr.substring(from: index)
        formattedString.append(remainder)
        self.text = formattedString as String
        return false
    }
    
    func formatZipCode(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.characters.count > 5 {
            return false
        }
        return true
    }
    func formatAppartment(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
//        let newRange = self.text!.startIndex.advancedBy(range.location)..<self.text!.startIndex.advancedBy(range.location + range.length)
//        let newString = self.text!.stringByReplacingCharactersInRange(newRange, withString: string)
//        if newString.characters.count > 3 {
//            return false
//        }
        return true
    }
    
    func formatExt(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.characters.count > 3 {
            return false
        }
        return true
    }
    
    
    func formatWeeks(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.characters.count > 2 {
            return false
        }
        return true
    }
    
    func formatNumbers(_ range: NSRange, string: String, count : Int) -> Bool {
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.characters.count > count {
            return false
        }
        return true
    }
    
    func formatToothNumber(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "01234567890,").inverted) != nil {
            return false
        }
        
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let textFieldString = self.text!.replacingCharacters(in: newRange, with: string)
        let textString = textFieldString.components(separatedBy: ",")
        
        if textFieldString.characters.count > 2 {
            let lastString = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 1))
            let lastTwoStrings = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 2))
            let lastThreeStrings = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 3))
            
            if lastTwoStrings == ",," {
                return false
            }
            if lastString == "," && lastThreeStrings.components(separatedBy: ",").count == 3 {
                let requiredString = textFieldString.substring(to: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 2)) + "0" + lastTwoStrings
                self.text = requiredString
                return false
            }
            
        } else {
            if textFieldString.characters.count == 2 {
                let lastString = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 1))
                if lastString == "," {
                    self.text = "0" + textFieldString
                    return false
                }
                
            }
            if textFieldString == "," {
                return false
            }
        }
        
        
        
        for text in textString {
            if text == "0" {
                return true
            }
            if text == "00" {
                return false
            }
            if Int(text) > 35 {
                return false
            }
        }
        return true
    }
    
    func formatDate(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.characters.count > 2 {
            return false
        }
        //        let maximumDateForMonth: [String: Int] = ["": 31, "JAN": 31, "FEB": 29, "MAR": 31 ,"APR": 30 ,"MAY": 31 ,"JUN": 30 ,"JUL": 31 ,"AUG": 31 ,"SEP": 30 ,"OCT": 31 ,"NOV": 30 ,"DEC": 31]
        if Int(newString) > 31 {
            return false
        }
        return true
    }

    
    func formatInitial(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz,").inverted) != nil {
            return false
        }
        let newRange = text!.characters.index(text!.startIndex, offsetBy: range.location)..<text!.characters.index(text!.startIndex, offsetBy: range.location + range.length)
        let newString = text!.replacingCharacters(in: newRange, with: string)
        if newString.characters.count > 1 {
            return false
        }
        return true
    }
}

extension UITextView {
    var isEmpty : Bool {
        return self.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lengthOfBytes(using: String.Encoding.utf8) == 0
    }
}



extension String {
    
    var isValidYear: Bool {
        if self.characters.count != 4 {
            return false
        }
        let components = (Calendar.current as NSCalendar).components(NSCalendar.Unit.year, from: Date())
        if Int(self) > components.year {
            return false
        }
        return true
    }

    public func rangeOfText(_ text : String) -> NSRange {
        return NSMakeRange(self.characters.count - text.characters.count, text.characters.count)
    }
    
    func heightWithConstrainedWidth(_ width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.height
    }
    
    var isValidEmail : Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    var isPhoneNumber: Bool {
        let charcter  = CharacterSet(charactersIn: "+0123456789").inverted
        let inputString:NSArray = self.components(separatedBy: charcter) as NSArray
        let filtered = inputString.componentsJoined(by: "")
        return filtered.characters.count == 10
    }
    var phoneNumber: String {
        let charcter  = CharacterSet(charactersIn: "+0123456789").inverted
        let inputString:NSArray = self.components(separatedBy: charcter) as NSArray
        let filtered = inputString.componentsJoined(by: "")
        return filtered
    }
    var isZipCode: Bool {
        let charcter  = CharacterSet(charactersIn: "0123456789").inverted
        let inputString:NSArray = self.components(separatedBy: charcter) as NSArray
        let filtered = inputString.componentsJoined(by: "")
        return filtered.characters.count == 5
    }
    
    var isSocialSecurityNumber: Bool {
        let charcter  = CharacterSet(charactersIn: "0123456789").inverted
        let inputString:NSArray = self.components(separatedBy: charcter) as NSArray
        let filtered = inputString.componentsJoined(by: "")
        return filtered.characters.count == 9
    }
    
    var socialSecurityNumber: String {
        if characters.count != 9 {
            return ""
        }
        var socialSecurityNumber = String()
        for char in characters {
            socialSecurityNumber.append(char)
            if socialSecurityNumber.characters.count == 3 || socialSecurityNumber.characters.count == 6 {
                socialSecurityNumber.append(Character("-"))
            }
        }
        return socialSecurityNumber
    }
    
    var isValidExt: Bool {
        let charcter  = CharacterSet(charactersIn: "0123456789").inverted
        let inputString:NSArray = self.components(separatedBy: charcter) as NSArray
        let filtered = inputString.componentsJoined(by: "")
        return filtered.characters.count == 3
    }
    
    var is18YearsOld : Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        let birthDate = dateFormatter.date(from: self.capitalized)
        let ageComponents = (Calendar.current as NSCalendar).components(.year, from: birthDate!, to: Date(), options: NSCalendar.Options(rawValue: 0))
        return ageComponents.year! >= 18
    }
    func setTextForArrayOfLabels(_ arrayOfLabels: [UILabel]) {
        
        if arrayOfLabels.count == 0 {
            return
        }
        
        let wordArray = self.components(separatedBy: " ")
        
        var textToCheck: NSString = ""
        
        for string in wordArray {
            let previousLength = textToCheck.length
            let label = arrayOfLabels[0]
            textToCheck = textToCheck.length == 0 ? ((textToCheck as String) + string) as NSString: ((textToCheck as String) + " " + string) as NSString
            
            let size = textToCheck.size(attributes: [NSFontAttributeName: label.font])
            if size.height > label.frame.height || size.width > label.frame.width {
                var array = arrayOfLabels
                array.removeFirst()
                ((self as NSString).replacingCharacters(in: NSMakeRange(0, previousLength), with: "").trimmingCharacters(in: CharacterSet.whitespaces)).setTextForArrayOfLabels(array)
                return
            } else {
                label.text = textToCheck as String
            }
        }
    }
}

extension NSAttributedString {
    func heightWithConstrainedWidth(_ width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil)
        return ceil(boundingBox.height) + 10
    }
    
}
