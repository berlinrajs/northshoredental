//
//  BoneGraftingStep2VC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 14/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class BoneGraftingStep2VC: PDViewController {
  
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: DateLabel!
//    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        labelName.text = patient.fullName
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.BoneGraftingSignature = signatureView.isSigned() ? signatureView.signatureImage() : nil
    }
    func loadValues() {
        signatureView.previousImage = patient.BoneGraftingSignature
    }
    @IBAction func buttonBackAction(_ sender: PDButton) {
        self.buttonActionBack(sender)
    }
    @IBAction func buttonNextAction(_ sender: PDButton) {
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kBoneGraftingFormVC") as! BoneGraftingFormVC
            formVC.patient = patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
//extension BoneGraftingStep2VC: UITableViewDataSource {
//    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        return 1
//    }
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return patient.boneGraftingQuestions.count
//    }
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! MedicalHistoryStep1TableViewCell
//        let obj = patient.boneGraftingQuestions[indexPath.row]
//        
//        cell.backgroundColor = UIColor.clearColor()
//        cell.contentView.backgroundColor = UIColor.clearColor()
//        cell.question = obj
//        cell.labelTitle.text = obj.question
//        cell.buttonYes.selected = obj.selectedOption
//        
//        return cell
//    }
//}
