//
//  HomePageTableViewCell.swift
//  AdultMedicalForm
//
//  Created by SRS Web Solutions on 13/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HomePageTableViewCell: UITableViewCell {

    @IBOutlet var lblName: UILabel!
    
    @IBOutlet var imgViewCheckMark: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    self.backgroundColor = UIColor.clear
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
