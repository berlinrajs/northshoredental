//
//  InOfficeWhiteningViewController.swift
//  North Shore
//
//  Created by Leojin Bose on 2/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class InOfficeWhiteningViewController: PDViewController {

    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var labelDate1: PDLabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let patientName = "\(patient.fullName)"
        labelName.text = patientName
        signatureView1.layer.cornerRadius = 3.0
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(InOfficeWhiteningViewController.setDateOnLabel1))
        tapGesture1.numberOfTapsRequired = 1
        labelDate1.addGestureRecognizer(tapGesture1)
        
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.signatureInOffice = signatureView1.isSigned() ? signatureView1.signatureImage() : nil
    }
    func loadValues() {
        signatureView1.previousImage = patient.signatureInOffice
    }
    
    func setDateOnLabel1() {
        labelDate1.text = patient.dateToday
        labelDate1.textColor = UIColor.black
    }
        
    @IBAction func buttonNextPressed(_ sender: AnyObject) {
        if !signatureView1.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if labelDate1.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            let inOfficeWhiteningFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kInOfficeWhiteningFormVC") as! InOfficeWhiteningFormViewController
            inOfficeWhiteningFormVC.patient = patient
            self.navigationController?.pushViewController(inOfficeWhiteningFormVC, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
