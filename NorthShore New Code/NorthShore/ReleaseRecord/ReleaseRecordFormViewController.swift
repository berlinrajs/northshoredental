//
//  ReleaseRecordFormViewController.swift
//  DiamondDental
//
//  Created by Leojin Bose on 5/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ReleaseRecordFormViewController: PDViewController {


    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var radioButton: RadioButton!
    @IBOutlet weak var labelAuthorizedBy: UILabel!
    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        radioButton.setSelectedWithTag(patient.releaseType)
        let patientName = "\(patient.fullName)"
        labelPatientName.text = patientName
        if patient.releaseType == 3 {
            labelAuthorizedBy.text = patient.authorizedPerson
        }
        imageViewSignature.image = patient.releaseRecordsignature1
        labelDate.text = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
