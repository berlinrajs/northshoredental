//
//  ToothWhiteningStep1VC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 13/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class SedationConsentStep1VC: PDViewController {

    @IBOutlet weak var patientSignatureView: SignatureView!

    @IBOutlet weak var labelDate1: DateLabel!
  
    
    @IBOutlet weak var witnessSignature: SignatureView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValues()
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.sedationsignature1 = patientSignatureView.isSigned() ? patientSignatureView.signatureImage() : nil
        patient.sedationsignature2 = witnessSignature.isSigned() ? witnessSignature.signatureImage() : nil
    }
    
    func loadValues() {
        patientSignatureView.previousImage = patient.sedationsignature1
        witnessSignature.previousImage = patient.sedationsignature2
        
        labelDate1.todayDate = patient.dateToday
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if !patientSignatureView.isSigned()  || !witnessSignature.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }  else if !labelDate1.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kSedationConsentFormVC") as! SedationConsentFormVC
            formVC.patient = patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
