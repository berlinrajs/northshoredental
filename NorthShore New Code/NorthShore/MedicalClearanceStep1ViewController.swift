//
//  MedicalClearanceStep1ViewController.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/10/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalClearanceStep1ViewController: PDViewController {
    
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var tableViewTreatment: UITableView!
    
    var selectedOptions : [String]! = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let patientName = patient.fullName

        labelText.text = labelText.text!.replacingOccurrences(of: "kPatientName", with: patientName).replacingOccurrences(of: "kDateofBirth", with: patient.dateOfBirth)
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.selectedOptions = self.selectedOptions
    }
    func loadValues() {
        if patient.selectedOptions != nil {
            self.selectedOptions = patient.selectedOptions
            self.tableViewTreatment.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        if self.selectedOptions.count == 0 {
            let alert = Extention.alert("PLEASE SELECT ALL THAT APPLY")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            let medicalClearanceStep2VC = self.storyboard?.instantiateViewController(withIdentifier: "kMedicalClearanceStep2VC") as! MedicalClearanceStep2ViewController
            medicalClearanceStep2VC.patient = self.patient
            self.navigationController?.pushViewController(medicalClearanceStep2VC, animated: true)
        }
        
    }
}

extension MedicalClearanceStep1ViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let titles = ["Prophylaxis", "Scaling and Root Planning", "Root Canal", "Extraction", "Crowns / Bridge", "Other"]
        let title = titles[indexPath.row]
        if selectedOptions.contains(title) {
            selectedOptions.remove(at: selectedOptions.index(of: title)!)
        } else {
            selectedOptions.append(title)
        }
        tableView.reloadData()
    }
}

extension MedicalClearanceStep1ViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalClearance", for: indexPath) as! MedicalHistoryStep2TableViewCell
        let titles = ["Prophylaxis", "Scaling and Root Planning", "Root Canal", "Extraction", "Crowns / Bridge", "Other"]
        cell.labelTitle.text = titles[indexPath.row]
        cell.buttonCheckbox.isSelected = selectedOptions.contains(titles[indexPath.row])
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear        
        return cell
        
    }
}
