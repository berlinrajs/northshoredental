//
//  DentalHealthHistoryStep2VC.swift
//  NorthShore
//
//  Created by SRS Websolutions.com on 7/4/18.
//  Copyright © 2018 SRS. All rights reserved.
//

import UIKit

class DentalHealthHistoryStep2VC: PDViewController {

    @IBOutlet var buttonCheck: [UIButton]!
    
    @IBOutlet weak var textFieldFloss: PDTextField!
    @IBOutlet weak var textFieldBrush: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
    }
    
    func loadValues() {
        textFieldFloss.text = patient.dhhFloss
        textFieldBrush.text = patient.dhhTeeth
        
        if patient.dhhProblemCheckBoxTag == nil {
            patient.dhhProblemCheckBoxTag = [Int]()
        }
        for button in buttonCheck {
            button.isSelected = patient.dhhProblemCheckBoxTag.contains(button.tag)
        }
    }
    
    func saveValues() {
        patient.dhhFloss = textFieldFloss.text
        patient.dhhTeeth = textFieldBrush.text
    }
    
    @IBAction func buttonCheckBoxAction (_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            if !self.patient.dhhProblemCheckBoxTag.contains(sender.tag) {
                self.patient.dhhProblemCheckBoxTag.append(sender.tag)
            }
        } else {
            if self.patient.dhhProblemCheckBoxTag.contains(sender.tag) {
                self.patient.dhhProblemCheckBoxTag.remove(at: self.patient.dhhProblemCheckBoxTag.index(of: sender.tag)!)
            }
        }
        
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    
    @IBAction func buttonNextAction(_ sender: Any) {
        self.view.endEditing(true)
        
        saveValues()
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "DentalHealthHistoryStep3VC") as! DentalHealthHistoryStep3VC
        nextVC.patient = patient
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
}
