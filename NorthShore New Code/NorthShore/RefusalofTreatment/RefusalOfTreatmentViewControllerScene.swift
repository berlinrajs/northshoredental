//
//  RefusalOfTreatmentViewControllerScene.swift
//  AL-ShifaDentistry
//
//  Created by SRS Web Solutions on 21/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class RefusalOfTreatmentViewControllerScene: PDViewController {
    
    @IBOutlet var labelText: UILabel!
    @IBOutlet var imageViewSignature: SignatureView!
    @IBOutlet var labelDate: DateLabel!
    
    @IBOutlet var buttonsRefusalOPtions: [UIButton]!
    
    var refusalOptions: [PDOption] = [PDOption]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        labelText.text = labelText.text!.replacingOccurrences(of: "kPatientName", with: "\(patient.fullName)")
        labelDate.todayDate = patient.dateToday
        
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.Refusaltreatsignature1 = imageViewSignature.isSigned() ? imageViewSignature.signatureImage() : nil
        patient.refusalOptions = self.refusalOptions
    }
    func loadValues() {
        imageViewSignature.previousImage = patient.Refusaltreatsignature1
        if patient.refusalOptions == nil {
            let options = ["Bone Loss", "Subgingival Calculus", "Bleeding upon probing"]
            for option in options {
                let obj = PDOption(value: option)
                obj.isSelected = false
                refusalOptions.append(obj)
            }
        } else {
            self.refusalOptions = patient.refusalOptions
            for button in buttonsRefusalOPtions {
                button.isSelected = self.refusalOptions[button.tag].isSelected == nil ? false : self.refusalOptions[button.tag].isSelected!
            }
        }
    }
   
    @IBAction func buttonActionRefusal(_ sender: UIButton) {
        let obj = refusalOptions[sender.tag]
        sender.isSelected = !sender.isSelected
        obj.isSelected = sender.isSelected
    }
    
    func checkSelected() -> PDOption? {
        for option in refusalOptions {
            if option.isSelected == true {
                return option
            }
        }
        return nil
    }
    
   
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        
        if checkSelected() == nil {
            let alert = Extention.alert("PLEASE SELECT ALL THAT APPLY")
            self.present(alert, animated: true, completion: nil)
        } else if !imageViewSignature.isSigned()  {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            
            saveValues()
            
            let nextViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "RefusalofTreatmentFormViewController") as! RefusalofTreatmentFormViewController
            nextViewControllerObj.patient = self.patient
            self.navigationController?.pushViewController(nextViewControllerObj, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
