//
//  ToothExtractionViewController.swift
//  North Shore
//
//  Created by Leojin Bose on 2/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ToothExtractionViewController: PDViewController {

    var toothExtractionStep1 : [PDOption]! = [PDOption]()
    
    @IBOutlet weak var textFieldOthers: PDTextField!
    @IBOutlet weak var collectionViewoptions: UICollectionView!
    @IBOutlet weak var buttonOther: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchData()
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        if !textFieldOthers.isEmpty {
            patient.othersText1 = textFieldOthers.text
        } else {
            patient.othersText1 = nil
        }
        let selectedOptions = self.toothExtractionStep1.filter { (option) -> Bool in
            return option.isSelected != nil && option.isSelected == true
        }
        
        let step1Text : String! = ((selectedOptions as NSArray).value(forKey: "question") as! [String]).joined(separator: ", ")
        patient.toothExtractionQuestions1 = step1Text.isEmpty ? nil : step1Text
    }
    func loadValues() {
        textFieldOthers.text = patient.othersText1
        if patient.othersText1 != nil && patient.othersText1?.characters.count > 0 {
            textFieldOthers.isEnabled = true
            buttonOther.isSelected = true
        }
        let selectedOptions = patient.toothExtractionQuestions1?.components(separatedBy: ", ")
        if selectedOptions != nil {
            for option in toothExtractionStep1 {
                option.isSelected = selectedOptions!.contains(option.question)
                self.collectionViewoptions.reloadData()
            }
        }
    }
    
    func fetchData() {
        PDOption.fetchQuestionsToothExtractionForm1 { (result, success) -> Void in
            if success {
                self.toothExtractionStep1.append(contentsOf: result!)
                self.collectionViewoptions.reloadData()
            }
        }
    }    
    
    @IBAction func buttonNextPressed(_ sender: AnyObject) {
        self.view.endEditing(true)
        let selectedOptions = self.toothExtractionStep1.filter { (option) -> Bool in
            return option.isSelected != nil && option.isSelected == true
        }
        
        if selectedOptions.count == 0 && !textFieldOthers.isEnabled {
            let alert = Extention.alert("PLEASE SELECT ANY OPTION")
            self.present(alert, animated: true, completion: nil)
        } else if textFieldOthers.isEnabled && textFieldOthers.isEmpty {
            let alert = Extention.alert("PLEASE ENTER OTHER REASON")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let toothExtractionStep2VC = self.storyboard?.instantiateViewController(withIdentifier: "kToothExtractionStep2VC") as! ToothExtractionStep2ViewController
              toothExtractionStep2VC.patient = patient
            self.navigationController?.pushViewController(toothExtractionStep2VC, animated: true)
        }
    }
    
    @IBAction func buttonActionOthers(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        textFieldOthers.isEnabled = sender.isSelected
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ToothExtractionViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = self.toothExtractionStep1[indexPath.item]
        if let selected = obj.isSelected {
            obj.isSelected = !selected
        } else {
            obj.isSelected = true
        }
        collectionView.reloadData()
    }
}

extension ToothExtractionViewController : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.toothExtractionStep1.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellMedicalHistory", for: indexPath) as! MedicalHistoryCollectionViewCell
        let obj = self.toothExtractionStep1[indexPath.item]
        cell.configureCellOption(obj)
        return cell
    }
}
