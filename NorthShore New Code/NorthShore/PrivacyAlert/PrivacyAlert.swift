//
//  PrivacyAlert.swift
//  PeopleCenter
//
//  Created by Bala Murugan on 6/1/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PrivacyAlert: UIView {

//    @IBOutlet weak var textviewEffort : UITextView!
    @IBOutlet weak var textviewAcknowledgement : UITextView!
    @IBOutlet weak var radioAcknowledgement : RadioButton!

    var completion:((Int,String,String,Bool)->Void)?
    var errorAlert : ((String) -> Void)?

    static var sharedInstance : PrivacyAlert {
        let instance :  PrivacyAlert = Bundle.main.loadNibNamed("PrivacyAlert", owner: nil, options: nil)!.first as! PrivacyAlert
        return instance
        
    }

    func textView(_ textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    func showPopUp(_ inview : UIView ,completion : @escaping (_ acknowledgementTag : Int , _ acknowledgementReason : String , _ efforts : String , _ isOkSelected : Bool) -> Void , showAlert : @escaping (_ alertMessage : String)-> Void)  {
        self.completion = completion
        self.errorAlert = showAlert
//        textviewEffort.text = ""
        textviewAcknowledgement.text = ""
        radioAcknowledgement.deselectAllButtons()
        
        //let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        inview.addSubview(self)
        inview.bringSubview(toFront: self)
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }

    @IBAction func radioButtonTagged (_ sender : RadioButton){
        if sender.tag == 4{
            textviewAcknowledgement.isUserInteractionEnabled = true
            textviewAcknowledgement.alpha = 1.0
        }else{
            textviewAcknowledgement.isUserInteractionEnabled = false
            textviewAcknowledgement.alpha = 0.5
        }
    }
    
    @IBAction func okButtonPressed (_ sender : UIButton){
//        if  textviewEffort.isEmpty && radioAcknowledgement.selectedButton == nil{
//            self.errorAlert!("Please enter the efforts made to obtain written Acknowledgement or select any reason")
//        } else
        if radioAcknowledgement.selected == nil {
            self.errorAlert!("PLEASE SELECT ANY REASON")
        } else if (radioAcknowledgement.selected.tag == 4 && textviewAcknowledgement.isEmpty) {
            self.errorAlert!("Please specify the reason")
        } else{
            let index = radioAcknowledgement.selected == nil ? 0 : radioAcknowledgement.selected.tag
            self.completion!(index, index == 4 ? textviewAcknowledgement.text! : "" , "", true)
            self.removeFromSuperview()
        }
    }
    
    @IBAction func skipButtonPressed (_ sender : UIButton){
        self.completion!(0, "" , "" , false)
        self.removeFromSuperview()
    }
}
