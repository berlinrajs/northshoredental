//
//  PDFManager.swift
//  FutureDentistry
//
//  Created by Office on 2/21/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

let kKeychainItemLoginName = "North Shore: Google Login"
let kKeychainItemName = "North Shore: Google Drive"
let kClientId = "791256823112-ujdhs5ftdkgemqk28j99hdnn4f3vloms.apps.googleusercontent.com"
let kClientSecret = "0RpySGLwdaC9y1LNODCfrxGG"
private let kFolderName = "NorthShore"

var sharedPDFManager: PDFManager!
class PDFManager: NSObject, GIDSignInUIDelegate, GIDSignInDelegate { 
    
    var service : GTLServiceDrive!
    var credentials : GTMOAuth2Authentication!
    var authViewController: UIViewController!
    var completion: ((_ success : Bool) -> Void)!
    
    class func sharedInstance() -> PDFManager {
    if sharedPDFManager == nil {
    sharedPDFManager = PDFManager()
    }
    return sharedPDFManager
    }
    
    fileprivate func driveService() -> GTLServiceDrive {
        if (service == nil)
        {
            service = GTLServiceDrive()
            
            // Have the service object set tickets to fetch consecutive pages
            // of the feed so we do not need to manually fetch them.
            service.shouldFetchNextPages = true
            
            // Have the service object set tickets to retry temporary error conditions
            // automatically.
            service.isRetryEnabled = true
        }
        return service
    }
    
    
    func createPDFForView(_ view : UIView, patient : PDPatient, completionBlock:(_ finished : Bool) -> Void) {
        var image: UIImage?
        UIGraphicsBeginImageContext(view.bounds.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if let _ = image {
            self.savePDF(UIImageJPEGRepresentation(image!, 1.0)!, patient: patient, completionBlock: completionBlock)
        }
    }
    
    func createPDFForScrollView(_ scrollView : UIScrollView, patient : PDPatient, completionBlock:(_ finished : Bool) -> Void) {
        var image: UIImage?
        scrollView.isScrollEnabled = false
        scrollView.clipsToBounds = false
        let size: CGSize = CGSize(width: scrollView.contentSize.width, height: scrollView.contentSize.height)
        
        let savedContentOffset = scrollView.contentOffset
        
        UIGraphicsBeginImageContext(size)
        scrollView.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        image = UIGraphicsGetImageFromCurrentImageContext()
        scrollView.contentOffset = savedContentOffset
        
        UIGraphicsEndImageContext()
        if let _ = image {
            self.savePDF(UIImageJPEGRepresentation(image!, 1.0)!, patient: patient, completionBlock: completionBlock)
        }
        scrollView.isScrollEnabled = true
        scrollView.clipsToBounds = true
    }
    
    func savePDF(_ pdfData : Data, patient : PDPatient, completionBlock:(_ finished : Bool) -> Void) {
        do {
            // save as a local file
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            do {
                let fileManager = FileManager.default
                let files = try fileManager.contentsOfDirectory(atPath: documentsPath)
                for file in files {
                    let path = "\(documentsPath)/\(file)"
                    try fileManager.removeItem(atPath: path)
                }
            } catch  {
                
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM'_'dd'_'yyyy"
            let dateString = dateFormatter.string(from: NSDate() as Date).uppercased()
            let name = "\(patient.firstName)_\(patient.lastName)_\(dateString)_\(patient.selectedForms.first!.formTitle.fileName).jpg"
            let path = "\(documentsPath)/\(name)"
            try pdfData.write(to: URL(fileURLWithPath: path), options: .atomic)
            self.uploadFileToDrive(path, fileName: name, clientName: "Northshore Dental", patientName: patient.fullName, formName: patient.selectedForms.first!.formTitle.fileName)
            completionBlock(true)
        } catch _ as NSError {
            completionBlock(false)
        }
    }
    
    func createImageForView(_ view : UIView, patient : PDPatient, completionBlock:(_ finished : Bool) -> Void) {
        var image: UIImage?
        UIGraphicsBeginImageContext(view.bounds.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if let _ = image {
            self.saveImage(UIImageJPEGRepresentation(image!, 1.0)!, patient: patient, completionBlock: completionBlock)
        }
    }
    
    func createImageForScrollView(_ scrollView : UIScrollView, patient : PDPatient, completionBlock:(_ finished : Bool) -> Void) {
        var image: UIImage?
        scrollView.isScrollEnabled = false
        scrollView.clipsToBounds = false
        let size: CGSize = CGSize(width: scrollView.contentSize.width, height: scrollView.contentSize.height)
        
        let savedContentOffset = scrollView.contentOffset
        
        UIGraphicsBeginImageContext(size)
        scrollView.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        image = UIGraphicsGetImageFromCurrentImageContext()
        scrollView.contentOffset = savedContentOffset
        
        UIGraphicsEndImageContext()
        if let _ = image {
            self.saveImage(UIImageJPEGRepresentation(image!, 1.0)!, patient: patient, completionBlock: completionBlock)
        }
        scrollView.isScrollEnabled = true
        scrollView.clipsToBounds = true
    }
    
    
    func saveImage(_ imageData : Data, patient : PDPatient, completionBlock:(_ finished : Bool) -> Void) {
        do {
            // save as a local file
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM'_'dd'_'yyyy"
            let dateString = dateFormatter.string(from: NSDate() as Date).uppercased()
            let name = "\(patient.fullName.fileName)_\(dateString)_\(patient.selectedForms.first!.formTitle.fileName).jpg"
            let path = "\(documentsPath)/\(name)"
            try imageData.write(to: URL(fileURLWithPath: path), options: .atomic)
            self.uploadFileToDrive(path, fileName: name, clientName: "Northshore Dental", patientName: patient.fullName, formName: patient.selectedForms.first!.formTitle.fileName)
            completionBlock(true)
        } catch _ as NSError {
            completionBlock(false)
        }
    }
    
    ///MARK:- CHECK DRIVE FREE SPACE
    func CheckGoogleDriveFreeSpace()  {
        let query : GTLQueryDrive = GTLQueryDrive.queryForAboutGet()
        query.fields = "storageQuota,user"
        _  = driveService().executeQuery(query) { (ticket, about, error) in
            if error == nil{
                let abt : GTLDriveAbout = about as! GTLDriveAbout
                let limitInGB : Int = Int(abt.storageQuota.limit.doubleValue/1024.0/1024.0/1024.0)
                let usageinGB : Int = Int(abt.storageQuota.usage.doubleValue/1024.0/1024.0/1024.0)
                let userEmail : String = abt.user.emailAddress
                
                //ALMOST FULL
                if usageinGB > limitInGB - 1{
                    self.showGoogleDriveErrorAlert()
                    
                }else if (usageinGB/limitInGB) * 100 > 80{
                    /// 80% OF DRIVE IS FULL
                    self.sendWarningEmail(userEmail)
                    
                }
            }else{
                print("Error \(error?.localizedDescription)")
            }
            
        }
    }
    
    //MARK:- GOOGLE DRIVE ERROR ALERT
    func showGoogleDriveErrorAlert () {
        let alertController = UIAlertController(title: "WARNING", message: "YOUR GOOGLE DRIVE IS ALMOST FULL", preferredStyle: UIAlertControllerStyle.alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.destructive) { (action) -> Void in
        }
        alertController.addAction(alertOkAction)
        ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController as! UINavigationController).viewControllers.last!.present(alertController, animated: true, completion: nil)
        
    }
    
    //MARK:- SEND WARNING EMAIL
    func sendWarningEmail(_ toEmail : String){
        let message : SMTPMessage = SMTPMessage()
        message.from = "demo@srswebsolutions.com"
        message.to = toEmail
        message.host = "smtp.gmail.com"
        message.account = "demo@srswebsolutions.com"
        message.pwd = "Srsweb123#"
        
        message.subject = "Your Google drive almost Full"
        message.content = "<p>Hi,</p><p>Your google drive account is 80% full. So please clear some contents immediately</p><p>Thank you.</p>"
        message.send({ (messg, now, total) in
            
            }, success: { (messg) in
                print("Email sent")
            }, failure: { (messg, error) in
        })
        
    }
    
    
    func uploadFileToDrive(_ path : String, fileName : String, clientName: String, patientName: String, formName: String) {
        self.CheckGoogleDriveFreeSpace()
        
        let formData = try! Data(contentsOf: URL(fileURLWithPath: path))
        
        DispatchQueue.main.async {
            //        http://mconsent.net/admin/api/activity_api.php?client_name=pp&formname=MedicalHistoryform&task_data=image&email=srs@gmail.com
            
            let manager = AFHTTPSessionManager(baseURL: URL(string: "http://mconsent.net/admin/api/")!)
            
            manager.responseSerializer.acceptableContentTypes = ["text/html"]
            manager.post("activity_api.php?", parameters: ["client_name": clientName.fileName, "patient_name": patientName.fileName, "formname": formName.fileName, "appkey": "mcNorthShore"], constructingBodyWith: { (data) in
                data.appendPart(withFileData: formData, name: "task_data", fileName: fileName.fileName, mimeType: fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg")
            }, progress: { (progress) in
                
            }, success: { (task, result) in
                
            }, failure: { (task, booledad) in
                
            })
        }

        func uploadFile(_ identitifer : String) {
            let driveFile = GTLDriveFile()
            driveFile.mimeType = fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg"
            driveFile.originalFilename = fileName
            driveFile.name = fileName
            driveFile.parents = [identitifer]
            
            let uploadParameters = GTLUploadParameters(data: formData, mimeType: fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg")
            let query = GTLQueryDrive.queryForFilesCreate(withObject: driveFile, uploadParameters: uploadParameters)
            query?.addParents = identitifer
            
            self.driveService().executeQuery(query!, completionHandler: { (ticket, uploadedFile, error) -> Void in
                if (error == nil) {
                    let fileManager = FileManager.default
                    if fileManager.fileExists(atPath: path) {
                        do {
                            try fileManager.removeItem(atPath: path)
                        } catch  {
                            
                        }
                    }
                } else {
                }
            })
        }
        
        func checkAndCreateFolder(_ folderName: String, parent: GTLDriveFile?, completion: @escaping ((_ success: Bool, _ folder: GTLDriveFile) -> Void)) {
            let folderDateQuery = GTLQueryDrive.queryForFilesList()
            folderDateQuery?.q = parent == nil ? "mimeType = 'application/vnd.google-apps.folder' and name = '\(folderName)' and trashed = false" : "mimeType = 'application/vnd.google-apps.folder' and name = '\(folderName)' and trashed = false and '\(parent!.identifier!)' in parents"
            
            self.driveService().executeQuery(folderDateQuery!, completionHandler: { (ticket, obj, error) -> Void in
                if error == nil {
                    let childFolder = (obj as! GTLDriveFileList).files
                    if childFolder != nil && (childFolder?.count)! > 0 {
                        let dateFolder = childFolder?[0] as! GTLDriveFile
                        completion(true, dateFolder)
                    } else {
                        createNewFolder(folderName, parent: parent == nil ? nil : [parent!.identifier!], createCompletion: { (success, returnFolder) in
                            if success {
                                completion(true, returnFolder!)
                            }
                        })
                    }
                } else {
                }
            })
        }
        
        func createNewFolder(_ folderName : String, parent : [String]?, createCompletion: @escaping ((_ success: Bool, _ returnFolder: GTLDriveFile?) -> Void)) {
            let folderObj = GTLDriveFile()
            folderObj.name = folderName
            if parent != nil {
                folderObj.parents = parent
            }
            folderObj.mimeType = "application/vnd.google-apps.folder"
            
            let queryFolder = GTLQueryDrive.queryForFilesCreate(withObject: folderObj, uploadParameters: nil)
            
            self.driveService().executeQuery(queryFolder!, completionHandler: { (ticket, result, error) -> Void in
                if (error == nil) {
                    let folder = result as! GTLDriveFile
                    createCompletion(true, folder)
                } else {
                    createCompletion(false, nil)
                }
            })
        }
        
        let name = "\(UIDevice.current.name)_" + kFolderName
        checkAndCreateFolder(name, parent: nil) { (success, folder) in
            if success {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM'-'dd'-'yyyy"
                let folderName = dateFormatter.string(from: NSDate() as Date).uppercased()
                
                checkAndCreateFolder(folderName.fileName, parent: folder, completion: { (success, folder) in
                    uploadFile(folder.identifier!)
                })
            }
        }
    }
    func authorizeDrive(_ viewController : UIViewController, completion:@escaping (_ success : Bool) -> Void) {
        credentials = GTMOAuth2ViewControllerTouch.authForGoogleFromKeychain(forName: kKeychainItemName, clientID: kClientId, clientSecret: kClientSecret)
        if credentials.canAuthorize {
            self.driveService().authorizer = credentials
            completion(true)
        } else {
            self.completion = completion
            self.authViewController = viewController
            var configureError: NSError?
            GGLContext.sharedInstance().configureWithError(&configureError)
            assert(configureError == nil, "Error configuring Google services: \(String(describing: configureError))")
            
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().scopes = ["https://www.googleapis.com/auth/drive"]
            GIDSignIn.sharedInstance().signIn()
        }
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        authViewController.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        authViewController.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if error == nil {
            // ...
            self.driveService().authorizer = user.authentication.fetcherAuthorizer()
            self.completion(true)
        } else {
            self.completion(false)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
}
