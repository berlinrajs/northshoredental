//
//  MedicalHistoryStep1ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryStep3ViewController: PDViewController {
    
    var medicalHistoryStep3 : [PDQuestion]! = [PDQuestion]()
    var fetchCompleted : Bool! = false
    
    var selectedButton : RadioButton!
    @IBOutlet weak var DoUHavePhysician: RadioButton!
    @IBOutlet weak var YourMedicalhealthRadio: RadioButton!
    
    @IBOutlet weak var tableViewQuestions: UITableView!
    @IBOutlet var viewPopup: PDView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textViewAnswer: PDTextView!
    @IBOutlet weak var buttonVerified: UIButton!
    @IBOutlet weak var MedicalPhysicianName: PDTextField!
    
    @IBOutlet weak var physicianView: PDView!
    @IBOutlet weak var PhysicianAddress: PDTextField!
    @IBOutlet weak var PhysicianPhone: PDTextField!
    @IBOutlet weak var LastVist: PDTextField!
    
    @IBOutlet weak var Label1: UILabel!
    @IBOutlet weak var Label2: UILabel!
    @IBOutlet weak var Label3: UILabel!
    @IBOutlet weak var Label4: UILabel!
    
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var toolBar: UIToolbar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        datePicker.maximumDate = nil
//        LastVist.inputView = datePicker
//        LastVist.inputAccessoryView = toolBar
        DateInputView.addDatePickerForTextField(LastVist)
        
//        self.physicianView.userInteractionEnabled = false
//        self.physicianView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
//        
//        self.MedicalPhysicianName.borderColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
//        self.PhysicianAddress.borderColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
//        self.PhysicianPhone.borderColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
//        self.LastVist.borderColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
//        
//        self.Label1.textColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
//        self.Label2.textColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
//        self.Label3.textColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
//        self.Label4.textColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
        
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.medicalHistoryQuestions3 = self.medicalHistoryStep3
        patient.yourMedicalHealthTag = YourMedicalhealthRadio.selected.tag
        patient.PersonalPhysicianTag = DoUHavePhysician.selected.tag
        patient.MedicalPhysicianName = MedicalPhysicianName.text
        patient.MedicalPhysicianAddress = PhysicianAddress.text
        patient.MedicalPhysicanVisit = LastVist.text
        patient.MedicalPhysicianPhone = PhysicianPhone.text
    }
    func loadValues() {
        if patient.medicalHistoryQuestions3 == nil {
            fetchData()
        } else {
            self.medicalHistoryStep3 = patient.medicalHistoryQuestions3
            self.fetchCompleted = true
            self.tableViewQuestions.reloadData()
        }
        YourMedicalhealthRadio.setSelectedWithTag(patient.yourMedicalHealthTag == nil ? 0 : patient.yourMedicalHealthTag)
        DoUHavePhysician.setSelectedWithTag(patient.PersonalPhysicianTag == nil ? 0 : patient.PersonalPhysicianTag)
        
        MedicalPhysicianName.text = patient.MedicalPhysicianName
        PhysicianAddress.text = patient.MedicalPhysicianAddress
        LastVist.text = patient.MedicalPhysicanVisit
        PhysicianPhone.text = patient.MedicalPhysicianPhone
        
        if DoUHavePhysician.selected != nil && DoUHavePhysician.selected.tag == 1 {
            self.physicianView.isUserInteractionEnabled = true
            self.physicianView.backgroundColor = UIColor.clear
            self.MedicalPhysicianName.borderColor = UIColor.white.withAlphaComponent(1)
            self.PhysicianAddress.borderColor = UIColor.white.withAlphaComponent(1)
            self.PhysicianPhone.borderColor = UIColor.white.withAlphaComponent(1)
            
            self.LastVist.borderColor = UIColor.white.withAlphaComponent(1)
            self.Label1.textColor = UIColor.white.withAlphaComponent(1)
            self.Label2.textColor = UIColor.white.withAlphaComponent(1)
            self.Label3.textColor = UIColor.white.withAlphaComponent(1)
            self.Label4.textColor = UIColor.white.withAlphaComponent(1)
        } else {
            self.physicianView.isUserInteractionEnabled = false
            self.physicianView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            
            MedicalPhysicianName.text = ""
            PhysicianAddress.text = ""
            PhysicianPhone.text = ""
            LastVist.text = ""
            
            patient.MedicalPhysicianName = ""
            patient.MedicalPhysicianAddress = ""
            patient.MedicalPhysicanVisit = ""
            patient.MedicalPhysicianPhone = ""
            
            self.MedicalPhysicianName.borderColor = UIColor.white.withAlphaComponent(0.2)
            self.PhysicianAddress.borderColor = UIColor.white.withAlphaComponent(0.2)
            self.PhysicianPhone.borderColor = UIColor.white.withAlphaComponent(0.2)
            self.LastVist.borderColor = UIColor.white.withAlphaComponent(0.2)
            
            self.Label1.textColor = UIColor.white.withAlphaComponent(0.2)
            self.Label2.textColor = UIColor.white.withAlphaComponent(0.2)
            self.Label3.textColor = UIColor.white.withAlphaComponent(0.2)
            self.Label4.textColor = UIColor.white.withAlphaComponent(0.2)
        }
    }
    
    func fetchData() {
        PDQuestion.fetchQuestionsMedicalForm3 { (result, success) -> Void in
            self.fetchCompleted = true
            if success {
                self.medicalHistoryStep3.append(contentsOf: result!)
                self.tableViewQuestions.reloadData()
            }
        }
    }
    
    
    @IBAction func toolbarDoneButtonAction(_ sender: AnyObject) {
        LastVist.resignFirstResponder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        LastVist.text = dateFormatter.string(from: datePicker.date).uppercased()
        
    }
    
    @IBAction func datePickerDateChanged(_ sender: AnyObject) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        LastVist.text = dateFormatter.string(from: datePicker.date).uppercased()
    }
    
    @IBAction func physicianViewAction(_ sender: AnyObject) {
        if sender.tag == 1 {
            self.physicianView.isUserInteractionEnabled = true
            self.physicianView.backgroundColor = UIColor.clear
            self.MedicalPhysicianName.borderColor = UIColor.white.withAlphaComponent(1)
            self.PhysicianAddress.borderColor = UIColor.white.withAlphaComponent(1)
            self.PhysicianPhone.borderColor = UIColor.white.withAlphaComponent(1)
            
            self.LastVist.borderColor = UIColor.white.withAlphaComponent(1)
            self.Label1.textColor = UIColor.white.withAlphaComponent(1)
            self.Label2.textColor = UIColor.white.withAlphaComponent(1)
            self.Label3.textColor = UIColor.white.withAlphaComponent(1)
            self.Label4.textColor = UIColor.white.withAlphaComponent(1)
        } else {
            self.physicianView.isUserInteractionEnabled = false
            self.physicianView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            
            MedicalPhysicianName.text = ""
            PhysicianAddress.text = ""
            PhysicianPhone.text = ""
            LastVist.text = ""
            
            patient.MedicalPhysicianName = ""
            patient.MedicalPhysicianAddress = ""
            patient.MedicalPhysicanVisit = ""
            patient.MedicalPhysicianPhone = ""
            
            self.MedicalPhysicianName.borderColor = UIColor.white.withAlphaComponent(0.2)
            self.PhysicianAddress.borderColor = UIColor.white.withAlphaComponent(0.2)
            self.PhysicianPhone.borderColor = UIColor.white.withAlphaComponent(0.2)
            self.LastVist.borderColor = UIColor.white.withAlphaComponent(0.2)
            
            self.Label1.textColor = UIColor.white.withAlphaComponent(0.2)
            self.Label2.textColor = UIColor.white.withAlphaComponent(0.2)
            self.Label3.textColor = UIColor.white.withAlphaComponent(0.2)
            self.Label4.textColor = UIColor.white.withAlphaComponent(0.2)
        }
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if self.fetchCompleted == true {
            
            if !buttonVerified.isSelected {
                let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
                self.present(alert, animated: true, completion: nil)
            } else {
                saveValues()
                
                let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
                let MedicalHistory4 = storyboard.instantiateViewController(withIdentifier: "kMedicalHistoryStep4VC") as! MedicalHistoryStep4ViewController
                MedicalHistory4.patient = patient
                self.navigationController?.pushViewController(MedicalHistory4, animated: true)
            }
        }
    }
    
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func buttonActionPopupOk(_ sender: AnyObject) {
        
        let obj = medicalHistoryStep3[selectedButton.tag]
        if !textViewAnswer.isEmpty && textViewAnswer.text != "IF YES TYPE HERE" {
            obj.answer = textViewAnswer.text
            obj.selectedOption = true
            
            patient.ifUnderPhysicanCare = textViewAnswer.text
        } else {
            selectedButton.isSelected = false
            obj.selectedOption = false
        }
        textViewAnswer.resignFirstResponder()
        self.viewPopup.removeFromSuperview()
        self.viewShadow.isHidden = true
    }
    
    
    func findEmptyValue() -> PDQuestion? {
        for question in self.medicalHistoryStep3 {
            if question.selectedOption == nil {
                return question
            }
        }
        return nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension MedicalHistoryStep3ViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "IF YES TYPE HERE" || textView.text == "TYPE HERE*" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "IF YES TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
        
        if textView.text.isEmpty {
            textView.text = "TYPE HERE*"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

extension MedicalHistoryStep3ViewController : MedicalHistoryCellDelegate {
    
    func radioButtonAction(_ sender: RadioButton) {
        selectedButton = sender
        if selectedButton.tag == 0 {
            let frameSize = CGRect(x: 0, y: 0, width: 348, height: 174)
            self.viewPopup.frame = frameSize
            self.viewPopup.center = self.view.center
            self.viewShadow.addSubview(self.viewPopup)
            self.viewPopup.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            textViewAnswer.text = "IF YES TYPE HERE"
            textViewAnswer.textColor = UIColor.lightGray
            self.viewShadow.isHidden = false
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.3)
            self.viewPopup.transform = CGAffineTransform.identity
            UIView.commitAnimations()
        } else {
            let obj = medicalHistoryStep3[selectedButton.tag]
            obj.selectedOption = true
        }
    }
}


extension MedicalHistoryStep3ViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return medicalHistoryStep3.count
    }
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryStep1", for: indexPath) as! MedicalHistoryStep1TableViewCell
        let obj = medicalHistoryStep3[indexPath.row]
        cell.configureCell(obj)
        cell.buttonYes.tag = indexPath.row
        cell.buttonNo.tag = indexPath.row
        cell.delegate = self
        return cell
    }
}

extension MedicalHistoryStep3ViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField ==  PhysicianPhone{
            
            return textField.formatPhoneNumber(range, string: string)
        }
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


