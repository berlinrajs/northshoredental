//
//  ToothExtractionStep3ViewController.swift
//  North Shore
//
//  Created by Office on 2/24/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ToothExtractionStep3ViewController: PDViewController {

    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var signatureView2: SignatureView!
    @IBOutlet weak var signatureView3: SignatureView!

    @IBOutlet weak var labelDate1: PDLabel!
    @IBOutlet weak var labelName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelName.text = "\(patient.fullName)"
        signatureView1.layer.cornerRadius = 3.0
        signatureView2.layer.cornerRadius = 3.0
        signatureView3.layer.cornerRadius = 3.0
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(ToothExtractionStep3ViewController.setDateOnLabel1))
        tapGesture1.numberOfTapsRequired = 1
        labelDate1.addGestureRecognizer(tapGesture1)
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.signatureToothExtraction = signatureView1.isSigned() ? signatureView1.signatureImage() : nil
        patient.signature2 = signatureView2.isSigned() ? signatureView2.signatureImage() : nil
        patient.signature3 = signatureView3.isSigned() ? signatureView3.signatureImage() : nil
    }
    func loadValues() {
        signatureView1.previousImage = patient.signatureToothExtraction
        signatureView2.previousImage = patient.signature2
        signatureView3.previousImage = patient.signature3
    }
    
    func setDateOnLabel1() {
        labelDate1.text = patient.dateToday
        labelDate1.textColor = UIColor.black
    }

    
    @IBAction func buttonNextPressed(_ sender: AnyObject) {
        if !signatureView1.isSigned() || !signatureView2.isSigned() || !signatureView3.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if labelDate1.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()

            let toothExtractionFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kToothExtractionFormVC") as! ToothExtractionFormViewController
            toothExtractionFormVC.patient = patient
            self.navigationController?.pushViewController(toothExtractionFormVC, animated: true)
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
