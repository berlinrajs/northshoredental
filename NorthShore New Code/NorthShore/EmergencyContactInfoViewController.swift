//
//  ContactInfoViewController.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class EmergencyContactInfoViewController: PDViewController {

    @IBOutlet weak var textFieldState: PDTextField!
  
    @IBOutlet weak var textFieldWorkphone: PDTextField!
    @IBOutlet weak var textFieldRelation: PDTextField!
    
    @IBOutlet weak var textFieldEmergencyContactNumber: PDTextField!
    @IBOutlet weak var textFiledEmergencyContact: PDTextField!
    @IBOutlet weak var textFiledzip: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldAddress: PDTextField!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        StateListView.addStateListForTextField(textFieldState)
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.emergContactName = textFiledEmergencyContact.text
        patient.emergState = textFieldState.text
        patient.emergcity = textFieldCity.text
        patient.emergZip = textFiledzip.text
        patient.emergRelationName = textFieldRelation.text
        patient.emergworkphoneNumber = textFieldWorkphone.text
        patient.emergHomephoneNumber = textFieldEmergencyContactNumber.text
        patient.emergAddress = textFieldAddress.text
    }
    func loadValues() {
        textFiledEmergencyContact.text = patient.emergContactName
        textFieldState.text = patient.emergState == nil || patient.emergState.isEmpty ? "IL" : patient.emergState
        textFieldCity.text = patient.emergcity
        textFiledzip.text = patient.emergZip
        textFieldRelation.text = patient.emergRelationName
        textFieldWorkphone.text = patient.emergworkphoneNumber
        textFieldEmergencyContactNumber.text = patient.emergHomephoneNumber
        textFieldAddress.text = patient.emergAddress
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
//        if let _ = findEmptyTextField() {
//            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
//            self.presentViewController(alert, animated: true, completion: nil)
//        } else
        if !textFieldEmergencyContactNumber.isEmpty && !textFieldEmergencyContactNumber.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID HOME PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldWorkphone.isEmpty && !textFieldWorkphone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID WORK PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else if !textFiledzip.isEmpty && !textFiledzip.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID ZIP CODE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            YesOrNoAlert.sharedInstance.showWithTitle("DO YOU HAVE A INSURANCE POLICY", button1Title: "YES", button2Title: "NO", completion: { (buttonIndex) in
                if buttonIndex == 1 {
                    let info7VC = self.storyboard?.instantiateViewController(withIdentifier: "kPrimaryInsuranceVC") as! PrimaryInsuranceVC
                    info7VC.patient = self.patient
                    self.navigationController?.pushViewController(info7VC, animated: true)
                } else {
                    self.patient.resetPrimaryInsurance()
                    self.patient.resetSecondaryInsurance()
                    let diseaseInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kResponsiblePersonVC") as! ResponsiblePersonVC
                    diseaseInfoVC.patient = self.patient
                    self.navigationController?.pushViewController(diseaseInfoVC, animated: true)
                }
            })
        }
    }
    
    func findEmptyTextField() -> UITextField? {
        let textFields = [textFiledEmergencyContact, textFieldRelation, textFieldEmergencyContactNumber]
        for textField in textFields {
            if (textField?.isEmpty)! {
                return textField
            }
        }
        return nil
    }
}

extension EmergencyContactInfoViewController : UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldWorkphone || textField == textFieldEmergencyContactNumber {
            return textField.formatPhoneNumber(range, string: string)
        } else if textField == textFiledzip {
            return textField.formatZipCode(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


