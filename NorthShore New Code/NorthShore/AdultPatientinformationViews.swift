//
//  AdultPatientinformation.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AdultPatientInformation: UIView, UITableViewDataSource, UITableViewDelegate {
    
    var arrayOfQuestions: [PDQuestion]! {
        didSet {
            (self.viewWithTag(100) as! UITableView).reloadData()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayOfQuestions == nil ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellInfo", for: indexPath) as! AdultPatientInfoCell
        
        cell.configWithQuestion(arrayOfQuestions[indexPath.row], atQuestionIndex: indexPath.row + 1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 14
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let question = arrayOfQuestions[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CellInfo") as? AdultPatientInfoCell {
            return question.question.heightWithConstrainedWidth(240, font: cell.labelTitle.font) + 6
        } else {
            return indexPath.row == 3 ? 27.0 : indexPath.row == 2 ? 27: indexPath.row == 5 ? 20.0 : indexPath.row == 6 ? 27.0 : indexPath.row == 7 ? 20.0 : indexPath.row == 8 ? 20.0 : indexPath.row == 9 ? 20.0 : indexPath.row == 10 ? 20 : 27
        }
    }
}
class AdultPatientInfoCell: UITableViewCell {
    @IBOutlet weak var labelCount: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var radioYes: RadioButton!
    
    @IBOutlet weak var labelSubtitle: UILabel!
    @IBOutlet weak var labelAnswer1: FormLabel!
    @IBOutlet weak var labelAnswer2: FormLabel!
    @IBOutlet weak var labelAnswer3: FormLabel!
    
    var question: PDQuestion!
    var option : PDOption!
    func configWithQuestion(_ question: PDQuestion, atQuestionIndex: Int) {
        
        self.question = question
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        
        labelCount.text = "\(atQuestionIndex)."
        labelTitle.text = question.question + " ........................................................................................."
        radioYes.setSelected(question.selectedOption)
        
      //  labelSubtitle.text = atQuestionIndex == 2 ? "If yes, please explain" : atQuestionIndex == 3 ? "If yes, what medication(s) are you taking?" : ""
//        
//        var rect = labelSubtitle.frame
//        labelSubtitle.sizeToFit()
//        rect.size.width = CGRectGetWidth(labelSubtitle.frame) + 3
//        labelSubtitle.frame = rect
    }
    
    
//    override func layoutSubviews() {
//        var rect = labelSubtitle.frame
//        labelSubtitle.sizeToFit()
//        rect.size.width = CGRectGetWidth(labelSubtitle.frame) + 3
//        labelSubtitle.frame = rect
//        
//        super.layoutSubviews()
//        if question != nil {
//            if question.isAnswerRequired == true {
////                labelAnswer1.text = question.answer
//                question.answer?.setTextForArrayOfLabels([labelAnswer1, labelAnswer2, labelAnswer3])
//            }
//        }
//    }
}


