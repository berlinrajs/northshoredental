//
//  ContactInfoViewController.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class SpouseInformationVC: PDViewController {

  //  @IBOutlet weak var textFieldWorkphone: PDTextField!
    @IBOutlet weak var textFieldSocialSecurity: PDTextField!
   // @IBOutlet weak var textFieldEmployer: PDTextField!
    @IBOutlet weak var textFiledEmergencyContact: PDTextField!
    
    @IBOutlet weak var textFieldDriverLicense: PDTextField!
    @IBOutlet weak var textFieldDateofBirth: PDTextField!
    @IBOutlet var toolBar: UIToolbar!
    
    @IBOutlet var datePicker: UIDatePicker!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        datePicker.maximumDate = nil
//        textFieldDateofBirth.inputView = datePicker
//        textFieldDateofBirth.inputAccessoryView = toolBar
        DateInputView.addDatePickerForDateOfBirthTextField(textFieldDateofBirth)

        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
      //  patient.SpouseemployerName = textFieldEmployer.text
        patient.SpouseContactName = textFiledEmergencyContact.text
        patient.SpouseDateofbirth = textFieldDateofBirth.text
        //patient.SpouseworkphoneNumber = textFieldWorkphone.text
        patient.SpouseSocialSecurity = textFieldSocialSecurity.text
        patient.SpouseDriverLicense = textFieldDriverLicense.text
    }
    func loadValues() {
        //textFieldEmployer.text = patient.SpouseemployerName
        textFiledEmergencyContact.text = patient.SpouseContactName
        textFieldDateofBirth.text = patient.SpouseDateofbirth
       // textFieldWorkphone.text = patient.SpouseworkphoneNumber
        textFieldSocialSecurity.text = patient.SpouseSocialSecurity
        textFieldDriverLicense.text = patient.SpouseDriverLicense
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func findEmptyTextField() -> UITextField? {
        let textFields = [textFiledEmergencyContact, textFieldDateofBirth]
        for textField in textFields {
            if (textField?.isEmpty)! {
                return textField
            }
        }
        return nil
    }
    

    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if let _ = findEmptyTextField() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldSocialSecurity.isEmpty && !textFieldSocialSecurity.text!.isSocialSecurityNumber {
            let alert = Extention.alert("PLEASE ENTER VALID SOCIAL SECURITY NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            let diseaseInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kAgreementVC") as! AgreementViewController
            diseaseInfoVC.patient = patient
            self.navigationController?.pushViewController(diseaseInfoVC, animated: true)
        }
    }
    
    
    @IBAction func toolbarDoneButtonAction(_ sender: AnyObject) {
        
        textFieldDateofBirth.resignFirstResponder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldDateofBirth.text = dateFormatter.string(from: datePicker.date).uppercased()
        
    }
    
    @IBAction func datePickerDateChanged(_ sender: AnyObject) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldDateofBirth.text = dateFormatter.string(from: datePicker.date).uppercased()
    }
}

extension SpouseInformationVC : UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         if textField == textFieldSocialSecurity  {
            return textField.formatNumbers(range, string: string, count: 9)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


