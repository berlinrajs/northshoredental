//
//  DentalHealthHistoryStep1VC.swift
//  NorthShore
//
//  Created by SRS Websolutions.com on 7/4/18.
//  Copyright © 2018 SRS. All rights reserved.
//

import UIKit

class DentalHealthHistoryStep1VC: PDViewController {

    @IBOutlet weak var textFieldAddress: PDTextField!
    @IBOutlet weak var textFieldCity: PDTextField!
    @IBOutlet weak var textFieldState: PDTextField!
    @IBOutlet weak var textFieldZip: PDTextField!
    @IBOutlet weak var textFieldEmail: PDTextField!
    @IBOutlet weak var textFieldPhone: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        StateListView.addStateListForTextField(textFieldState)
        
    }
    
    func loadValues() {
        textFieldAddress.text = patient.addressLine
        textFieldCity.text = patient.city
        textFieldState.text = patient.state
        textFieldZip.text = patient.zipCode
        textFieldEmail.text = patient.email
        textFieldPhone.text = patient.phoneNumber
    }
    
    func saveValues() {
        patient.addressLine = textFieldAddress.text
        patient.city = textFieldCity.text
        patient.state = textFieldState.text
        patient.zipCode = textFieldZip.text
        patient.email = textFieldEmail.text
        patient.phoneNumber = textFieldPhone.text
    }
    
    @IBAction func buttonNextAction(_ sender: Any) {
        self.view.endEditing(true)
        if textFieldAddress.isEmpty {
            self.showAlert("PLEASE ENTER THE VALID ADDRESS")
        } else if textFieldCity.isEmpty {
            self.showAlert("PLEASE ENTER THE VALID CITY")
        } else if textFieldState.isEmpty {
            self.showAlert("PLEASE ENTER THE VALID STATE")
        } else if textFieldZip.isEmpty {
            self.showAlert("PLEASE ENTER THE VALID ZIPCODE")
        } else if !textFieldEmail.isEmpty && !textFieldEmail.text!.isValidEmail {
            self.showAlert("PLEASE ENTER THE VALID EMAIL")
        } else {
            saveValues()
            
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "DentalHealthHistoryStep2VC") as! DentalHealthHistoryStep2VC
            nextVC.patient = patient
            self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }
    
    
}

extension DentalHealthHistoryStep1VC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textFieldZip {
            return textField.formatZipCode(range, string: string)
        } else if textField == textFieldPhone  {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
