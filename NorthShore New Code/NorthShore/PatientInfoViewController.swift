//
//  PatientInfoViewController.swift
//  North Shore
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientInfoViewController: PDViewController {

    @IBOutlet weak var labelDate: UILabel!
    
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
    
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var buttonNext: PDButton!
    @IBOutlet weak var textFieldMiddleInitial: PDTextField!
    
    @IBOutlet weak var patientGender: RadioButton!
    @IBOutlet weak var Preferedname: PDTextField!
    
    @IBOutlet weak var dropdown : BRDropDown!
    @IBOutlet weak var textfieldMonth : UITextField!
    @IBOutlet weak var textfieldDate : UITextField!
    @IBOutlet weak var textfieldYear : UITextField!
    @IBOutlet weak var pickerMonth : UIPickerView!
    
    var isFromVisitorForm: Bool = false

    var arrayMonths = ["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"]

    override func viewDidLoad() {
        super.viewDidLoad()
        pickerMonth.dataSource = self
        pickerMonth.delegate = self
        textfieldMonth.inputView = pickerMonth
        textfieldMonth.inputAccessoryView = toolBar

        labelDate.text = patient.dateToday
        dropdown.items = ["Mr.","Mrs.","Miss.","Ms.","Dr."]
        buttonBack?.isHidden = isFromVisitorForm
        if isFromVisitorForm == true {
            textFieldFirstName.text = patient.VisitorFirstName
            textFieldLastName.text = patient.VisitorLastName
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
//        if dropdown.selectedIndex == 0 {
//            let alert = Extention.alert("PLEASE SELECT A TITLE")
//            self.presentViewController(alert, animated: true, completion: nil)
//        }
//        else
        if textFieldFirstName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER PATIENT FIRST NAME")
            self.present(alert, animated: true, completion: nil)
        } else if textFieldLastName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER PATIENT LAST NAME")
            self.present(alert, animated: true, completion: nil)
        } else if patientGender.selected == nil {
            let alert = Extention.alert("PLEASE SELECT GENDER")
            self.present(alert, animated: true, completion: nil)
        } else if textfieldMonth.isEmpty || textfieldDate.isEmpty  || textfieldYear.isEmpty {
            let alert = Extention.alert("PLEASE ENTER DATE OF BIRTH")
            self.present(alert, animated: true, completion: nil)
        } else if Int(textfieldDate.text!)! == 0{
            let alert = Extention.alert("PLEASE ENTER THE VALID DATE")
            self.present(alert, animated: true, completion: nil)
        } else if !textfieldYear.text!.isValidYear {
            let alert = Extention.alert("PLEASE ENTER A VALID YEAR")
            self.present(alert, animated: true, completion: nil)
        }
        else {
            
            patient.firstName = textFieldFirstName.text
            patient.lastName = textFieldLastName.text
            patient.dateOfBirth = textfieldMonth.text! + " " + textfieldDate.text! + ", " + textfieldYear.text!
            patient.middleName = textFieldMiddleInitial.isEmpty ? "" : textFieldMiddleInitial.text!
            patient.preferredName = Preferedname.text
            patient.patientGender = patientGender.selected.tag
            patient.PatientTile = dropdown.selectedIndex
            
            gotoNextForm()
        }
    }
    
    @IBAction func toolbarDoneButtonAction(_ sender: AnyObject) {
        textfieldMonth.resignFirstResponder()
        let string1 = arrayMonths[pickerMonth.selectedRow(inComponent: 0)]
        textfieldMonth.text = string1.substring(with: string1.startIndex ..< string1.characters.index(string1.startIndex, offsetBy: 3))
    }
}

extension PatientInfoViewController : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldMiddleInitial {
            return textField.formatInitial(range, string: string)
        }else if textField == textfieldDate{
            return textField.formatDate(range, string: string)
        }else if textField == textfieldYear{
            return textField.formatNumbers(range, string: string, count: 4)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension PatientInfoViewController : UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayMonths.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayMonths[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let string1 = arrayMonths[row]
        
        //let index1 = string1.startIndex.advancedBy(3)
        
        textfieldMonth.text = string1.substring(with: string1.startIndex ..< string1.characters.index(string1.startIndex, offsetBy: 3))
        //  textfieldMonth.text = arrayMonths[row]
    }
}

