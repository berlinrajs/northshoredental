//
//  ToothWhiteningStep1VC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 13/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class LazerWhiteningStep1VC: PDViewController {

    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var patientSignatureView: SignatureView!
    @IBOutlet weak var labelDate1: DateLabel!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.Lazersignature1 = patientSignatureView.isSigned() ? patientSignatureView.signatureImage() :nil
    }
    
    func loadValues() {
        labelPatientName.text = patient.fullName
        labelDate1.todayDate = patient.dateToday
        
        patientSignatureView.previousImage = patient.Lazersignature1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if !patientSignatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }  else if !labelDate1.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kLazerWhiteningFormVC") as! LazerWhiteningFormVC
            formVC.patient = patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
