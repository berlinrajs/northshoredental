//
//  ToothWhiteningStep1VC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 13/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ToothWhiteningStep1VC: PDViewController {

    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var labelDentistName: UILabel!
    @IBOutlet weak var patientSignatureView: SignatureView!
    @IBOutlet weak var dentistSignatureView: SignatureView!
    @IBOutlet weak var initialView: SignatureView!
    
    @IBOutlet weak var labelDate1: DateLabel!
    @IBOutlet weak var labelDate2: DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.Toothwhitningsignature1 = patientSignatureView.isSigned() ? patientSignatureView.signatureImage() : nil
        patient.Toothwhitningsignature2 = dentistSignatureView.isSigned() ? dentistSignatureView.signatureImage() : nil
        patient.Toothwhitningsignature3 = !initialView.isSigned() ? nil : initialView.signatureImage()
    }
    
    func loadValues() {
        labelPatientName.text = patient.fullName
        labelDentistName.text = patient.doctorName
        
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        
        patientSignatureView.previousImage = patient.Toothwhitningsignature1
        dentistSignatureView.previousImage = patient.Toothwhitningsignature2
        initialView.previousImage = patient.Toothwhitningsignature3
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if !patientSignatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !dentistSignatureView.isSigned() {
            let alert = Extention.alert("PLEASE GET SIGNATURE FROM YOUR DENTIST")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate1.dateTapped || !labelDate2.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else if !initialView.isSigned() {
            let alert = Extention.alert("PLEASE ADD YOUR INITIAL TO THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kToothWhiteningFormVC") as! ToothWhiteningFormVC
            formVC.patient = patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
