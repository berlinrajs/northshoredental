//
//  NewPatient3ViewController.swift
//  YourCaringDentist
//
//  Created by Bala Murugan on 6/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SecondaryInsuranceVC: PDViewController {
    
    @IBOutlet weak var textfieldInsuranceCompanyName : PDTextField!
    @IBOutlet weak var textfieldPhone : PDTextField!
    @IBOutlet weak var textfieldGroup : PDTextField!
    @IBOutlet weak var textfieldInsuranceCoAddress : PDTextField!
    
    @IBOutlet weak var textFieldInsuranceCoCity: PDTextField!
    @IBOutlet weak var textFieldInsuraceCoState: PDTextField!
    @IBOutlet weak var textFieldInsuranceCozip: PDTextField!
    @IBOutlet weak var textFieldInsuredname: PDTextField!
    @IBOutlet weak var textFieldSocialSecurity: PDTextField!
    
    @IBOutlet weak var textFieldInsuredDOB: PDTextField!
    @IBOutlet weak var textfieldRelationship : PDTextField!
    @IBOutlet weak var textFieldEmployer: PDTextField!
    @IBOutlet weak var textfieldEmployerAddress: PDTextField!
    
    @IBOutlet weak var textFieldEmpCity: PDTextField!
    
    @IBOutlet weak var textFieldEmpzip: PDTextField!
    @IBOutlet weak var textFieldEmployerState: PDTextField!
    
    @IBOutlet weak var dentalCoverage: RadioButton!
    
    @IBOutlet weak var orthodonticCoverage: RadioButton!
    @IBOutlet weak var medicalCoverage: RadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        StateListView.addStateListForTextField(textFieldInsuraceCoState)
        StateListView.addStateListForTextField(textFieldEmployerState)
        
        DateInputView.addDatePickerForDateOfBirthTextField(textFieldInsuredDOB)

        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.secondaryInsuranceCompanyName = textfieldInsuranceCompanyName.text
        patient.secondaryPhone = textfieldPhone.text
        patient.secondaryGroup = textfieldGroup.text
        patient.secondaryInsuranceCoAddress = textfieldInsuranceCoAddress.text
        patient.secondaryInsuranceCoCity = textFieldInsuranceCoCity.text
        patient.secondaryInsuraceCoState = (!textfieldInsuranceCoAddress.isEmpty || !textFieldInsuranceCoCity.isEmpty || !textFieldInsuranceCozip.isEmpty) ? textFieldInsuraceCoState.text : ""
        patient.secondaryInsuranceCozip = textFieldInsuranceCozip.text
        
        patient.secondaryInsuredPersonname = textFieldInsuredname.text
        
        patient.secondarySocialSecurity = textFieldSocialSecurity.text
        patient.secondaryInsuredDOB = textFieldInsuredDOB.text
        patient.secondaryRelationship = textfieldRelationship.text
        patient.secondaryEmployer = textFieldEmployer.text
        
        patient.secondaryEmployerAddress = textfieldEmployerAddress.text
        patient.secondaryEmpCity = textFieldEmpCity.text
        patient.secondaryEmpzip = textFieldEmpzip.text
        patient.secondaryEmployerState = textFieldEmployerState.text
        
        
        patient.secondarydentalCoverageBtn = dentalCoverage.selected.tag
        patient.secondarymedicalCoverageBtn = medicalCoverage.selected.tag
        patient.secondaryOrthodonticCoverageBtn = orthodonticCoverage.selected.tag
    }
    func loadValues() {
        textfieldInsuranceCompanyName.text = patient.secondaryInsuranceCompanyName
        textfieldPhone.text = patient.secondaryPhone
        textfieldGroup.text = patient.secondaryGroup
        textfieldInsuranceCoAddress.text = patient.secondaryInsuranceCoAddress
        textFieldInsuranceCoCity.text = patient.secondaryInsuranceCoCity
        textFieldInsuraceCoState.text = patient.secondaryInsuraceCoState == nil || patient.secondaryInsuraceCoState.isEmpty ? "IL" : patient.secondaryInsuraceCoState
        textFieldInsuranceCozip.text = patient.secondaryInsuranceCozip
        
        textFieldInsuredname.text = patient.secondaryInsuredPersonname
        
        textFieldSocialSecurity.text = patient.secondarySocialSecurity
        textFieldInsuredDOB.text = patient.secondaryInsuredDOB
        textfieldRelationship.text = patient.secondaryRelationship
        textFieldEmployer.text = patient.secondaryEmployer
        
        textfieldEmployerAddress.text = patient.secondaryEmployerAddress
        textFieldEmpCity.text = patient.secondaryEmpCity
        textFieldEmpzip.text = patient.secondaryEmpzip
        textFieldEmployerState.text = patient.secondaryEmployerState == nil || patient.secondaryEmployerState.isEmpty ? "IL" : patient.secondaryEmployerState
        
        dentalCoverage.setSelectedWithTag(patient.secondarydentalCoverageBtn == nil ? 0 : patient.secondarydentalCoverageBtn)
        medicalCoverage.setSelectedWithTag(patient.secondarymedicalCoverageBtn == nil ? 0 : patient.secondarymedicalCoverageBtn)
        orthodonticCoverage.setSelectedWithTag(patient.secondaryOrthodonticCoverageBtn == nil ? 0 : patient.secondaryOrthodonticCoverageBtn)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func findEmptyTextField() -> UITextField? {
        let textFields = [textfieldInsuranceCompanyName]
        for textField in textFields {
            if (textField?.isEmpty)! {
                return textField
            }
        }
        return nil
    }
    
    
    @IBAction func onNextButtonPressed (_ sender : UIButton){
        
        if let _ = findEmptyTextField() {
            let alert = Extention.alert("PLEASE ENTER ALL REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        } else if !textfieldPhone.isEmpty && !textfieldPhone.text!.isPhoneNumber {
            let alert = Extention.alert("PLEASE ENTER VALID PHONE")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldInsuranceCozip.isEmpty && !textFieldInsuranceCozip.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID INSURANCE COMPANY ZIP CODE")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldSocialSecurity.isEmpty && !textFieldSocialSecurity.text!.isSocialSecurityNumber {
            let alert = Extention.alert("PLEASE ENTER VALID INSURED'S SOCIAL SECURITY NUMBER")
            self.present(alert, animated: true, completion: nil)
        } else if !textFieldEmpzip.isEmpty && !textFieldEmpzip.text!.isZipCode {
            let alert = Extention.alert("PLEASE ENTER VALID ZIP CODE")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let diseaseInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kResponsiblePersonVC") as! ResponsiblePersonVC
            diseaseInfoVC.patient = self.patient
            self.navigationController?.pushViewController(diseaseInfoVC, animated: true)
        }
    }
    
    @IBAction func radioInsuredPersonAction (_ sender : RadioButton){
        if sender.tag == 1{
            textFieldInsuredname.text = patient.fullName
            textFieldInsuredDOB.text = patient.dateOfBirth
            textfieldRelationship.text = "PATIENT"
            textFieldEmployer.text = patient.ContactemployerName
            textfieldEmployerAddress.text = patient.ContactAddress
            
            textFieldEmpCity.text = patient.ContactCity
            textFieldEmpzip.text = patient.ContactZip
            textFieldEmployerState.text = patient.ContactState
        } else {
            textFieldInsuredname.text = ""
            textFieldInsuredDOB.text = ""
            textfieldRelationship.text = ""
            textFieldEmployer.text = ""
            textfieldEmployerAddress.text = ""
            
            textFieldEmpCity.text = ""
            textFieldEmpzip.text = ""
            textFieldEmployerState.text = "IL"
        }
    }
}



extension SecondaryInsuranceVC : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldSocialSecurity{
            return textField.formatNumbers(range, string: string, count: 9)
        } else if textField == textFieldEmpzip || textField == textFieldInsuranceCozip {
            return textField.formatZipCode(range, string: string)
        } else if textField == textfieldPhone {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

