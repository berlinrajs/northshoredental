//
//  AdultPatientinformation.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AdultPatientInformation4: UIView, UITableViewDataSource, UITableViewDelegate {
    
    var arrayOfQuestions: [PDOption]! {
        didSet {
            (self.viewWithTag(100) as! UITableView).reloadData()
            (self.viewWithTag(200) as! UITableView).reloadData()
            (self.viewWithTag(300) as! UITableView).reloadData()
            (self.viewWithTag(400) as! UITableView).reloadData()
            }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayOfQuestions == nil ? 0 : 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 100 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellInfo", for: indexPath) as! AdultPatientInfoCell4
            
            cell.configWithOption(arrayOfQuestions[indexPath.row], atQuestionIndex: indexPath.row + 1)
            return cell

            
        } else if tableView.tag == 200 {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellInfo", for: indexPath) as! AdultPatientInfoCell4
            
            cell.configWithOption(arrayOfQuestions[indexPath.row + 14], atQuestionIndex: indexPath.row + 14)
            return cell
        
        
        
        }else if tableView.tag == 300 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellInfo", for: indexPath) as! AdultPatientInfoCell4
            
            cell.configWithOption(arrayOfQuestions[indexPath.row + 28], atQuestionIndex: indexPath.row + 28)
            return cell
            
            
            
        }else if tableView.tag == 400 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellInfo", for: indexPath) as! AdultPatientInfoCell4
            
            cell.configWithOption(arrayOfQuestions[indexPath.row + 42], atQuestionIndex: indexPath.row + 42)
            return cell
            
            
            
        } else {
        
        
        
        }
     
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellInfo", for: indexPath) as! AdultPatientInfoCell4
        
        cell.configWithOption(arrayOfQuestions[indexPath.row], atQuestionIndex: indexPath.row + 1)
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 100 {
            
            return 14
            
        } else if tableView.tag == 200{
        
            return 14
        
        }else if tableView.tag == 300{
            
            return 14
            
        }else if tableView.tag == 400 {
            
            return 13
            
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 4 || indexPath.row == 5 {
//            return 29.0
//        }
       // return indexPath.row == 1 ? 65.0 : indexPath.row == 2 ? 83 : indexPath.row == 4 ? 29 : indexPath.row == 5 ? 29.0 : 18.0
        
        return 23
    }
//    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return section == 1 ? 36.0 : section == 2 ? 54.0 : 0.0
//    }
//    
//    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        if section == 1 {
//            let view = UIView(frame: CGRectMake(0, 0, CGRectGetWidth(self.frame), 36))
//            
//            return nil
//        } else if section == 2 {
//            return nil
//        } else {
//            return nil
//        }
//    }
}
class AdultPatientInfoCell4: UITableViewCell {
    @IBOutlet weak var labelCount: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var radioYes: RadioButton!
    
    @IBOutlet weak var labelSubtitle: UILabel!
    @IBOutlet weak var labelAnswer1: FormLabel!
    @IBOutlet weak var labelAnswer2: FormLabel!
    @IBOutlet weak var labelAnswer3: FormLabel!
    
    var question: PDQuestion!
    var option : PDOption!
    
    func configWithOption(_ question: PDOption, atQuestionIndex: Int) {
        
        self.option = question
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        
       // labelCount.text = "\(atQuestionIndex)."
        labelTitle.text = question.question //+ " ........................................................................................."
        radioYes.setSelected(question.isSelected!)
        //
        //        labelSubtitle.text = atQuestionIndex == 2 ? "If yes, please explain" : atQuestionIndex == 3 ? "If yes, what medication(s) are you taking?" : ""
        //
        //        var rect = labelSubtitle.frame
        //        labelSubtitle.sizeToFit()
        //        rect.size.width = CGRectGetWidth(labelSubtitle.frame) + 3
        //        labelSubtitle.frame = rect
    }

    func configWithQuestion(_ question: PDQuestion, atQuestionIndex: Int) {
        
        self.question = question
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        
       // labelCount.text = "\(atQuestionIndex)."
        labelTitle.text = question.question + " ........................................................................................."
        radioYes.setSelected(question.selectedOption)
        
        labelTitle.sizeToFit()
        
       // labelSubtitle.text = atQuestionIndex == 2 ? "If yes, please explain" : atQuestionIndex == 3 ? "If yes, what medication(s) are you taking?" : ""
        
//        var rect = labelSubtitle.frame
//        labelSubtitle.sizeToFit()
//        rect.size.width = CGRectGetWidth(labelSubtitle.frame) + 3
//        labelSubtitle.frame = rect
    }
//    override func layoutSubviews() {
//        var rect = labelSubtitle.frame
//        labelSubtitle.sizeToFit()
//        rect.size.width = CGRectGetWidth(labelSubtitle.frame) + 3
//        labelSubtitle.frame = rect
//        
//        super.layoutSubviews()
//        if question != nil {
//            if question.isAnswerRequired == true {
////                labelAnswer1.text = question.answer
//                question.answer?.setTextForArrayOfLabels([labelAnswer1, labelAnswer2, labelAnswer3])
//            }
//        }
//    }
}


