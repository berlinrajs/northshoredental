//
//  EndodonticTreatmentStep1VC.swift
//  TotalHealthDental
//
//  Created by SRS Web Solutions on 06/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class EndodonticTreatmentStep1VC: PDViewController {

    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var signatureView2: SignatureView!
    @IBOutlet weak var signatureView3: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelToothNumber: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday!
        labelName.text = patient.fullName
        
        for form in patient.selectedForms {
            if form.formTitle == kEndodonticTreatment {
                labelToothNumber.text = form.toothNumbers
                break
            }
        }
        
        self.buttonBack?.isHidden = self.isFromPreviousForm
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.endodonticdentistSignature = signatureView3.isSigned() ? signatureView3.signatureImage() : nil
        patient.endodonticwitnessSignature = signatureView2.isSigned() ? signatureView2.signatureImage() : nil
        patient.endodonticPatientSignature = signatureView1.isSigned() ? signatureView1.signatureImage() : nil
    }
    func loadValues() {
        signatureView3.previousImage = patient.endodonticdentistSignature
        signatureView2.previousImage = patient.endodonticwitnessSignature
        signatureView1.previousImage = patient.endodonticPatientSignature
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonNextAction(_ sender: AnyObject) {
        if !signatureView1.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !labelDate.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        } else if !signatureView2.isSigned() {
            let alert = Extention.alert("PLEASE GET SIGNATURE FROM WITNESS")
            self.present(alert, animated: true, completion: nil)
        } else if !signatureView3.isSigned() {
            let alert = Extention.alert("PLEASE GET SIGNATURE FROM DENTIST")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kEndodonticTreatmentFormVC") as! EndodonticTreatmentFormVC
            formVC.patient = patient
            navigationController?.pushViewController(formVC, animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
