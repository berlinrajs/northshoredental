//
//  FinancialPolicyFormViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class FinancialPolicyFormViewController: PDViewController {

    @IBOutlet var labelDate: [UILabel]!
    @IBOutlet weak var imageViewSignature1: UIImageView!
    @IBOutlet weak var imageViewSignature2: UIImageView!
    @IBOutlet weak var imageViewSignature3: UIImageView!
    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var labelDOB: UILabel!
    @IBOutlet weak var labelToday: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = patient.fullName
        labelToday.text = patient.dateToday
        labelDOB.text = patient.dateOfBirth
        imageViewSignature1.image = patient.Fillingsignature1
        imageViewSignature2.image = patient.Fillingsignature2
        imageViewSignature3.image = patient.Fillingsignature3
        for label in labelDate {
            label.text = patient.dateToday
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension FinancialPolicyFormViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let serviceObj = patient.consentFillings[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellInitials") as! InitialsTableViewCell
        let text = serviceObj.serviceDescription
        let height = (text?.heightWithConstrainedWidth(553.0 * screenSize.width/768.0, font: cell.labelDetails.font))! + 4
        return height
    }
    
}


extension FinancialPolicyFormViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.consentFillings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellInitials", for:  indexPath) as! InitialsTableViewCell
        let serviceObj = patient.consentFillings[indexPath.row]
        cell.configureCell(serviceObj)
        cell.signatureView.tag = indexPath.row
        cell.signatureView.previousImage = serviceObj.signatureView.imageObj
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        return cell
    }
    
}
