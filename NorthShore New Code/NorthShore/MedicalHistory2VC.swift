//
//  HippaStep3VC.swift
//  North Shore
//
//  Created by SRS Web Solutions on 05/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistory2VC: PDViewController {
    
    @IBOutlet weak var CurrentDentalhealth: RadioButton!
    @IBOutlet weak var BristlesStage: RadioButton!
    @IBOutlet weak var HappyAbtYourSmile: RadioButton!
    
    @IBOutlet weak var PreviousDentistName: PDTextField!
    @IBOutlet weak var LastVisitDate: PDTextField!
    
    @IBOutlet weak var LeavePreviousDentistReason: PDTextView!
    @IBOutlet weak var LikeMostandLeastabtDentist: PDTextView!
    @IBOutlet weak var HowlonguseToothBrush: PDTextView!
    @IBOutlet weak var Sensitivity: PDTextView!
    @IBOutlet weak var popUpTextView: PDTextView!
    
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet weak var viewShadow: UIView!
    
    @IBOutlet var smilePopUp: PDView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        datePicker.maximumDate = nil
//        LastVisitDate.inputView = datePicker
//        LastVisitDate.inputAccessoryView = toolBar
        DateInputView.addDatePickerForTextField(LastVisitDate)
        
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.CurrentDentalTag  = CurrentDentalhealth.selected.tag
        patient.TypesOfBristleTag = BristlesStage.selected.tag
        patient.YouLikeyourSmileTag = HappyAbtYourSmile.selected.tag
        
        patient.HowlongUseToothBrush = HowlonguseToothBrush.text == "TYPE HERE" ? "" : HowlonguseToothBrush.text
        patient.ToothSensitive = Sensitivity.text == "TYPE HERE" ? "" : Sensitivity.text
        patient.PreviousDentist = PreviousDentistName.text
        patient.LastDentistVisit = LastVisitDate.text
        patient.YLeavePreviousDentist = LeavePreviousDentistReason.text == "TYPE HERE" ? "" : LeavePreviousDentistReason.text
        patient.WhatLikeAnyDentist = LikeMostandLeastabtDentist.text == "TYPE HERE" ? "" : LikeMostandLeastabtDentist.text
    }
    func loadValues() {
        CurrentDentalhealth.setSelectedWithTag(patient.CurrentDentalTag == nil ? 0 : patient.CurrentDentalTag)
        BristlesStage.setSelectedWithTag(patient.TypesOfBristleTag == nil ? 0 : patient.TypesOfBristleTag)
        HappyAbtYourSmile.setSelectedWithTag(patient.YouLikeyourSmileTag == nil ? 0 : patient.YouLikeyourSmileTag)
        
        self.textViewDidBeginEditing(HowlonguseToothBrush)
        HowlonguseToothBrush.text = patient.HowlongUseToothBrush
        self.textViewDidEndEditing(HowlonguseToothBrush)
        
        self.textViewDidBeginEditing(Sensitivity)
        Sensitivity.text = patient.ToothSensitive
        self.textViewDidEndEditing(Sensitivity)
        
        PreviousDentistName.text = patient.PreviousDentist
        LastVisitDate.text = patient.LastDentistVisit
        
        self.textViewDidBeginEditing(LeavePreviousDentistReason)
        LeavePreviousDentistReason.text = patient.YLeavePreviousDentist
        self.textViewDidEndEditing(LeavePreviousDentistReason)
        
        self.textViewDidBeginEditing(LikeMostandLeastabtDentist)
        LikeMostandLeastabtDentist.text = patient.WhatLikeAnyDentist
        self.textViewDidEndEditing(HowlonguseToothBrush)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func toolbarDoneButtonAction(_ sender: AnyObject) {
        LastVisitDate.resignFirstResponder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        LastVisitDate.text = dateFormatter.string(from: datePicker.date).uppercased()
    }
    
    @IBAction func datePickerDateChanged(_ sender: AnyObject) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        LastVisitDate.text = dateFormatter.string(from: datePicker.date).uppercased()
        
    }
    
    @IBAction func YesOrNoSmile(_ sender: AnyObject) {
        if HappyAbtYourSmile.selected.tag == 2 {
            self.viewShadow.isHidden = false
            let frameSize = CGRect(x: 0, y: 0, width: 348, height: 174)
            self.smilePopUp.frame = frameSize
            self.smilePopUp.center = self.view.center
            self.viewShadow.addSubview(self.smilePopUp)
            self.smilePopUp.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            popUpTextView.text = "TYPE HERE"
            popUpTextView.textColor = UIColor.lightGray
            self.viewShadow.isHidden = false
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.3)
            self.smilePopUp.transform = CGAffineTransform.identity
            UIView.commitAnimations()
        }
    }
    @IBAction func actionNext(_ sender: AnyObject) {
        
        saveValues()
        
        let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
        let MedicalHistory3 = storyboard.instantiateViewController(withIdentifier: "kMedicalHistoryStep3VC") as! MedicalHistoryStep3ViewController
        MedicalHistory3.patient = patient
        self.navigationController?.pushViewController(MedicalHistory3, animated: true)
        
    }
    
    @IBAction func POpUpOkPressed(_ sender: AnyObject) {
        if !popUpTextView.isEmpty && popUpTextView.text != "TYPE HERE" {
            patient.SmileReasonPopup = popUpTextView.text
        } else {
            patient.SmileReasonPopup = nil
        }
        popUpTextView.resignFirstResponder()
        self.smilePopUp.removeFromSuperview()
        self.viewShadow.isHidden = true
    }
}


extension MedicalHistory2VC : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
