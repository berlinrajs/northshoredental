//
//  CrownsAndBridgesViewControllerScene.swift
//  AdultMedicalForm
//
//  Created by SRS Web Solutions on 17/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CrownsAndBridgesViewControllerScene: PDViewController {
    @IBOutlet var imgSignatureView: SignatureView!
    @IBOutlet var imgSignatureView2: SignatureView!
    
    @IBOutlet var lblDate1: DateLabel!
    @IBOutlet var lblDate2: DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        lblDate1.todayDate = patient.dateToday
        lblDate2.todayDate = patient.dateToday
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.CrownBridgesignature1 = imgSignatureView.isSigned() ? imgSignatureView.signatureImage() : nil
        patient.CrownBridgesignature2 = imgSignatureView2.isSigned() ? imgSignatureView2.signatureImage() : nil
    }
    func loadValues() {
        imgSignatureView.previousImage = patient.CrownBridgesignature1
        imgSignatureView2.previousImage = patient.CrownBridgesignature2
    }
   
    
    @IBAction func btnActionNext(_ sender: AnyObject) {
        
         if !imgSignatureView.isSigned() || !imgSignatureView2.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if !lblDate1.dateTapped || !lblDate2.dateTapped {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
         }else {
            saveValues()
            let nextViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "CrownsAndBridgesConsentFormViewController") as! CrownsAndBridgesConsentFormViewController
            nextViewControllerObj.patient = self.patient
            self.navigationController?.pushViewController(nextViewControllerObj, animated: true)
        }}

override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
        }}


