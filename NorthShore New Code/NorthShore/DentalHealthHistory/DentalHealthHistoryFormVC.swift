//
//  DentalHealthHistoryFormVC.swift
//  NorthShore
//
//  Created by SRS Websolutions.com on 7/5/18.
//  Copyright © 2018 SRS. All rights reserved.
//

import UIKit

class DentalHealthHistoryFormVC: PDViewController {

    @IBOutlet weak var labelLastName: UILabel!
    @IBOutlet weak var labelFirstName: UILabel!
    @IBOutlet weak var labelDOB: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var labelFloss: UILabel!
    @IBOutlet weak var labelBrush: UILabel!
    @IBOutlet weak var labelPhysicianName: UILabel!
    @IBOutlet weak var labelDateLastVisit: UILabel!
    @IBOutlet weak var labelYesDescribe: UILabel!
    @IBOutlet weak var labelOther: UILabel!
    @IBOutlet weak var labelMedications: UILabel!
    @IBOutlet weak var labelPharmacyName: UILabel!
    @IBOutlet weak var labelPharmacyPhone: UILabel!
    @IBOutlet weak var labelAllergiesOther: UILabel!
    
    @IBOutlet var dentalHistoryTags: [UIButton]!
    
    @IBOutlet var dietMedicationTags: [UIButton]!
    @IBOutlet var bloodThinnersTags: [UIButton]!
    @IBOutlet var otherTags: [UIButton]!
    
    @IBOutlet var radioButtonMH1: [RadioButton]!
    @IBOutlet var radioButtonMH2: [RadioButton]!
    @IBOutlet var radioButtonMH3: [RadioButton]!
    
    @IBOutlet var allergiesTags: [UIButton]!
    
    @IBOutlet weak var imageSignature: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    
    
    @IBOutlet weak var radioWomen1: RadioButton!
    @IBOutlet weak var radioWomen2: RadioButton!
    @IBOutlet weak var radioWomen3: RadioButton!
    
    @IBOutlet weak var radioTheseMedications: RadioButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
    }
    
    func loadValues() {
        labelLastName.text = patient.lastName
        labelFirstName.text = patient.firstName
        labelDOB.text = patient.dateOfBirth
        labelAddress.text = patient.addressLine
        labelEmail.text = patient.email
        labelPhone.text = patient.phoneNumber
        
        if patient.dhhProblemCheckBoxTag == nil {
            patient.dhhProblemCheckBoxTag = [Int]()
        }
        for button in dentalHistoryTags {
            button.isSelected = patient.dhhProblemCheckBoxTag.contains(button.tag)
        }
        
        radioTheseMedications.setSelectedWithTag(patient.dhhIllnessTag)
        
        labelFloss.text = patient.dhhFloss
        labelBrush.text = patient.dhhTeeth
        labelPhysicianName.text = patient.dhhPhysicianName
        labelDateLastVisit.text = patient.dhhPhysicianDateLastVisit
        labelYesDescribe.text = patient.dhhIllnessYES
        labelOther.text = patient.medicalHistory.medicalQuestions[2][16].answer
        labelMedications.text = patient.dhhMedicationList
        labelPharmacyName.text = patient.dhhPharmacyName
        labelPharmacyPhone.text = patient.dhhPharmacyPhone
        labelAllergiesOther.text = patient.dhhOtherAllergies
        
        radioWomen1.setSelectedWithTag(patient.dhhWomenTag1)
        radioWomen2.setSelectedWithTag(patient.dhhWomenTag2)
        radioWomen3.setSelectedWithTag(patient.dhhWomenTag3)
        
        
        if patient.dhhMedicationsTag == nil {
            patient.dhhMedicationsTag = [Int]()
        }
        for button in dietMedicationTags {
            button.isSelected = patient.dhhMedicationsTag.contains(button.tag)
        }
        
        if patient.dhhBloodThinnersTag == nil {
            patient.dhhBloodThinnersTag = [Int]()
        }
        for button in bloodThinnersTags {
            button.isSelected = patient.dhhBloodThinnersTag.contains(button.tag)
        }
        
        if patient.dhhOtherTag == nil {
            patient.dhhOtherTag = [Int]()
        }
        for button in otherTags {
            button.isSelected = patient.dhhOtherTag.contains(button.tag)
        }
        
        
        for (idx,btn) in radioButtonMH1.enumerated(){
            btn.isSelected = patient.medicalHistory.medicalQuestions[0][idx].selectedOption
        }
        
        for (idx,btn) in radioButtonMH2.enumerated(){
            btn.isSelected = patient.medicalHistory.medicalQuestions[1][idx].selectedOption
        }
        
        for (idx,btn) in radioButtonMH3.enumerated(){
            btn.isSelected = patient.medicalHistory.medicalQuestions[2][idx].selectedOption
        }
        
        if patient.dhhAllergies == nil {
            patient.dhhAllergies = [Int]()
        }
        for button in allergiesTags {
            button.isSelected = patient.dhhAllergies.contains(button.tag)
        }
        
        imageSignature.image = patient.dhhSignature
        labelDate.text = patient.dateToday
    }
}
