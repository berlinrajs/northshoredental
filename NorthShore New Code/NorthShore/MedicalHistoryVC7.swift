//
//  AgreementViewController.swift
//  North Shore
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryVC7: PDViewController {
    
    @IBOutlet weak var labelText1: UILabel!
    @IBOutlet weak var labelText2: UILabel!
    @IBOutlet weak var constraintLabel1Height: NSLayoutConstraint!
    @IBOutlet weak var constraintLabel2Height: NSLayoutConstraint!
    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var signatureView2: SignatureView!
    @IBOutlet weak var labelDate1: PDLabel!
    @IBOutlet weak var labelDate2: PDLabel!
    
    @IBOutlet weak var labelDoctorName: UILabel!
    @IBOutlet weak var dropdownPayment : BRDropDown!
    @IBOutlet weak var insuranceCompanyName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        signatureView1.layer.cornerRadius = 3.0
        signatureView2.layer.cornerRadius = 3.0
        
        labelText1.setAttributedText()
        labelText2.setAttributedText()
        dropdownPayment.items = ["Credit card","Debit card","Cheque","Cash"]

        if patient.primaryInsuranceCompanyName != nil {
            insuranceCompanyName.text = patient.primaryInsuranceCompanyName
        } else{
            insuranceCompanyName.text = "N/A"
        }
        labelDoctorName.text = patient.doctorName
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(MedicalHistoryVC7.setDateOnLabel1))
        tapGesture1.numberOfTapsRequired = 1
        labelDate1.addGestureRecognizer(tapGesture1)
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(MedicalHistoryVC7.setDateOnLabel2))
        tapGesture2.numberOfTapsRequired = 1
        labelDate2.addGestureRecognizer(tapGesture2)
        
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        patient.medicalHis7signature1 = signatureView1.isSigned() ? signatureView1.signatureImage() : nil
        patient.medicalHis7signature2 = signatureView2.isSigned() ? signatureView2.signatureImage() : nil
        
        patient.MethodOfPayment = dropdownPayment.selectedOption
    }
    func loadValues() {
        signatureView1.previousImage = patient.medicalHis7signature1
        signatureView2.previousImage = patient.medicalHis7signature2
        
        if patient.MethodOfPayment != nil {
            dropdownPayment.selectedIndex = dropdownPayment.items.index(of: patient.MethodOfPayment)! + 1
        }
    }
    
    func setDateOnLabel1() {
        labelDate1.text = patient.dateToday
        labelDate1.textColor = UIColor.black
    }

    func setDateOnLabel2() {
        labelDate2.text = patient.dateToday
        labelDate2.textColor = UIColor.black
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonNext(_ sender: AnyObject) {
        if !signatureView1.isSigned() || !signatureView2.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        } else if labelDate1.text == "Tap to date" || labelDate2.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.present(alert, animated: true, completion: nil)
        }else if dropdownPayment.selectedIndex == 0 {
            let alert = Extention.alert("PLEASE SELECT THE METHOD OF PAYMENT")
            self.present(alert, animated: true, completion: nil)
        } else {
            saveValues()
            
            let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
            let Welcomepdf = storyboard.instantiateViewController(withIdentifier: "kWelcomeFormVCRef") as! WelcomeForm
            Welcomepdf.patient = patient
            self.navigationController?.pushViewController(Welcomepdf, animated: true)
        }
    }

}
