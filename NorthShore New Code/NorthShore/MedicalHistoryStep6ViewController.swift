//
//  MedicalHistoryStep4ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryStep6ViewController: PDViewController {

//    var medicalHistoryStep6 : [PDOption]! = [PDOption]()
    var currentIndex : Int = 0
    var fetchCompleted : Bool! = false
    
    @IBOutlet weak var takingAnyPrescription: RadioButton!
    @IBOutlet weak var buttonNextOut: PDButton!
    @IBOutlet weak var buttonBackOut: PDButton!
    @IBOutlet weak var tableViewQuestions: UITableView!
//    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var buttonVerified: UIButton!
    
    
     @IBOutlet weak var viewShadow: UIView!
     @IBOutlet weak var popUpTextView: PDTextView!
     @IBOutlet var smilePopUp: PDView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
    }
    
    override func buttonActionBack(_ sender: AnyObject) {
        saveValues()
        super.buttonActionBack(sender)
    }
    func saveValues() {
        
    }
    func loadValues() {
        if self.patient.medicalHistoryQuestions6 == nil {
            fetchData()
        } else {
            self.fetchCompleted = true
            tableViewQuestions.reloadData()
        }
    }
    
    func fetchData() {
        self.patient.medicalHistoryQuestions6 = [PDOption]()
        PDOption.fetchQuestionsForm6 { (result, success) -> Void in
//            self.activityIndicator.stopAnimating()
            self.fetchCompleted = true
            if success {
                self.patient.medicalHistoryQuestions6.append(contentsOf: result!)
                self.tableViewQuestions.reloadData()
            }
        }
    }
    
    @IBAction func buttonBackAction(_ sender: AnyObject) {
        if currentIndex == 0 {
            self.buttonActionBack(sender)
        } else {
            self.buttonActionBack(sender)
//            buttonBackOut.userInteractionEnabled = false
//            buttonNextOut.userInteractionEnabled = false
//            self.buttonVerified.selected = true
//            self.activityIndicator.startAnimating()
//            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.4 * Double(NSEC_PER_SEC)))
//            dispatch_after(delayTime, dispatch_get_main_queue()) {
//                self.activityIndicator.stopAnimating()
//                self.currentIndex = self.currentIndex - 1
//                self.tableViewQuestions.reloadData()
//                self.buttonBackOut.userInteractionEnabled = true
//                self.buttonNextOut.userInteractionEnabled = true
//            }
        }
    }
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func takingAnyPrescriptionAction(_ sender: AnyObject) {
        if sender.tag == 1 {
            self.viewShadow.isHidden = false
            let frameSize = CGRect(x: 0, y: 0, width: 348, height: 174)
            self.smilePopUp.frame = frameSize
            self.smilePopUp.center = self.view.center
            self.viewShadow.addSubview(self.smilePopUp)
            self.smilePopUp.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            popUpTextView.text = "IF YES TYPE HERE"
            popUpTextView.textColor = UIColor.lightGray
            self.viewShadow.isHidden = false
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.3)
            self.smilePopUp.transform = CGAffineTransform.identity
            UIView.commitAnimations()
        }
    }
    
    
    @IBAction func POpUpOkPressed(_ sender: AnyObject) {
        if !popUpTextView.isEmpty && popUpTextView.text != "IF YES TYPE HERE" {
            patient.takingAnyPrescriptionPopup = popUpTextView.text
        } else {
            patient.SmileReasonPopup = nil
        }
        popUpTextView.resignFirstResponder()
        self.smilePopUp.removeFromSuperview()
        self.viewShadow.isHidden = true
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if self.fetchCompleted == true {
            if ((currentIndex + 1) * 20) < self.patient.medicalHistoryQuestions6.count {
                if !buttonVerified.isSelected {
                    let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
                    let MedicalHistory6 = storyboard.instantiateViewController(withIdentifier: "kMedicalHistoryStep6VC") as! MedicalHistoryStep6ViewController
                    MedicalHistory6.patient = patient
                    MedicalHistory6.currentIndex = self.currentIndex + 1
                    self.navigationController?.pushViewController(MedicalHistory6, animated: true)
//                    buttonBackOut.userInteractionEnabled = false
//                    buttonNextOut.userInteractionEnabled = false
//                    self.buttonVerified.selected = false
//                    self.activityIndicator.startAnimating()
//                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.4 * Double(NSEC_PER_SEC)))
//                    dispatch_after(delayTime, dispatch_get_main_queue()) {
//                        self.activityIndicator.stopAnimating()
//                        self.currentIndex = self.currentIndex + 1
//                        self.tableViewQuestions.reloadData()
//                        self.buttonBackOut.userInteractionEnabled = true
//                        self.buttonNextOut.userInteractionEnabled = true
//                    }
                }
            } else {
                if !buttonVerified.isSelected {
                    let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let storyboard = UIStoryboard(name: "Welcome", bundle: nil)
                    let MedicalHistory6 = storyboard.instantiateViewController(withIdentifier: "kMedicalHistory7VC") as! MedicalHistoryVC7
                    MedicalHistory6.patient = patient
                    self.navigationController?.pushViewController(MedicalHistory6, animated: true)
                }
            }
        }
    }
    
    func findEmptyValue() -> PDOption? {
        let objects = patient.medicalHistoryQuestions6.filter { (obj) -> Bool in
            return obj.index >= (currentIndex * 20) && obj.index < ((currentIndex + 1) * 20)
        }
        for question in objects {
            if question.isSelected == nil {
                return question
            }
        }
        return nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MedicalHistoryStep6ViewController : MedicalHistoryCellDelegate {
    func radioButtonAction(_ sender: RadioButton) {
        self.tableViewQuestions.reloadData()
    }
}


extension MedicalHistoryStep6ViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let objects = patient.medicalHistoryQuestions6.filter { (obj) -> Bool in
            return obj.index >= (currentIndex * 20) && obj.index < ((currentIndex + 1) * 20)
        }
        return objects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryStep4", for: indexPath) as! MedicalHistoryStep1TableViewCell
        let index = (currentIndex * 20) + indexPath.row
        let obj = patient.medicalHistoryQuestions6[index]
        cell.configureCellOption(obj)
        if let selected = obj.isSelected {
            cell.buttonYes.isSelected = selected
        } else {
            cell.buttonYes.deselectAllButtons()
        }
        cell.buttonYes.tag = index
        cell.buttonNo.tag = index
        cell.delegate = self
        return cell
    }
}


extension MedicalHistoryStep6ViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "IF YES TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "IF YES TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
