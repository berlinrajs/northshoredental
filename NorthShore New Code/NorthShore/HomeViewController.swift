//
//  HomeViewController.swift
//  AdultMedicalForm
//
//  Created by SRS Web Solutions on 06/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HomeViewController: PDViewController {
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var tablevViewForm: UITableView!
    @IBOutlet weak var textFieldToothNumbers: PDTextField!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet var labelAlertFooter: UILabel!
    @IBOutlet var labelAlertHeader: UILabel!
    @IBOutlet var viewToothNumbers: PDView!
    @IBOutlet weak var viewAlert: PDView!
    @IBOutlet weak var labelVersion: UILabel!
    var consentIndex : Int = 7
    var privacyAcknowledgementTag : Int = 0
    var privacyAcknowledgementReason : String = ""
    var okPressed : Bool = false
    
    var selectedForms : [Forms]! = [Forms]()
    var formList : [Forms]! = [Forms]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        if let text = Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String {
            labelVersion.text = text
        }
        self.navigationController?.navigationBar.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.showAlertPopUp), name: NSNotification.Name(rawValue: kFormsCompletedNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dateChangedNotification), name: NSNotification.Name(rawValue: kDateChangedNotification), object: nil)
        self.dateChangedNotification()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.background).async {
            let defaults = UserDefaults.standard
            ServiceManager.loginWithUsername(defaults.value(forKey: kAppLoginUsernameKey) as! String, password: defaults.value(forKey: kAppLoginPasswordKey) as! String) { (success, error) -> (Void) in
                if success {
                    
                } else {
                    if error == nil {
                        
                    } else {
                        DispatchQueue.main.async(execute: {
                            UserDefaults.standard.set(false, forKey: "kApploggedIn")
                            UserDefaults.standard.synchronize()
                            (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
                        })
                    }
                }
            }
        }
        Forms.getAllForms { (isConnectionFailed, forms) -> Void in
            self.formList = forms
            self.tablevViewForm.reloadData()
            if isConnectionFailed == true {
                let alertController = UIAlertController(title: "North Shore", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
                let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url)
                    }
                }
                let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    
                }
                alertController.addAction(alertOkAction)
                alertController.addAction(alertCancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func dateChangedNotification() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        self.lblDate.text = dateFormatter.string(from: NSDate() as Date).uppercased()
    }
    
    @IBAction func buttonActionDone(_ sender: AnyObject) {
        textFieldToothNumbers.resignFirstResponder()
        self.viewToothNumbers.removeFromSuperview()
        self.viewShadow.isHidden = true
        let form = textFieldToothNumbers.tag <= consentIndex ? formList[textFieldToothNumbers.tag] : formList[consentIndex].subForms[textFieldToothNumbers.tag - (consentIndex + 1)]
        if !textFieldToothNumbers.isEmpty {
            form.isSelected = true
            form.toothNumbers = textFieldToothNumbers.text
        } else {
            form.isSelected = false
        }
        self.tablevViewForm.reloadData()
        
    }
    
    func showAlertPopUp() {
        viewAlert.isHidden = false
        viewShadow.isHidden = false
    }
    
    @IBAction func buttonActionOk(_ sender: AnyObject) {
        
        viewAlert.isHidden = true
        viewShadow.isHidden = true
    }
    
    @IBAction func btnNextAction(_ sender: AnyObject) {
        selectedForms.removeAll()
        for (_, form) in formList.enumerated() {
            if form.isSelected == true {
                if form.formTitle == kConsentForms  {
                    for subForm in form.subForms {
                        if subForm.isSelected == true {
                            selectedForms.append(subForm)
                        }
                    }
                } else {
                    selectedForms.append(form)
                }
            }
        }
        self.view.endEditing(true)
        if selectedForms.count > 0 {
            selectedForms.sort(by: { (formObj1, formObj2) -> Bool in
                return formObj1.index < formObj2.index
            })
            let patient = PDPatient(forms: selectedForms)
            patient.dateToday = lblDate.text
            if selectedForms.count == 1 && selectedForms.first!.formTitle == kFeedBack {
                let patientInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kFeedBackInfoViewController") as! FeedBackInfoViewController
                patientInfoVC.patient = patient
                self.navigationController?.pushViewController(patientInfoVC, animated: true)
            } else if selectedForms.first!.formTitle == kVisitorCheckForm && selectedForms.count > 0 {
                
                let patientInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kVisitorVC") as! VisitorCheckinVC
                patientInfoVC.patient = patient
                self.navigationController?.pushViewController(patientInfoVC, animated: true)
            } else {
                let patientInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientInfoVC") as! PatientInfoViewController
                patient.privacyAcknowledgementTag = privacyAcknowledgementTag
                patient.privacyAcknowledgementReason = privacyAcknowledgementReason
                patient.okPressed = okPressed
                patientInfoVC.patient = patient
                self.navigationController?.pushViewController(patientInfoVC, animated: true)
            }            } else {
            let alert = Extention.alert("PLEASE SELECT ANY FORM")
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showPopup() {
        textFieldToothNumbers.text = ""
        self.viewToothNumbers.frame = CGRect(x: 0, y: 0, width: 512.0, height: 250.0)
        self.viewToothNumbers.center = self.view.center
        self.viewShadow.addSubview(self.viewToothNumbers)
        self.viewToothNumbers.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        self.viewShadow.isHidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewToothNumbers.transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
}


extension HomeViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.row == consentIndex {
            let subForms = formList[consentIndex].subForms
            for subFrom in subForms! {
                subFrom.isSelected = false
            }
            let form = self.formList[consentIndex]
            form.isSelected = !form.isSelected
            var indexPaths : [IndexPath] = [IndexPath]()
            for (idx, _) in form.subForms.enumerated() {
                let indexPath = IndexPath(row: 6 + idx, section: 0)
                indexPaths.append(indexPath)
            }
            if form.isSelected == true {
                tableView.insertRows(at: indexPaths, with: .bottom)
                let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    if form.isSelected == true {
                        tableView.scrollToRow(at: indexPaths.last!, at: .bottom, animated: true)
                    }
                }
            } else {
                
                tableView.deleteRows(at: indexPaths, with: .bottom)
            }
            tableView.reloadRows(at: [indexPath], with: .none)
            return
        }
        var form : Forms!
        if (indexPath.row <= consentIndex) {
            form = formList[indexPath.row]
        } else if formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count {
            form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
        } else {
            form = formList.last
        }
        if form.isToothNumberRequired == true && !form.isSelected {
            textFieldToothNumbers.tag = indexPath.row
            if indexPath.row == 9 {
                textFieldToothNumbers.placeholder = "01, 02, 15, 18"
                labelAlertHeader.text = "PLEASE ENTER TOOTH NUMBERS"
                labelAlertFooter.text = "Note: Separate with commas"
                textFieldToothNumbers.delegate = self
                textFieldToothNumbers.keyboardType = UIKeyboardType.numberPad
            } else if indexPath.row == 22 {
                textFieldToothNumbers.placeholder = "01, 02, 15, 18"
                labelAlertHeader.text = "PLEASE ENTER TOOTH NUMBERS"
                labelAlertFooter.text = "Note: Separate with commas"
                textFieldToothNumbers.delegate = self
                textFieldToothNumbers.keyboardType = UIKeyboardType.numberPad
            }else if indexPath.row == 25 {
                textFieldToothNumbers.placeholder = "01, 02, 15, 18"
                labelAlertHeader.text = "PLEASE ENTER TOOTH NUMBERS"
                labelAlertFooter.text = "Note: Separate with commas"
                textFieldToothNumbers.delegate = self
                textFieldToothNumbers.keyboardType = UIKeyboardType.numberPad
            } else {
                textFieldToothNumbers.placeholder = "PLEASE TYPE"
                labelAlertHeader.text = "ENTER PRESCRIPTION"
                labelAlertFooter.text = "If Any"
                textFieldToothNumbers.delegate = nil
                textFieldToothNumbers.keyboardType = UIKeyboardType.default
            }
            self.showPopup()
        }
        form.isSelected = !form.isSelected
        tableView.reloadData()
    }
}

extension HomeViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if formList.count > 0{
            let subForms = formList[consentIndex].subForms
            return formList[consentIndex].isSelected == true ? formList.count + subForms!.count  : formList.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let formsCount = formList[3].isSelected == true ? formList.count + formList[3].subForms.count  : formList.count
        if (indexPath.row <= consentIndex) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellMainForm", for: indexPath) as! HomePageTableViewCell
            let form = formList[indexPath.row]
            cell.lblName.text = form.formTitle
            cell.imgViewCheckMark.isHidden = !form.isSelected
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            return cell
        } else if formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellSubForm", for: indexPath) as!HomePageTableViewCell
            let form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
            cell.lblName.text = form.formTitle
            cell.imgViewCheckMark.isHidden = !form.isSelected
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellMainForm", for: indexPath) as! HomePageTableViewCell
            let form = formList[consentIndex + 1]
            cell.lblName.text = form.formTitle
            cell.imgViewCheckMark.isHidden = !form.isSelected
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            return cell
        }
    }

}

extension HomeViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return textField.formatToothNumbers(range, string: string)
    }
}



