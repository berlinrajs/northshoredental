//
//  PDTextField.swift
//  SecureDental
//
//  Created by Leojin Bose on 18/02/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDTextField: UITextField {

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.borderColor = isEnabled ? borderColor.cgColor : borderColor.withAlphaComponent(0.2).cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: isEnabled ? placeHolderColor : placeHolderColor.withAlphaComponent(0.2)])
        tintColor = isEnabled ? placeHolderColor : placeHolderColor.withAlphaComponent(0.2)
        clipsToBounds = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: frame.height))
        self.leftView = leftView
        leftViewMode = .always
    }
    
    var borderColor: UIColor = UIColor.white {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }

    var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    var placeHolderColor: UIColor = UIColor.white.withAlphaComponent(0.5) {
        didSet {
            attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: placeHolderColor])
            tintColor = placeHolderColor
        }
    }
    
    override var placeholder: String? {
        willSet {
            
        }
        didSet {
            attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: placeHolderColor])
            tintColor = placeHolderColor
        }
    }
    
    
//    //MARK: Initializers
//    override func prepareForInterfaceBuilder() {
//        super.prepareForInterfaceBuilder()
//        layer.borderColor = borderColor.CGColor
//        layer.borderWidth = borderWidth
//        layer.cornerRadius = cornerRadius
//    }
//
//    func setup() {
//        layer.borderColor = UIColor.whiteColor().CGColor
//        layer.borderWidth = 1.0
//        layer.cornerRadius = 3.0
//    }
//    
//    func configure() {
//        layer.borderColor = borderColor.CGColor
//        layer.borderWidth = borderWidth
//        layer.cornerRadius = cornerRadius
//    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        layer.borderColor = borderColor.CGColor
//        layer.borderWidth = borderWidth
//        layer.cornerRadius = cornerRadius
//        clipsToBounds = true
//    }
}
