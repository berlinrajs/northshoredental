//
//  ToothWhiteningFormVC.swift
//  SecureDental
//
//  Created by SRS Web Solutions on 13/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class LazerWhiteningFormVC: PDViewController {

    
    @IBOutlet weak var labelDate1: UILabel!
   
    @IBOutlet weak var labelPatientName: UILabel!
   
    @IBOutlet weak var PatientNamePrint: UILabel!
    
    @IBOutlet weak var ImagePatientSignature: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        
        labelDate1.text = patient.dateToday
     
        PatientNamePrint.text = patient.fullName
    
        labelPatientName.text = patient.fullName
        
        ImagePatientSignature.image = patient.Lazersignature1
     
      
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
